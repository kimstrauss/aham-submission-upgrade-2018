USE [Ahamoper]
GO

/****** Object:  View [Submission].[vModelPermissions]    Script Date: 2/9/2018 2:49:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER   view [Submission].[vModelPermissions]
as
select distinct 
cr.contactkey, 
cr.assnkey,
cr.ntuser,
rmfg.mfgkey, 
mfgname,
rmfg.productkey,
productname,
cr.dealerkey,
seeCoData,
EditCoData,
seeIndData,
SeeModels,
EditModels,
SubmitData,
Responsible,
EditCodes,
SecLevel,
SeePublic, 
seeAll
from contactroles as cr
inner join rmfg on (rmfg.productkey = cr.productkey 
		and cr.mfgkey = rmfg.mfgkey)
		or (cr.mfgkey = rmfg.mfgkey 
		and cr.productkey = 0)
		or (cr.mfgkey = 0 
		and cr.productkey = rmfg.productkey)
		or (cr.mfgkey = 0 and cr.productkey = 0)
inner join dmfg on rmfg.mfgkey = dmfg.mfgkey 
inner join dproduct on dproduct.productkey = rmfg.productkey
inner join YearDates on begdate between rmfg.begindate and rmfg.enddate and companydatapresent = 1

where seecodata + editcodata + seeinddata + seemodels + submitdata + seeall > 0
and rmfg.mfgkey not in (50, 52, 120)

GO
