USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickSubmissionGetReceipt]    Script Date: 2/9/2018 2:32:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






ALTER Procedure  [Submission].[ickSubmissionGetReceipt](
	@SubmitKey int, @ntuser varchar(50) )
as 
/* ickSubmissionGetReceipt takes the given submission key and looks up 
   the mfgplogs with that submission key.  It returns an xml report of those
   reports.
	Created by Kim Strauss
	4/23/02
	2/7/18 kds Pass in Username, change output
*/
set nocount on
declare @jobtype char(1)
set @jobtype = (select top 1 jobtype from at_subjobs 
	where subjobkey = @submitkey
	and ntuser = @ntuser)

declare @jobinfo table(
subjobkey int, 
ntuser varchar(50),
submitdate datetime,
mfgkey int,
mfgname varchar(50),
postperiod smalldatetime)

declare @reporttable table(
subjobkey int,
mfgplogkey int,
mfgreportkey int, 
reportname varchar(250),
enddate smalldatetime,
datarev varchar(20),
catalogsetkey integer,
catalogsetname varchar(30))


insert into @jobinfo 
select subjobkey, ntuser, max(submitdate), ats.mfgkey, mfgname, ats.enddate
from at_subjobs as ats
inner join dmfg on dmfg.mfgkey = ats.mfgkey
where subjobkey = @submitkey and ntuser = @ntuser
group by subjobkey, ntuser, ats.mfgkey, mfgname, ats.enddate

if @jobtype = 'S' begin

insert into @reporttable
select ji.subjobkey, mfgplog.mfgplogkey, rmfg.mfgreportkey, 
case rmfg.activitysetkey when 2 then isnull(rmfg.rptname, dfreq.freqname + ' ' + catalogsetname + ' ' + productname + ' ' + activitysetname)
else isnull(rmfg.rptname, dfreq.freqname + ' ' + catalogsetname + ' ' + productname + ' ' + activitysetname + ' to ' + geosetname) end,
mfgplog.enddate, 
case when mfgplog.enddate = mfgplog.postperiod then 'Data'
	else 'Revision' end,
sdcatalog.catalogsetkey, catalogsetname
from mfgplog 
inner join @jobinfo as ji on ji.subjobkey = mfgplog.submitkey
inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
inner join dfreq on dfreq.freqkey = rmfg.freqkey
inner join sdactivity on sdactivity.activitysetkey = rmfg.activitysetkey
inner join sdcatalog on sdcatalog.catalogsetkey = rmfg.catalogsetkey
inner join sdgeo on sdgeo.geosetkey = rmfg.geosetkey
inner join dproduct as dpt on dpt.productkey = rmfg.productkey
where mfgplog.submitkey = @submitkey

end
if @jobtype = 'A' begin

insert into @reporttable
select ji.subjobkey, mfgplog.mfgplogkey, rmfg.mfgreportkey, 
case rmfg.activitysetkey when 2 then isnull(rmfg.rptname, dfreq.freqname + ' ' + catalogsetname + ' ' + productname + ' ' + activitysetname)
else isnull(rmfg.rptname, dfreq.freqname + ' ' + catalogsetname + ' ' + productname + ' ' + activitysetname + ' to ' + geosetname) end,
mfgplog.enddate, 
case when mfgplog.enddate = mfgplog.postperiod then 'Data'
	else 'Revision' end,
sdcatalog.catalogsetkey, catalogsetname
from mfgplog 
inner join @jobinfo as ji on ji.subjobkey = mfgplog.acceptkey
inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
inner join dfreq on dfreq.freqkey = rmfg.freqkey
inner join sdactivity on sdactivity.activitysetkey = rmfg.activitysetkey
inner join sdcatalog on sdcatalog.catalogsetkey = rmfg.catalogsetkey
inner join sdgeo on sdgeo.geosetkey = rmfg.geosetkey
inner join dproduct as dpt on dpt.productkey = rmfg.productkey
where mfgplog.acceptkey = @submitkey

end

update @reporttable
set reportname = reportname + ' - Split Week'
from @reporttable as rt 
inner join rmfg on rmfg.mfgreportkey = rt.mfgreportkey
inner join dcalendar as dc on dc.ddate = rt.enddate
and freqkey = 5 and endofweek = 0


select Catalogsetkey, catalogsetname, mfgplogkey, mfgreportkey, reportname, enddate, datarev from @reporttable as rt inner join @jobinfo as jt on jt.subjobkey = rt.subjobkey

		









