USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[deInputReportMenuTreeXML]    Script Date: 2/22/2018 11:53:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure  [Submission].[deInputReportMenuTreeXML] 
			@Scope int=0,
			@mfgkey int = 0, 
			@ntuser varchar(50)
as
---deInputReportMenuTreeXML -- councils through dates
--creates the menu tree for printing input reports.
--created by Kim Strauss 
--	12/14/05
-- 2/7/18 kds change security to pass in the username


 --Declare @scope int, 
 --@mfgkey int, @ntuser varchar(100)
 --Set @scope = 2017
 --set @mfgkey = 124
 --set @ntuser = 'kstrauss'

 --drop table #r

set nocount on

Declare @begindate smalldatetime,@enddate smalldatetime
Declare @ValidYears table (year int primary key)

declare @admin tinyint
set @admin = isnull((select 1 from duser where ntuser = @ntuser
	and opeiops = 1 ), 0)

insert into @validyears
select Distinct year(enddate)
from mfgplog as f where f.enddate > (year(getdate()) - 2)
and @admin = 0
-- let admin see back to 1994
insert into @validyears
select Distinct year(enddate)
from mfgplog as f where f.enddate > '1/1/1994'
and @admin = 1

insert into @validyears select 0

IF @Scope not in (Select Year from @validyears) begin
	set @scope=0
end
IF @scope=0 begin
	select @begindate = Dateadd(m,-14,getdate()),@enddate=getdate()
end
else begin
	select @enddate = '12/31/'+str(@scope,4)
	select @begindate = '1/1/' + str(@scope,4)	
end


declare @rptSecIn table (mfgReportKey int, begindate smalldatetime,enddate smalldateTime,primary key (mfgreportkey,Begindate))

insert into @rptsecIn
select s.* from dbo.fnSecurityInputReportKeysPassInUser(@ntuser, 'D') as s
inner join rmfg on rmfg.mfgreportkey = s.mfgreportkey
	and rmfg.mfgkey = @mfgkey


Select rm.councilkey,rm.groupkey,rm.catalogkey,rm.freqkey,reportenddate = mfgplog.enddate,rm.calendar,
rm.CouncilName,rm.Freqname,rm.catalogname
into #r
from Dgroup as dg 
inner join vrmfg as rm on rm.groupkey = dg.groupkey
inner join mfgplog on rm.mfgreportkey = mfgplog.mfgreportkey
	and mfgplog.enddate between @begindate and @enddate
inner join dcalendar as dcal on dcal.ddate = mfgplog.enddate
inner join @rptsecIn as rsi on rsi.mfgreportkey = rm.mfgreportkey
	and mfgplog.enddate between rsi.begindate and rsi.enddate
where mfgplog.statuskey >15
and (rm.freqkey <> 5 or dcal.endofweek = 1)	-- make sure it only shows valid end dates for weeklies
and (rm.freqkey <> 4 or (dcal.endofmonth = 1 and rm.calendar = 1) or (dcal.endofwkmonth = 1 and rm.calendar = 0))
group by rm.councilkey,rm.groupkey,rm.catalogkey,rm.freqkey,mfgplog.enddate,rm.calendar,rm.CouncilName,rm.Freqname,rm.catalogname

--don't let portables see dates before 2005
delete from #r
where councilkey = 3
and reportenddate < '1/1/05'
and @admin = 0

update #r
set calendar = 0
where freqkey = 5
 
select
Reporttitle = 'AHAM Company Data Report Menu', 
RunTime = getdate(), 
r.Councilkey, 
r.councilname, 
r.catalogkey, 
r.catalogname, 
r.freqkey, 
Freqname =case when r.calendar = 1 then 'Calendar ' else '' end + r.freqname, 
r.calendar, 
FreqSort = 0-r.freqkey,
r.ReportEndDate, 
DateSort = datediff(dd, reportenddate, '1/1/1990')
from #r as r
where (@scope<>0 or r.reportenddate in (Select top 4 x.ReportEndDate from #r as x where
	x.groupkey=r.groupkey
	and x.catalogkey=r.catalogkey
	and x.freqkey=r.freqkey
	and x.Calendar=R.calendar
	order by x.reportEnddate desc))

select
FilterYearsValue = y.year, 
FilterYearsText = case when y.year=0 then 'Current' else str(y.year,4) end
from @validyears as y where y.year <> @scope

 



























