USE [Ahamoper]
GO

/****** Object:  View [Submission].[vMfgReportPermissions]    Script Date: 2/20/2018 12:18:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create VIEW [Submission].vOpenJobs
AS
SELECT DISTINCT 
sj.subjobkey, mfgkey, requestingusername, jobtype, status, mfgplogkey
from Submission.SubJobExecution as sj
inner join submission.submembers as sm on sm.subjobkey = sj.subjobkey 
where status < 100




GO


