USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickSubmissionSetStatus]    Script Date: 2/8/2018 12:11:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER Procedure  [Submission].[ickSubmissionSetStatus](
	@subjobkey int, 
	@ntuser varchar(50), 
	@plogtable submission.PlogTableType readonly
	)

as

/* ickSubmissionSetStatus changes each of the plogs in the @doc list to the new
	status as specified by @newStatus.  It then returns a list of the documents
	submitted.
	Created by Kim Strauss
	4/16/02
	2/8/18  kds Change schema, remove the xml input, take out the results at the end since the UI ignored them.  
*/

set nocount on
declare	@NewStatus tinyint 
declare @errorcount int
declare @curdate datetime
declare @SubmissionKey int
declare @mfgname varchar(50)
declare @postdate smalldatetime
declare @mfgkey int

declare @ploglist table(
mfgplogkey integer,
postdate smalldatetime,
subjobkey int)

declare @WarningList table(
mfgplogkey integer)

declare @reporttable table(
mfgplogkey int,
mfgreportkey int, 
reportname varchar(250),
enddate smalldatetime,
datarev varchar(20),
catalogsetkey int,
catalogsetname varchar(30))

declare @flashlist table(
mfgplogkey integer primary key, 
flmfgreportkey integer)


declare @errorlist table(
mfgplogkey integer, 
errorcode integer,
Message varchar(250))
 
set @errorcount = 0
set @curdate = getdate()

set @postdate = (select enddate from submission.subjobs
	where subjobkey = @submissionkey)

insert into @ploglist(mfgplogkey, postdate, subjobkey)
select plogkey, @postdate, @subjobkey 
from @plogtable

set @submissionkey = (select top 1 subjobkey from @ploglist)
set @mfgkey = (select mfgkey from submission.subjobs
	where subjobkey = @submissionkey)
set @mfgname = (select mfgname from dmfg 
	inner join submission.subjobs on subjobs.mfgkey = dmfg.mfgkey
	where subjobkey = @submissionkey)

select pl.mfgplogkey, enddate = min(mp.enddate)
into #t
from @ploglist as pl
inner join mfgplog as mp2 on pl.mfgplogkey = mp2.mfgplogkey
inner join mfgplog as mp on mp.mfgreportkey = mp2.mfgreportkey
	and mp.enddate >= mp2.enddate
	and mp.statuskey < 24
	and mp.enddate = mp.postperiod
inner join rmfg on rmfg.mfgreportkey = mp.mfgreportkey
inner join dcalendar on mp.enddate = ddate and ((endofweek = 1 and (rmfg.calendar = 0 or rmfg.freqkey = 5))
	or (endofmonth = 1 and rmfg.calendar =1 and rmfg.freqkey < 5))
group by pl.mfgplogkey

union all

select pl.mfgplogkey, min(mp1.enddate)
from @ploglist as pl
inner join mfgplog as mp2 on pl.mfgplogkey = mp2.mfgplogkey
inner join rmfg as r2 on r2.mfgreportkey = mp2.mfgreportkey
inner join rmfg as r1 on r1.mfgreportkey <> r2.mfgreportkey
		and r1.freqkey = r2.freqkey
		and r1.catalogsetkey = r2.catalogsetkey
		and r1.marketsetkey = r2.marketsetkey
		and r1.mfgkey = r2.mfgkey
inner join mfgplog as mp1 on mp1.mfgreportkey = r1.mfgreportkey
	and mp1.enddate >= mp2.enddate
	and mp1.statuskey < 24
	and mp1.enddate = mp1.postperiod
inner join dcalendar on mp1.enddate = ddate and ((endofweek = 1 and (r1.calendar = 0 or r1.freqkey = 5))
	or (endofmonth = 1 and r1.calendar =1 and r1.freqkey < 5))
where not exists(select * from mfgplog as mp where mp.mfgreportkey = mp2.mfgreportkey
				and mp.statuskey < 24
				and mp.enddate > mp2.enddate
				and mp.enddate <> mp.postperiod)
group by pl.mfgplogkey

update @ploglist
set postdate = enddate
from @ploglist as pl
inner join #t as t on t.mfgplogkey = pl.mfgplogkey


select @newstatus = case when Jobtype='S' then 16 when jobtype='A' then 24 else 0 end from submission.subjobs where subjobkey=@submissionkey

if @newstatus=24 and isnull((Select count(*) from duser where ntuser=@ntuser and opeiops=1),0) < 1 
return (-1)


-- need to check permissions if want to change status to something smaller.
insert into @warninglist
select pl.mfgplogkey
from @ploglist as pl
inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
where mfgplog.statuskey > (@newstatus + 8)


insert into @errorlist
select wl.mfgplogkey, 1, 'User does not have sufficient permission to change this status'
from @Warninglist as wl
where mfgplogkey not in(select distinct wl.mfgplogkey from @warninglist as wl
inner join mfgplog on mfgplog.mfgplogkey = wl.mfgplogkey
inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vmodelpermissions as vmp on vmp.mfgkey = rmfg.mfgkey
	and vmp.productkey = rmfg.productkey
	and ntuser = @ntuser
	and seclevel >= 10)

insert into @errorlist
select pl.mfgplogkey, 1, 'User does not have permission to change this status'
from @ploglist as pl
where mfgplogkey not in(select distinct pl.mfgplogkey from @ploglist as pl
inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vmodelpermissions as vmp on vmp.mfgkey = rmfg.mfgkey
	and vmp.productkey = rmfg.productkey
	and ntuser = @ntuser
	and submitdata = 1)
and mfgplogkey not in(select distinct mfgplogkey from @errorlist)



--Set status.  If the new status is submit or higher and there is no 
--official submission date, set it.  Otherwise, don't. 

Begin tran
if @newstatus >= 16 and @newstatus < 24 begin
  update mfgplog
  set statuskey = @newstatus, 
  statusdate = @curdate,
  OffSubmitDate = isnull(offsubmitdate, @curdate),
  postperiod = isnull(postperiod, postdate),
  submitkey = @submissionkey
  from @ploglist as pl
  inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
  where pl.mfgplogkey not in(select distinct mfgplogkey from @errorlist)	

end
else if @newstatus > 23 begin	-- accepting
  update mfgplog
  set statuskey = @newstatus, 
  statusdate = @curdate,
  acceptkey = @submissionkey
  from @ploglist as pl
  inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
  where pl.mfgplogkey not in(select distinct mfgplogkey from @errorlist)
end
else begin	-- just changing
  update mfgplog
  set statuskey = @newstatus, 
  statusdate = @curdate
  from @ploglist as pl
  inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
  where pl.mfgplogkey not in(select distinct mfgplogkey from @errorlist)
end
commit












