USE [Ahamoper]
GO
/****** Object:  StoredProcedure [dbo].[DataEntryAnalyze]    Script Date: 2/8/2018 1:09:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--dataentryanalyze 1271, 0, '2/14/04'
--dataentryanalyze 0, 16526, '2/28/05'

create PROCEDURE [dbo].[DataEntryAnalyze]
      (
       @subjobkey INT
      ,@mfgreportkey INT
      ,@enddate SMALLDATETIME
      )
AS /* Analyzes a data entry screen for reasonability and matching other reports.
   also can analyze a submission job.  
	Written by Kim Strauss
	5/15/03
	3/17/03	  add more value checking
	7/13/12   Change the 0 report error checking		

list of things to check:
	- comparisons in the comparison file
	- compare with last x time periods for comparative volume
	- on value reports, compare the avg values with past time periods
	- look for missing data points.

For the comparison file checks
	- make list of reports to check
	- make list of comparisons that count
	- make a list of the reports with end dates that are in the comparisons
	- find all of those data points
	- add them all up
	- compare them

*/

-- declare	@subjobkey int,
-- 	@mfgreportkey int,
-- 	@enddate smalldatetime
-- set @subjobkey = 0
-- set @mfgreportkey = 16795
-- set @enddate = '12/31/04' 
-- drop table #pastdata, #fm



      SET nocount ON


      DECLARE @ntuser VARCHAR(50)
      SET @ntuser = dbo.fnntuser()


      DECLARE @rpts TABLE
              (
               mfgreportkey INT
              ,mfgkey INT
              ,enddate SMALLDATETIME
              ,rptperiod INT
              ,freqkey INT
              ,submemberkey INT
              ,PRIMARY KEY (mfgreportkey, enddate)
              )

      DECLARE @rptdetail TABLE
              (
               submemberkey INT
              ,rptcompkey INT
              ,side TINYINT
              ,calendar TINYINT
              ,freqkey TINYINT
              ,enddate SMALLDATETIME
              ,totunits MONEY
              ,PRIMARY KEY (submemberkey, rptcompkey, side, enddate, calendar)
              )

      DECLARE @complist TABLE
              (
               submemberkey INT
              ,rptcompkey INT
              ,totunits MONEY
              ,side TINYINT
              ,enddate SMALLDATETIME
              ,rptperiod INT
              ,PRIMARY KEY (rptcompkey, side, rptperiod, enddate, submemberkey)
              )

      DECLARE @sds TABLE
              (
               rptcompkey INT
              ,rptdetailkey INT
              ,mfgreportkey INT
              ,dim1key INT
              ,dpd1key INT
              ,cont1 TINYINT
              ,dim2key INT
              ,dpd2key INT
              ,cont2 TINYINT
              ,dim3key INT
              ,dpd3key INT
              ,cont3 TINYINT
              ,dim4key INT
              ,dpd4key INT
              ,cont4 TINYINT
              ,dim5key INT
              ,dpd5key INT
              ,cont5 TINYINT
              ,mfgkey INT
              ,productkey INT
              ,PRIMARY KEY (rptcompkey, rptdetailkey, mfgreportkey)
              )

      DECLARE @models TABLE
              (
               submemberkey INT
              ,rptcompkey INT
              ,rptdetailkey INT
              ,mfgreportkey INT
              ,modelkey INT
              ,enddate SMALLDATETIME
              ,ytd TINYINT
              ,freqkey INT
              ,side TINYINT
              ,calendar TINYINT
              ,PRIMARY KEY (mfgreportkey, enddate, modelkey, submemberkey, rptcompkey, rptdetailkey)
              )


      DECLARE @errors TABLE
              (
               submemberkey INT
              ,rptcompkey INT
              ,mfgreportkey1 INT
              ,mfgreportkey2 INT
              ,mfgreportkey3 INT
              ,dataside0 MONEY
              ,dataside1 MONEY
              ,errormessage VARCHAR(512)
              ,errorcode INT
              ,enddate SMALLDATETIME
              ,mfgfactkey INT
              )


--put the ones with acceptable permissions into the @rpts table
      INSERT    INTO @rpts
                SELECT  @mfgreportkey
                       ,0
                       ,@enddate
                       ,0
                       ,0
                       ,0
                WHERE   EXISTS ( SELECT *
                                 FROM   submission.vmfgreportpermissions AS vmp
                                 WHERE  vmp.mfgreportkey = @mfgreportkey
                                        AND ntuser = @ntuser
                                        AND seecodata = 1 )
                        AND @subjobkey = 0

--add the other ones in the big data entry forms
      INSERT    INTO @rpts
                SELECT  r2.mfgreportkey
                       ,0
                       ,@enddate
                       ,0
                       ,0
                       ,0
                FROM    rmfg AS r1
                INNER JOIN rmfg AS r2 ON r1.dinputformkey = r2.dinputformkey
                                         AND r1.mfgkey = r2.mfgkey
                                         AND r1.mfgreportkey <> r2.mfgreportkey
                                         AND @enddate BETWEEN r2.begindate AND r1.enddate
                                         AND r1.mfgreportkey = @mfgreportkey
                WHERE   EXISTS ( SELECT *
                                 FROM   submission.vmfgreportpermissions AS vmp
                                 WHERE  vmp.mfgreportkey = r2.mfgreportkey
                                        AND ntuser = @ntuser
                                        AND seecodata = 1 )
                        AND @subjobkey = 0
                        AND r1.dinputformkey IN (92, 93, 94, 95)

--add the reports for a submission
      INSERT    INTO @rpts
                SELECT DISTINCT
                        mfgplog.mfgreportkey
                       ,mfgkey
                       ,enddate
                       ,0
                       ,0
                       ,submemberskey
                FROM    Submembers
                INNER JOIN mfgplog ON mfgplog.mfgplogkey = submembers.mfgplogkey
                INNER JOIN submission.vmfgreportpermissions AS vmp ON vmp.mfgreportkey = mfgplog.mfgreportkey
                                                           AND ntuser = @ntuser
                                                           AND seecodata = 1
                WHERE   @subjobkey <> 0
                        AND @subjobkey = submembers.subjobkey

--if none with acceptable permissions, write an error
/*if (select count(*) from @rpts) = 0 
begin
insert into @errors(errorcode, errordesc, rowkey, columnkey, mfgreportkey1)
select 1, 'No valid report to analyze - check permissions', 0,0, 0
end
*/
      UPDATE    @rpts
      SET       freqkey = rmfg.freqkey
               ,mfgkey = rmfg.mfgkey
      FROM      @rpts AS rpt
      INNER JOIN rmfg ON rmfg.mfgreportkey = rpt.mfgreportkey
  
      UPDATE    @rpts
      SET       rptperiod = inweek
      FROM      @rpts AS rpt
      INNER JOIN dcalendar AS dc ON dc.ddate = rpt.enddate
                                    AND rpt.freqkey = 5

      UPDATE    @rpts
      SET       rptperiod = inmonth
      FROM      @rpts AS rpt
      INNER JOIN dcalendar AS dc ON dc.ddate = rpt.enddate
                                    AND rpt.freqkey = 4

      UPDATE    @rpts
      SET       rptperiod = inquarter
      FROM      @rpts AS rpt
      INNER JOIN dcalendar AS dc ON dc.ddate = rpt.enddate
                                    AND rpt.freqkey = 3

      UPDATE    @rpts
      SET       rptperiod = 1
      FROM      @rpts AS rpt
      INNER JOIN dcalendar AS dc ON dc.ddate = rpt.enddate
                                    AND rpt.freqkey = 1

-- need to take fiscal into account. 
      UPDATE    @rpts
      SET       rptperiod = inwkmonth
      FROM      @rpts AS rpt
      INNER JOIN rmfg ON rmfg.mfgreportkey = rpt.mfgreportkey
      INNER JOIN dcalendar AS dc ON dc.ddate = rpt.enddate
                                    AND rpt.freqkey = 4
                                    AND rmfg.calendar = 0

      UPDATE    @rpts
      SET       rptperiod = inwkquarter
      FROM      @rpts AS rpt
      INNER JOIN rmfg ON rmfg.mfgreportkey = rpt.mfgreportkey
      INNER JOIN dcalendar AS dc ON dc.ddate = rpt.enddate
                                    AND rpt.freqkey = 3
                                    AND rmfg.calendar = 0


      INSERT    INTO @sds
                SELECT DISTINCT
                        rcd.rptcompkey
                       ,rcd.rptdetailkey
                       ,rcd.mfgreportkey
                       ,dim1key
                       ,dpd1key = dpd1.productdimkey
                       ,cont1 = dpd1.continuous
                       ,dim2key
                       ,dpd2key = dpd2.productdimkey
                       ,cont2 = dpd2.continuous
                       ,dim3key
                       ,dpd3key = dpd3.productdimkey
                       ,cont3 = dpd3.continuous
                       ,dim4key
                       ,dpd4key = dpd4.productdimkey
                       ,cont4 = dpd4.continuous
                       ,dim5key
                       ,dpd5key = dpd5.productdimkey
                       ,cont5 = dpd5.continuous
                       ,rmfg.mfgkey
                       ,rmfg.productkey
                FROM    rptcomparedetail AS rcd
                INNER JOIN rptcomparemaster AS rcm ON rcm.rptcompkey = rcd.rptcompkey
                INNER JOIN @rpts AS rpt ON rpt.mfgreportkey = rcm.mfgreportkey1
                                           OR rpt.mfgreportkey = rcm.mfgreportkey2
                                           OR rpt.mfgreportkey = rcm.mfgreportkey3
                                           OR rpt.mfgreportkey = rcm.mfgreportkey4
                                           OR rpt.mfgreportkey = rcm.mfgreportkey5
                INNER JOIN rmfg ON rcd.mfgreportkey = rmfg.mfgreportkey
                                   AND NOT (geosetkey = 3
                                            AND @subjobkey = 0)
                INNER JOIN sdsize ON rmfg.sizesetkey = sdsize.sizesetkey
                LEFT JOIN dproductdim AS dpd1 ON rmfg.productkey = dpd1.productkey
                                                 AND sdsize.dim1key = dpd1.dimkey
                LEFT JOIN dproductdim AS dpd2 ON rmfg.productkey = dpd2.productkey
                                                 AND sdsize.dim2key = dpd2.dimkey
                LEFT JOIN dproductdim AS dpd3 ON rmfg.productkey = dpd3.productkey
                                                 AND sdsize.dim3key = dpd3.dimkey
                LEFT JOIN dproductdim AS dpd4 ON rmfg.productkey = dpd4.productkey
                                                 AND sdsize.dim4key = dpd4.dimkey
                LEFT JOIN dproductdim AS dpd5 ON rmfg.productkey = dpd5.productkey
                                                 AND sdsize.dim5key = dpd5.dimkey
            
      INSERT    INTO @models
                SELECT DISTINCT
                        rpt.submemberkey
                       ,rcm.rptcompkey
                       ,rcd.rptdetailkey
                       ,rcd.mfgreportkey
                       ,dmodel.modelkey
                       ,rpt.enddate
                       ,rcm.ytd
                       ,rmfg.freqkey
                       ,side
                       ,rcd.calendar
                FROM    @rpts AS rpt
                INNER JOIN rptcomparemaster AS rcm ON rpt.mfgreportkey IN (rcm.mfgreportkey1, rcm.mfgreportkey2,
                                                                           rcm.mfgreportkey3, rcm.mfgreportkey4,
                                                                           rcm.mfgreportkey5)
                INNER JOIN rptcomparedetail AS rcd ON rcm.rptcompkey = rcd.rptcompkey
                INNER JOIN rmfg ON rmfg.mfgreportkey = rcd.mfgreportkey
                INNER JOIN dmodel ON dmodel.mfgkey = rmfg.mfgkey
                                     AND dmodel.productkey = rmfg.productkey
                INNER JOIN @sds AS sds ON sds.rptcompkey = rcd.rptcompkey
                                          AND sds.mfgreportkey = rcd.mfgreportkey
                                          AND sds.rptdetailkey = rcd.rptdetailkey
                LEFT JOIN dmodelsize AS dms1 ON dms1.modelkey = dmodel.modelkey
                                                AND dms1.productdimkey = sds.dpd1key
                LEFT JOIN dsize AS dsize1 ON rcd.size1key = dsize1.sizekey
                LEFT JOIN dmodelsize AS dms2 ON dmodel.modelkey = dms2.modelkey
                                                AND sds.dpd2key = dms2.productdimkey
                LEFT JOIN dsize AS dsize2 ON rcd.size2key = dsize2.sizekey
                LEFT JOIN dmodelsize AS dms3 ON dms3.modelkey = dmodel.modelkey
                                                AND dms3.productdimkey = sds.dpd3key
                LEFT JOIN dsize AS dsize3 ON rcd.size3key = dsize3.sizekey
                LEFT JOIN dmodelsize AS dms4 ON dms4.modelkey = dmodel.modelkey
                                                AND dms4.productdimkey = sds.dpd4key
                LEFT JOIN dsize AS dsize4 ON rcd.size4key = dsize4.sizekey
                LEFT JOIN dmodelsize AS dms5 ON dms5.modelkey = dmodel.modelkey
                                                AND dms5.productdimkey = sds.dpd5key
                LEFT JOIN dsize AS dsize5 ON rcd.size5key = dsize5.sizekey
                WHERE   ((dms1.sizekey = rcd.size1key
                          AND cont1 = 0)
                         OR rcd.size1key IS NULL
                         OR rcd.size1key = 0
                         OR (dms1.modelsizevalue BETWEEN dsize1.sizelowerlimit
                                                 AND     dsize1.sizeupperlimit
                             AND cont1 = 1
                             AND dms1.sizekey = 0))
                        AND ((dms2.sizekey = rcd.size2key
                              AND cont2 = 0)
                             OR (rcd.size2key IS NULL
                                 OR rcd.size2key = 0)-- and dms2.modelkey is null)
                             OR (dms2.modelsizevalue BETWEEN dsize2.sizelowerlimit
                                                     AND     dsize2.sizeupperlimit
                                 AND cont2 = 1
                                 AND dms2.sizekey = 0))
                        AND ((dms3.sizekey = rcd.size3key
                              AND cont3 = 0)
                             OR (rcd.size3key IS NULL
                                 OR rcd.size3key = 0)--  and dms3.modelkey is null)
                             OR (dms3.modelsizevalue BETWEEN dsize3.sizelowerlimit
                                                     AND     dsize3.sizeupperlimit
                                 AND cont3 = 1
                                 AND dms3.sizekey = 0))
                        AND ((dms4.sizekey = rcd.size4key
                              AND cont4 = 0)
                             OR (rcd.size4key IS NULL
                                 OR rcd.size4key = 0) -- and dms4.modelkey is null)
                             OR (dms4.modelsizevalue BETWEEN dsize4.sizelowerlimit
                                                     AND     dsize4.sizeupperlimit
                                 AND cont4 = 1
                                 AND dms4.sizekey = 0))
                        AND ((dms5.sizekey = rcd.size5key
                              AND cont5 = 0)
                             OR (rcd.size5key IS NULL
                                 OR rcd.size5key = 0) -- and dms5.modelkey is null)
                             OR (dms5.modelsizevalue BETWEEN dsize5.sizelowerlimit
                                                     AND     dsize5.sizeupperlimit
                                 AND cont5 = 1
                                 AND dms5.sizekey = 0))
---order by rcm.rptcompkey, rcd.rptdetailkey, rcd.mfgreportkey, rpt.enddate, dmodel.modelkey

      INSERT    INTO @rptdetail
                SELECT  DISTINCT
                        md.submemberkey
                       ,md.rptcompkey
                       ,side
                       ,calendar
                       ,md.freqkey
                       ,mfgplog.enddate
                       ,totunits = sum(munits)
                FROM    @models AS md
                INNER JOIN mfgplog ON md.mfgreportkey = mfgplog.mfgreportkey
                                      AND datepart(yy, mfgplog.enddate) = datepart(yy, md.enddate)	--only do the ones for the current year
                                      AND mfgplog.enddate = postperiod
--		and md.enddate = mfgplog.enddate
                                      AND mfgplog.statuskey > 0
                INNER JOIN fmanufacturer AS fm ON fm.mfgreportkey = md.mfgreportkey
                                                  AND (fm.enddate = mfgplog.enddate) --or (fm.enddate < mfgplog.enddate and md.ytd = 1))
                                                  AND datepart(yy, fm.enddate) = datepart(yy, mfgplog.enddate)
                                                  AND fm.modelkey = md.modelkey
                GROUP BY md.submemberkey
                       ,md.rptcompkey
                       ,side
                       ,calendar
                       ,md.freqkey
                       ,mfgplog.enddate

-- was going to assume a 0 on a zero submission, but instead we decided to outlaw 0 submissions
-- insert into @rptdetail
-- select  distinct md.submemberkey, md.rptcompkey, side, md.calendar, md.freqkey, mfgplog.enddate, totunits = 0
-- from @models as md
-- inner join mfgplog on md.mfgreportkey = mfgplog.mfgreportkey
-- 		and datepart(yy, mfgplog.enddate) = datepart(yy, md.enddate)	--only do the ones for the current year
-- 		and mfgplog.enddate = postperiod
-- --		and md.enddate = mfgplog.enddate
-- 		and mfgplog.statuskey > 0
-- inner join rmfg on md.mfgreportkey = rmfg.mfgreportkey
-- 	and rmfg.master = 1
-- where not exists(select md.submemberkey from @rptdetail as r
-- 	where r.submemberkey = md.submemberkey
-- 	and r.rptcompkey = md.rptcompkey
-- 	and r.side = md.side
-- 	and r.calendar = md.calendar
-- 	and r.freqkey = md.freqkey
-- 	and r.enddate = mfgplog.enddate)

      INSERT    INTO @complist
--select distinct rcd.rptcompkey, sum(totunits), side, rpt.enddate, rptperiod
                SELECT DISTINCT
                        isnull(rpt.submemberkey, 0)
                       ,rcd.rptcompkey
                       ,sum(totunits)
                       ,side
                       ,rpt.enddate
                       ,rptperiod
--select *
                FROM    rptcomparemaster AS rcm
                INNER JOIN @rpts AS rpt ON rpt.mfgreportkey = rcm.mfgreportkey1
                                           OR rpt.mfgreportkey = rcm.mfgreportkey2
                                           OR rpt.mfgreportkey = rcm.mfgreportkey3
                                           OR rpt.mfgreportkey = rcm.mfgreportkey4
                                           OR rpt.mfgreportkey = rcm.mfgreportkey5
                INNER JOIN @rptdetail AS rcd ON rcd.rptcompkey = rcm.rptcompkey
                                                AND rcd.submemberkey = rpt.submemberkey
                INNER JOIN rmfg ON rpt.mfgreportkey = rmfg.mfgreportkey
                INNER JOIN dcalendar AS dc ON dc.ddate = rcd.enddate
--inner join vmfgcalendar as vmc on vmc.ddate = rcd.enddate
--	and rpt.mfgkey = vmc.mfgkey
                                              AND dc.year = datepart(yy, rpt.enddate)
                                              AND rcd.freqkey >= rpt.freqkey
                                              AND (rpt.freqkey = 1
                                                   OR (rcm.ytd = 1
                                                       AND rcd.freqkey <> 5
                                                       AND rcd.enddate <= rpt.enddate)
                                                   OR (rcm.ytd = 1
                                                       AND rcd.freqkey = rpt.freqkey
                                                       AND rcd.enddate <= rpt.enddate)
                                                   OR (rcm.ytd = 1
                                                       AND rcd.freqkey = 5
                                                       AND rpt.freqkey = 3
                                                       AND dc.inquarter <= rptperiod
                                                       AND rmfg.calendar = 1)
                                                   OR (rcm.ytd = 1
                                                       AND rcd.freqkey = 5
                                                       AND rpt.freqkey = 3
                                                       AND dc.inwkquarter <= rptperiod
                                                       AND rmfg.calendar = 0)
                                                   OR (rcm.ytd = 1
                                                       AND rcd.freqkey = 5
                                                       AND rpt.freqkey = 4
                                                       AND dc.inmonth <= rptperiod
                                                       AND rmfg.calendar = 1)
                                                   OR (rcm.ytd = 1
                                                       AND rcd.freqkey = 5
                                                       AND rpt.freqkey = 4
                                                       AND dc.inwkmonth <= rptperiod
                                                       AND rmfg.calendar = 0)
                                                   OR (rcm.ytd = 0
                                                       AND rcd.freqkey = rpt.freqkey
                                                       AND rpt.enddate = rcd.enddate)
                                                   OR (rcm.ytd = 0
                                                       AND rcd.freqkey = 4
                                                       AND rpt.freqkey = 3
                                                       AND dc.inquarter = rptperiod
                                                       AND rmfg.calendar = 1)
                                                   OR (rcm.ytd = 0
                                                       AND rcd.freqkey = 4
                                                       AND rpt.freqkey = 3
                                                       AND dc.inwkquarter = rptperiod
                                                       AND rmfg.calendar = 0)
                                                   OR (rcm.ytd = 0
                                                       AND rcd.freqkey = 5
                                                       AND rpt.freqkey = 3
                                                       AND dc.inquarter = rptperiod
                                                       AND rmfg.calendar = 1)
                                                   OR (rcm.ytd = 0
                                                       AND rcd.freqkey = 5
                                                       AND rpt.freqkey = 4
                                                       AND dc.inmonth = rptperiod
                                                       AND rmfg.calendar = 1)
                                                   OR (rcm.ytd = 0
                                                       AND rcd.freqkey = 5
                                                       AND rpt.freqkey = 3
                                                       AND dc.inwkquarter = rptperiod
                                                       AND rmfg.calendar = 0)
                                                   OR (rcm.ytd = 0
                                                       AND rcd.freqkey = 5
                                                       AND rpt.freqkey = 4
                                                       AND dc.inwkmonth = rptperiod
                                                       AND rmfg.calendar = 0))
/*inner join vmfgcalendar as dc2 on dc2.ddate = rcd.enddate
		and rpt.mfgkey = dc2.mfgkey
		and rptyear= datepart(yy, rpt.enddate)

--		and (((ytd = 0 and 
		and ((rcd.freqkey = 5
		and (rpt.freqkey = 4 and dc2.inmonth = rptperiod)
		or (rpt.freqkey = 3 and dc2.inquarter = rptperiod))
--		or ((ytd = 1 and rcd.freqkey = 5)
--		and (rpt.freqkey = 4 and dc2.inmonth <= rptperiod)
--		or (rpt.freqkey = 3 and dc2.inquarter <= rptperiod))
		or (rpt.freqkey =1)
		or (rpt.enddate = dc2.ddate and rcd.freqkey <> 5))
--		and ((dc2.ddate = rpt.enddate 
--		and (calendar = 1 or rcd.freqkey <> 5)
--		or (calendar =1 and rcd.freqkey = 5 and ytd = 0 and rpt.freqkey = 4 and rptperiod = dc2.inmonth)
--		or (calendar = 0 and rcd.freqkey = 5 
--				and ((endofwkmonth = 1 and rpt.freqkey = 4 and rptperiod = dc2.inwkmonth)
--				or (ytd = 0 and rpt.freqkey = 4 and rptperiod = dc2.inwkmonth)
--				or (ytd = 1 and rpt.freqkey = 4 and rptperiod < dc2.inwkmonth)
--				or (rpt.freqkey = 3 and rptperiod = dc2.inwkquarter)
--				or (ytd = 1 and rpt.freqkey = 3 and rptperiod < dc2.inwkquarter)
--				or (endofwkquarter = 1 and rpt.freqkey = 3 and rptperiod = dc2.inwkquarter)))))
--where rcd.rptcompkey = 1132
*/GROUP BY              rpt.submemberkey
                       ,rcd.rptcompkey
                       ,side
                       ,rpt.enddate
                       ,rptperiod--
--order by rcd.rptcompkey, side, rpt.enddate

--select * from @models
--select * from @rptdetail
--select * from @complist order by rptcompkey, side
--select * from @rpts
/*
insert into @errors(rptcompkey, dataside0, dataside1, errorcode, errormessage, enddate)
select l1.rptcompkey, l1.totunits, null, 6, 'Unable to compare data - no existing data for one side',
l1.enddate
from @complist as l1
where not exists(select * from @complist as l2 where l1.rptcompkey = l2.rptcompkey
		and l1.rptperiod = l2.rptperiod
		and l1.side <> l2.side)
*/
      INSERT    INTO @errors
                (submemberkey
                ,rptcompkey
                ,dataside0
                ,dataside1
                ,errorcode
                ,errormessage
                ,enddate
                ,mfgreportkey1
                ,mfgreportkey2
                ,mfgreportkey3)
                SELECT  l1.submemberkey
                       ,l1.rptcompkey
                       ,l1.totunits
                       ,l2.totunits
                       ,case WHEN @subjobkey = 0 THEN 1
                             ELSE rcm.error
                        END
                       ,case WHEN @subjobkey > 0
                             THEN rcm.errormsg + ' ' + cast(cast(round(l1.totunits, 0) AS INTEGER) AS VARCHAR) + ' vs '
                                  + cast(cast(round(l2.totunits, 0) AS INTEGER) AS VARCHAR)
                             ELSE rcm.errormsg
                        END
                       ,l1.enddate
                       ,rcm.mfgreportkey1
                       ,rcm.mfgreportkey2
                       ,rcm.mfgreportkey3
                FROM    @complist AS l1
                INNER JOIN @complist AS l2 ON l1.rptcompkey = l2.rptcompkey
                                              AND l1.rptperiod = l2.rptperiod
                                              AND l1.side = 0
                                              AND l2.side = 1
                                              AND l1.submemberkey = l2.submemberkey
                INNER JOIN rptcomparemaster AS rcm ON rcm.rptcompkey = l1.rptcompkey
                WHERE   l1.totunits <> l2.totunits
                        AND rcm.relationship = '='

      INSERT    INTO @errors
                (submemberkey
                ,rptcompkey
                ,dataside0
                ,dataside1
                ,errorcode
                ,errormessage
                ,enddate
                ,mfgreportkey1
                ,mfgreportkey2
                ,mfgreportkey3)
                SELECT  l1.submemberkey
                       ,l1.rptcompkey
                       ,l1.totunits
                       ,l2.totunits
                       ,case WHEN @subjobkey = 0 THEN 1
                             ELSE rcm.error
                        END
                       ,case WHEN @subjobkey > 0
                             THEN rcm.errormsg + ' ' + cast(cast(round(l1.totunits, 0) AS INTEGER) AS VARCHAR) + ' vs '
                                  + cast(cast(round(l2.totunits, 0) AS INTEGER) AS VARCHAR)
                             ELSE rcm.errormsg
                        END
                       ,l1.enddate
                       ,rcm.mfgreportkey1
                       ,rcm.mfgreportkey2
                       ,rcm.mfgreportkey3
                FROM    @complist AS l1
                INNER JOIN @complist AS l2 ON l1.rptcompkey = l2.rptcompkey
                                              AND l1.rptperiod = l2.rptperiod
                                              AND l1.side = 0
                                              AND l2.side = 1
                                              AND l1.submemberkey = l2.submemberkey
                INNER JOIN rptcomparemaster AS rcm ON rcm.rptcompkey = l1.rptcompkey
                WHERE   l1.totunits = l2.totunits
                        AND (rcm.relationship = '<>'
                             OR rcm.relationship = '<'
                             OR rcm.relationship = '>')

      INSERT    INTO @errors
                (submemberkey
                ,rptcompkey
                ,dataside0
                ,dataside1
                ,errorcode
                ,errormessage
                ,enddate
                ,mfgreportkey1
                ,mfgreportkey2
                ,mfgreportkey3)
                SELECT  l1.submemberkey
                       ,l1.rptcompkey
                       ,l1.totunits
                       ,l2.totunits
                       ,case WHEN @subjobkey = 0 THEN 1
                             ELSE rcm.error
                        END
                       ,case WHEN @subjobkey > 0
                             THEN rcm.errormsg + ' ' + cast(cast(round(l1.totunits, 0) AS INTEGER) AS VARCHAR) + ' vs '
                                  + cast(cast(round(l2.totunits, 0) AS INTEGER) AS VARCHAR)
                             ELSE rcm.errormsg
                        END
                       ,l1.enddate
                       ,rcm.mfgreportkey1
                       ,rcm.mfgreportkey2
                       ,rcm.mfgreportkey3
                FROM    @complist AS l1
                INNER JOIN @complist AS l2 ON l1.rptcompkey = l2.rptcompkey
                                              AND l1.rptperiod = l2.rptperiod
                                              AND l1.side = 0
                                              AND l2.side = 1
                                              AND l2.submemberkey = l1.submemberkey
                INNER JOIN rptcomparemaster AS rcm ON rcm.rptcompkey = l1.rptcompkey
                WHERE   l1.totunits < l2.totunits
                        AND (rcm.relationship = '=>'
                             OR rcm.relationship = '>'
                             OR rcm.relationship = '>=')

      INSERT    INTO @errors
                (submemberkey
                ,rptcompkey
                ,dataside0
                ,dataside1
                ,errorcode
                ,errormessage
                ,enddate
                ,mfgreportkey1
                ,mfgreportkey2
                ,mfgreportkey3)
                SELECT  l1.submemberkey
                       ,l1.rptcompkey
                       ,l1.totunits
                       ,l2.totunits
                       ,case WHEN @subjobkey = 0 THEN 1
                             ELSE rcm.error
                        END
                       ,case WHEN @subjobkey > 0
                             THEN rcm.errormsg + ' ' + cast(cast(round(l1.totunits, 0) AS INTEGER) AS VARCHAR) + ' vs '
                                  + cast(cast(round(l2.totunits, 0) AS INTEGER) AS VARCHAR)
                             ELSE rcm.errormsg
                        END
                       ,l1.enddate
                       ,rcm.mfgreportkey1
                       ,rcm.mfgreportkey2
                       ,rcm.mfgreportkey3
                FROM    @complist AS l1
                INNER JOIN @complist AS l2 ON l1.rptcompkey = l2.rptcompkey
                                              AND l1.rptperiod = l2.rptperiod
                                              AND l1.side = 0
                                              AND l2.side = 1
                                              AND l1.submemberkey = l2.submemberkey
                INNER JOIN rptcomparemaster AS rcm ON rcm.rptcompkey = l1.rptcompkey
                WHERE   l1.totunits > l2.totunits
                        AND (rcm.relationship = '<='
                             OR rcm.relationship = '<'
                             OR rcm.relationship = '=<')

--make sure value exists if it should
      INSERT    INTO @errors
                (submemberkey
                ,mfgfactkey
                ,mfgreportkey1
                ,errormessage
                ,errorcode
                ,enddate
                ,dataside0)
                SELECT DISTINCT
                        rpt.submemberkey
                       ,mfgfactkey
                       ,fm.mfgreportkey
                       ,'Value should not be 0 when units is not'
                       ,2
                       ,fm.enddate
                       ,fm.munits
                FROM    fmanufacturer AS fm
                INNER JOIN @rpts AS rpt ON rpt.mfgreportkey = fm.mfgreportkey
                                           AND rpt.enddate = fm.enddate
                INNER JOIN rmfg ON rmfg.mfgreportkey = rpt.mfgreportkey
                                   AND rmfg.measuresetkey = 4
                                   AND munits > 0
                                   AND mvalue = 0

--select * from @errors


      SELECT    fm.mfgreportkey
               ,marketkey
               ,activitykey
               ,modelkey
               ,geokey
               ,channelkey
               ,usekey
               ,catalogkey
               ,totdata = sum(munits)
               ,numpoints = count(*)
               ,mindata = min(munits)
               ,maxdata = max(munits)
               ,totalvalue = sum(mvalue)
      INTO      #pastdata
      FROM      fmanufacturer AS fm
      WHERE     EXISTS ( SELECT 1
                         FROM   @rpts AS rpt
                         WHERE  rpt.mfgreportkey = fm.mfgreportkey
                                AND fm.enddate BETWEEN dateadd(yy, -1, rpt.enddate) AND dateadd(dd, -1, rpt.enddate) )
      GROUP BY  mfgreportkey
               ,marketkey
               ,activitykey
               ,modelkey
               ,geokey
               ,channelkey
               ,usekey
               ,catalogkey

      SELECT    fm.*
               ,rpt.submemberkey
      INTO      #fm
      FROM      fmanufacturer AS fm
      INNER JOIN rmfg ON rmfg.mfgreportkey = fm.mfgreportkey
                         AND rmfg.status = 'A'		--dont do this check on estimated data
      INNER JOIN @rpts AS rpt ON rpt.mfgreportkey = fm.mfgreportkey
                                 AND rpt.enddate = fm.enddate

      INSERT    INTO @errors
                (submemberkey
                ,mfgfactkey
                ,mfgreportkey1
                ,errormessage
                ,errorcode
                ,enddate
                ,dataside0)
                SELECT DISTINCT
                        fm.submemberkey
                       ,mfgfactkey
                       ,fm.mfgreportkey
                       ,'Data point is more than 50% lower than the average for the past year'
                       ,2
                       ,fm.enddate
                       ,fm.munits
                FROM    #fm AS fm
                INNER JOIN #pastdata AS pd ON pd.mfgreportkey = fm.mfgreportkey
                                              AND pd.marketkey = fm.marketkey
                                              AND pd.activitykey = fm.activitykey
                                              AND pd.modelkey = fm.modelkey
                                              AND pd.geokey = fm.geokey
                                              AND pd.channelkey = fm.channelkey
                                              AND pd.usekey = fm.usekey
                                              AND pd.catalogkey = fm.catalogkey
                                              AND (fm.munits < (totdata / numpoints) * .5)-- or fm.munits < mindata)
                WHERE   numpoints > 0

      INSERT    INTO @errors
                (submemberkey
                ,mfgfactkey
                ,mfgreportkey1
                ,errormessage
                ,errorcode
                ,enddate
                ,dataside0)
                SELECT DISTINCT
                        fm.submemberkey
                       ,mfgfactkey
                       ,fm.mfgreportkey
                       ,'Data point is more than 50% higher than the average of the past year'
                       ,3
                       ,fm.enddate
                       ,fm.munits
                FROM    #fm AS fm
                INNER JOIN #pastdata AS pd ON pd.mfgreportkey = fm.mfgreportkey
                                              AND pd.marketkey = fm.marketkey
                                              AND pd.activitykey = fm.activitykey
                                              AND pd.modelkey = fm.modelkey
                                              AND pd.geokey = fm.geokey
                                              AND pd.channelkey = fm.channelkey
                                              AND pd.usekey = fm.usekey
                                              AND pd.catalogkey = fm.catalogkey
                                              AND (fm.munits > (totdata / numpoints) * 1.5) -- or fm.munits > maxdata)
                WHERE   numpoints > 0

      INSERT    INTO @errors
                (submemberkey
                ,mfgfactkey
                ,mfgreportkey1
                ,errormessage
                ,errorcode
                ,enddate
                ,dataside0)
                SELECT DISTINCT
                        fm.submemberkey
                       ,mfgfactkey
                       ,fm.mfgreportkey
                       ,'Average value is larger than usual'
                       ,3
                       ,fm.enddate
                       ,fm.munits
                FROM    #fm AS fm
                INNER JOIN #pastdata AS pd ON pd.mfgreportkey = fm.mfgreportkey
                                              AND pd.marketkey = fm.marketkey
                                              AND pd.activitykey = fm.activitykey
                                              AND pd.modelkey = fm.modelkey
                                              AND pd.geokey = fm.geokey
                                              AND pd.channelkey = fm.channelkey
                                              AND pd.usekey = fm.usekey
                                              AND pd.catalogkey = fm.catalogkey
                WHERE   fm.mvalue <> 0
                        AND fm.munits <> 0
                        AND totdata <> 0
                        AND totalvalue <> 0
                        AND (case WHEN fm.munits = 0 THEN 0
                                  ELSE fm.mvalue / fm.munits
                             END) > (totalvalue / totdata) * 1.4

      INSERT    INTO @errors
                (submemberkey
                ,mfgfactkey
                ,mfgreportkey1
                ,errormessage
                ,errorcode
                ,enddate
                ,dataside0)
                SELECT DISTINCT
                        fm.submemberkey
                       ,mfgfactkey
                       ,fm.mfgreportkey
                       ,'Average value is smaller than usual'
                       ,3
                       ,fm.enddate
                       ,fm.munits
                FROM    #fm AS fm
                INNER JOIN #pastdata AS pd ON pd.mfgreportkey = fm.mfgreportkey
                                              AND pd.marketkey = fm.marketkey
                                              AND pd.activitykey = fm.activitykey
                                              AND pd.modelkey = fm.modelkey
                                              AND pd.geokey = fm.geokey
                                              AND pd.channelkey = fm.channelkey
                                              AND pd.usekey = fm.usekey
                                              AND pd.catalogkey = fm.catalogkey
                WHERE   fm.mvalue <> 0
                        AND fm.munits <> 0
                        AND totdata <> 0
                        AND totalvalue <> 0
                        AND (case WHEN fm.munits = 0 THEN 0
                                  ELSE fm.mvalue / fm.munits
                             END) < (totalvalue / totdata) * 0.6


-- put errors in correct place

      IF @subjobkey = 0
         OR @subjobkey IS NULL 
         BEGIN

               INSERT   INTO @errors
                        (mfgreportkey1
                        ,errormessage
                        ,errorcode
                        ,enddate)
                        SELECT DISTINCT
                                mfgreportkey
                               ,'Data has been saved and there are no apparent errors.'
                               ,0
                               ,enddate
                        FROM    @rpts AS rpt
                        WHERE   NOT EXISTS ( SELECT *
                                             FROM   @errors )
               UPDATE   @errors
               SET      mfgreportkey1 = rcm.mfgreportkey1
                       ,mfgreportkey2 = rcm.mfgreportkey2
                       ,mfgreportkey3 = rcm.mfgreportkey3
               FROM     @errors AS er
               INNER JOIN rptcomparemaster AS rcm ON er.rptcompkey = rcm.rptcompkey

               DECLARE @delrpts TABLE (mfgreportkey INT)

               INSERT   INTO @delrpts
                        SELECT DISTINCT
                                mfgreportkey1
                        FROM    rpterrors
                        WHERE   mfgreportkey1 IN (SELECT    mfgreportkey
                                                  FROM      @rpts)
                                OR mfgreportkey2 IN (SELECT mfgreportkey
                                                     FROM   @rpts)
                                OR mfgreportkey3 IN (SELECT mfgreportkey
                                                     FROM   @rpts)

               DELETE   FROM rpterrors
               WHERE    mfgreportkey1 IN (SELECT    mfgreportkey
                                          FROM      @delrpts)

               INSERT   INTO rpterrors
                        (rptcompkey
                        ,rpt1total
                        ,rpt2total
                        ,errorcode
                        ,errordesc
                        ,mfgreportkey1
                        ,mfgreportkey2
                        ,mfgreportkey3
                        ,enddate
                        ,mfgfactkey)
                        SELECT  isnull(rptcompkey, 0)
                               ,dataside0
                               ,dataside1
                               ,errorcode
                               ,errormessage
                               ,mfgreportkey1
                               ,mfgreportkey2
                               ,mfgreportkey3
                               ,@enddate
                               ,mfgfactkey
                        FROM    @errors
         END

      IF @subjobkey > 0 
         BEGIN


               INSERT   INTO submission.suberrors
                        (subresultskey
                        ,error
                        ,warning
                        ,mfgkey
                        ,note)
                        SELECT  subresultskey
                               ,case WHEN errorcode > 1 THEN 0
                                     ELSE errorcode
                                END
                               ,case WHEN errorcode = 1 THEN 0
                                     ELSE errorcode
                                END
                               ,rmfg.mfgkey
                               ,errormessage
                        FROM    @errors AS er
                        INNER JOIN @rpts AS rpt ON (er.mfgreportkey1 = rpt.mfgreportkey
                                                    OR er.mfgreportkey2 = rpt.mfgreportkey
                                                    OR er.mfgreportkey3 = rpt.mfgreportkey)
                                                   AND er.enddate = rpt.enddate
                                                   AND rpt.submemberkey = er.submemberkey
                        INNER JOIN submission.subresults ON rpt.submemberkey = subresults.submemberkey
                        INNER JOIN rmfg ON rmfg.mfgreportkey = rpt.mfgreportkey

--special case for 0 submissions
               INSERT   INTO submission.suberrors
                        (subresultskey
                        ,error
                        ,warning
                        ,mfgkey
                        ,note)
                        SELECT  subresultskey
                               , 
--case when master = 1 and createweekly = 0 then 0 else 1 end, 
                                case WHEN councilkey = 4 THEN 0
                                     ELSE 1
                                END
                               , 
--case when master = 1 and createweekly = 0 then 1 else 0 end, 
                                case WHEN councilkey = 4 THEN 1
                                     ELSE 0
                                END
                               ,rmfg.mfgkey
                               , 
-- case when master = 1 and createweekly = 0 then 'No data entered - submitting 0 report'
-- else 'Unable to submit a 0 report.  Data must be entered. ' end
                                case WHEN councilkey = 4 THEN 'No Data entered for this report'
                                     ELSE 'Unable to submit a 0 report.  Data must be entered. '
                                END
                        FROM    @rpts AS rpt
                        INNER JOIN submission.subresults ON rpt.submemberkey = subresults.submemberkey
                        INNER JOIN rmfg ON rmfg.mfgreportkey = rpt.mfgreportkey
                        INNER JOIN dmgroup AS dm ON dm.productkey = rmfg.productkey
                        INNER JOIN dgroup AS dg ON dg.groupkey = dm.groupkey
                        WHERE   NOT EXISTS ( SELECT *
                                             FROM   at_fmanufacturer AS atf
                                             WHERE  atf.mfgreportkey = rmfg.mfgreportkey
                                                    AND rpt.enddate = atf.enddate )


               DELETE   se
               FROM     submission.suberrors AS se
               INNER JOIN submission.subresults AS sr ON se.subresultskey = sr.subresultskey
               INNER JOIN @rpts AS rpt ON rpt.submemberkey = sr.submemberkey
               INNER JOIN submission.suberrors AS se2 ON se2.SubErrorKey <> se.SubErrorKey
                                              AND se2.SubResultsKey = se.SubResultsKey
                                              AND se2.Note = 'Unable to submit a 0 report.  Data must be entered. '


         END

--select * from @errors
--select * from @complist
--select * from @rptdetail
--select * from @rpts
--select * from @pastdata

