USE [Ahamoper]
GO
/****** Object:  StoredProcedure [dbo].[DataEntryMfgListXML]    Script Date: 2/8/2018 1:13:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Procedure  [dbo].[DataEntryMfgListXML] (
	@DESubmission char(1)='D')
as

/*  DataEntryMfgListXML creates a list of all of the manufacturers
     the user can do data entry or submission for based on the DESubmission flag
	Created by Kim Strauss
		   8/27/04
		   10/3/11  Change the submission list to not include obsolete companies.
*/

--declare @desubmission char(1)
--set @desubmission = 'S' 

set nocount on
declare @ntuser varchar(50)
declare @firstdate smalldatetime

set @ntuser = dbo.fnNTUSER()
set @firstdate = (select min(ddate) from dcalendar
		where year(ddate)+2 = year(getdate()))
--select @firstdate

declare @mfgTable  table(
  mfgname varchar(120),
  mfgkey integer)

declare @ProductTable  table(
  mfgkey integer,
  groupkey integer,
  productname varchar(120),
  productkey integer)


if @desubmission = 'S' begin
insert into @productTable
select distinct vmp.mfgkey, dmgroup.groupkey, dproduct.productname, dproduct.productkey
from dproduct
inner join dmgroup on dmgroup.productkey = dproduct.productkey
inner join submission.vModelPermissions as vmp on dproduct.productkey = vmp.productkey
		and @ntuser = vmp.ntuser
		and SubmitData = 1
--		and @DESubmission = 'S'
inner join rmfg on vmp.mfgkey = rmfg.mfgkey 
		and vmp.productkey = rmfg.productkey
		and rmfg.enddate > dateadd(yy, -3, getdate())
group by vmp.mfgkey, dmgroup.groupkey, dproduct.productname, dproduct.productkey

end
if @desubmission = 'D' begin

insert into @productTable
select distinct vmp.mfgkey, dmgroup.groupkey, dproduct.productname, dproduct.productkey
from dproduct
inner join dmgroup on dmgroup.productkey = dproduct.productkey
inner join submission.vModelPermissions as vmp on dproduct.productkey = vmp.productkey
		and @ntuser = vmp.ntuser
		and SeeCoData = 1
--		and @DESubmission = 'D'
inner join rmfg on vmp.mfgkey = rmfg.mfgkey 
		and vmp.productkey = rmfg.productkey
		and rmfg.enddate > @firstdate
group by vmp.mfgkey, dmgroup.groupkey, dproduct.productname, dproduct.productkey
end
if @desubmission = 'M' begin

insert into @productTable
select distinct vmp.mfgkey, dmgroup.groupkey, dproduct.productname, dproduct.productkey
from dproduct
inner join dmgroup on dmgroup.productkey = dproduct.productkey
inner join submission.vModelPermissions as vmp on dproduct.productkey = vmp.productkey
		and @ntuser = vmp.ntuser
		and SeeModels = 1
--		and @DESubmission = 'M'
inner join rmfg on vmp.mfgkey = rmfg.mfgkey 
		and vmp.productkey = rmfg.productkey
		and rmfg.enddate > @firstdate
group by vmp.mfgkey, dmgroup.groupkey, dproduct.productname, dproduct.productkey
end

insert into @mfgtable 
select distinct dmfg.mfgname, dmfg.mfgkey
from dmfg
inner join @producttable as pt on pt.mfgkey = dmfg.mfgkey


SELECT distinct 1                as Tag, 
        NULL     	as Parent,
	[Manufacturers!1!Id] = null,
	[Manufacturer!2!Name] = null,
	[Manufacturer!2!Key] = null
FROM @mfgtable
union all
SELECT distinct 2                as Tag, 
        1     	as Parent,
	[Manufacturers!1!Id] = null,
	[Manufacturer!2!Name] = mfgname,
	[Manufacturer!2!Key] = mfgkey
FROM @mfgtable

ORDER BY [Manufacturers!1!Id],[Manufacturer!2!Name]

FOR XML EXPLICIT






















