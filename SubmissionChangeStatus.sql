USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[SubmissionChangeStatus]    Script Date: 2/7/2018 1:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










ALTER Procedure  [Submission].[SubmissionChangeStatus](
	@mfgkey int, 
	@asofDate datetime, 
	@ntuser varchar(50),
	@plogTable [Submission].[PlogTableType] readonly
--	@doc varchar(8000)
)
as

/* SubmissionChangeStatus sets a the selected reports to a new status.  
   If this changes them to unsubmitted, this includes merging any 
   unsubmitted revisions, as well as unsubmitting all future versions of this
   report as well.
	created by Kim Strauss
	8/30/02
*/
/*
declare @doc varchar(8000)
set @doc = '<HAI_PROCESSING MfgKey="6" AsOfDate="08/31/2002">
  <PLog Key="67647" StatusKey="1" /> 
  </HAI_PROCESSING>'
*/
--declare @idoc int
declare @errorcount int
--declare @ntuser varchar(50)
declare @ploglist table(
mfgplogkey int primary key,
newstatus int,
oldstatus int, 
unsubmit int)
declare @newstat int

--Get data out of XML and into temp tables
set @errorcount = 0
--set @ntuser = dbo.fnNTUSER()

--EXEC sp_xml_preparedocument @idoc OUTPUT, @doc

--Insert into @ploglist(mfgplogkey,newstatus)
--select mfgplogkey, status
--FROM       OPENXML (@idoc, '/HAI_PROCESSING/PLog',2)
--            WITH (mfgplogkey int '@Key',
--		  status int '@StatusKey')

--EXEC sp_xml_removedocument @idoc
insert into @ploglist (mfgplogkey, newstatus)
select plogkey, [status] from @plogtable

if isnull((Select count(*) from duser where ntuser=@ntuser and opeiops=1),0) < 1 begin
SELECT  tag=1,parent=0,
	[OpenReports!1!Error]='User does not have permission to modify plog details',
	[OpenReports!1!UserName]=@ntuser,
	[OpenReports!1!TimeSubmitted]=getdate()
	for xml explicit
return
end


update @ploglist
set oldstatus = mfgplog.statuskey,
unsubmit = 0
from @ploglist as pl
inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey

update @ploglist
set unsubmit = 1
where oldstatus > 15 and newstatus < 16

-- deal with ones that don't include an unsubmit
if (select count(*) from @ploglist where unsubmit = 0) > 0 begin

update mfgplog
set statuskey = newstatus
from mfgplog
inner join @ploglist as pl on pl.mfgplogkey = mfgplog.mfgplogkey
where unsubmit = 0

delete from @ploglist where unsubmit = 0

end
--deal with the unsubmit

if (select count(*) from @ploglist where unsubmit = 1) > 0 begin

declare @plogs table (
origplog int,
revplog int,
newstatus int,
primary key(origplog, revplog))

-- find plogs for later periods that have been submitted - must unsubmit them as well.

-- find any revisions that need to be incorporated
insert into @plogs
select distinct mp1.mfgplogkey, mp2.mfgplogkey, pl.newstatus
from mfgplog as ins
inner join @ploglist as pl on pl.mfgplogkey = ins.mfgplogkey
inner join mfgplog as mp1 on ins.mfgplogkey = mp1.mfgplogkey
inner join mfgplog as mp2 on ins.mfgreportkey = mp2.mfgreportkey 
		and ins.enddate = mp2.enddate
		and ins.mfgplogkey <> mp2.mfgplogkey
inner join dcostatus as dc on ins.statuskey = dc.dcostatuskey
inner join dcostatus as dc1 on mp1.statuskey = dc1.dcostatuskey
inner join dcostatus as dc2 on mp2.statuskey = dc2.dcostatuskey
where dc1.coapproved = 1 and dc2.coapproved = 0


if (select count(*) from @plogs) > 0 begin
  update at_fmanufacturer 
  set mfgplogkey = origplog
  from at_fmanufacturer as atfm
  inner join @plogs as pl on pl.revplog = atfm.mfgplogkey

  delete from mfgplog
  where mfgplogkey in (select distinct revplog from @plogs)

end

update mfgplog
set statuskey = pl.newstatus
from mfgplog
inner join @ploglist as pl on pl.mfgplogkey = mfgplog.mfgplogkey

update mfgplog
set postperiod = null
from mfgplog
inner join @ploglist as pl on pl.mfgplogkey = mfgplog.mfgplogkey
where mfgplog.enddate <> mfgplog.postperiod

end










