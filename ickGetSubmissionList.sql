USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickGetSubmissionList]    Script Date: 2/22/2018 2:15:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



















--submission.ickGetSubmissionList '12/31/17', 14, 1, 'kstrauss', 'S', 'A'


ALTER Procedure  [Submission].[ickGetSubmissionList](
	@rdate smalldatetime,
	@mfgkey int,
	@catalogkey int,
	@ntuser varchar(50),
	@JobType char(1)='S', 
	@filter char(1)='A')
as 

/* ickGetSubmissionList creates a list of all of the reports for the given date,
	mfgkey and catalogkey, and whether or not they have been submitted
	created by Kim Strauss
		3/14/02
	modified 2/7/18 kds Pass in Username, change schema
*/
--  declare
--  	@rdate smalldatetime,
--  	@mfgkey int,
--  	@catalogkey int,
--  	@JobType char(1), 
--		@ntuser varchar(50),
-- 	@filter char(1)
 
--  set @rdate='12/31/2017'
--  set @mfgkey=14
--set @ntuser = 'kstrauss'
--  set @catalogkey=1
--  set @jobtype='S'
--  set @filter='a'

set nocount on

-- check permissions
if @jobtype<>'S' and isnull((Select count(*) from duser where ntuser=@ntuser and opeiops=1),0) < 1 
return 


declare @reporttable table(
mfgkey int, 
mfgname varchar(75),
catalogkey int, 
catalogname varchar(75),
mfgreportkey int, 
mfgplogkey int, 
enddate smalldatetime,
approved bit, 
reportname varchar(175),
class varchar(10),
datarev bit,
cansubmit bit,
statusdesc varchar(50), 
masterrpt int, 
productname varchar(128))

declare @revtable table(
mfgkey int, 
mfgname varchar(75),
catalogkey int, 
catalogname varchar(75),
mfgreportkey int, 
mfgplogkey int, 
enddate smalldatetime,
approved bit, 
reportname varchar(175),
class varchar(10),
datarev bit,
cansubmit tinyint,
statusdesc varchar(30),
masterrpt int, 
productname varchar(128))

-- find the records that are eligible with the given filter
insert into @reporttable
select rm.mfgkey, rm.mfgname, rm.catalogkey, rm.catalogname, rm.mfgreportkey, mfgplogkey, mfgplog.enddate, 
case @jobtype when 'S' then coapproved
	else tapapproved
end,
case when dcal.EndOfWeek <> 1 then rm.rptname + ' - Split Week' else rm.rptname end,
'Data', 1, 
case when @jobtype='S' and coApproved=0 then 1
     when @jobType='A' and Coapproved=1 and TapApproved=0 then 1
	else 0
end,
'Eligible', rm.mfgreportkey, rm.ProductName
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
inner join dcalendar as dcal on dcal.ddate = mfgplog.enddate
inner join dcalendar as dc2 on dc2.ddate = @rdate
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
where ((mfgplog.postperiod = @rdate and mfgplog.enddate = @rdate) 
	or (mfgplog.postperiod < @rdate and rm.freqkey = 5 
		and dcal.endofweek <> 1 and dcal.inweek = dc2.inweek
		and datepart(yy, dcal.ddate) = datepart(yy, dc2.ddate)))
	and mfgplog.postperiod = mfgplog.enddate
	and rm.mfgkey = @mfgkey
	and rm.catalogkey = @catalogkey
	and rm.status <> 'R'
	and (@filter = 'A' 
	or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1)
	or (@filter = 'M'and rm.freqkey = 5 and status = 'E')
)

--select * from @reporttable

--find the revisions that go with the data records in the first step
insert into @revtable
select rm.mfgkey, rm.mfgname, rm.catalogkey, rm.catalogname, rm.mfgreportkey, mfgplogkey, mfgplog.enddate, 
case @jobtype when 'S' then coapproved
	else tapapproved
end,
rm.rptname,
'Revision', 0,
case when @jobtype='S' and coApproved=0 then 1
     when @jobType='A' and Coapproved=1 and TapApproved=0 then 1
	else 0
end,
'Eligible', rm.mfgreportkey, rm.productname
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
inner join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
where ((mfgplog.postperiod is null and @jobtype = 'S')
		or (mfgplog.postperiod = @rdate and mfgplog.enddate < @rdate))
	and mfgplog.enddate < @rdate
	and rm.mfgkey = @mfgkey
	and rm.catalogkey = @catalogkey
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'M' and rm.freqkey = 5 and status = 'E')
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))

--select * from @revtable

-- add the weeklies when it's a monthly report and the estimate weeklies
insert into @revtable
select  distinct rw.mfgkey, rw.mfgname, rm.catalogkey, rm.catalogname, rm.mfgreportkey, 
mfgplog.mfgplogkey, mfgplog.enddate, 
case @jobtype when 'S' then coapproved
	else tapapproved
end,
rw.rptname,
'Revision', 0,
case when @jobtype='S' and coApproved=0 then 1
     when @jobType='A' and Coapproved=1 and TapApproved=0 then 1
	else 0
end,
'Eligible', rm.mfgreportkey, rw.productname
from vrmfg as rm
inner join @reporttable as rt on rt.mfgreportkey = rm.mfgreportkey
INNER JOIN vrmfg as rw on rm.mfgkey = rw.mfgkey
		and rm.productkey = rw.productkey
		and rm.activitysetkey = rw.activitysetkey
		and rm.geosetkey = rw.geosetkey
		and rt.enddate between rw.begindate and rw.enddate
		and rw.status = 'E'
		and rm.status <> 'E'
inner join dcalendar as dm on dm.ddate = rt.enddate
inner join mfgplog on rw.mfgreportkey = mfgplog.mfgreportkey
	and (mfgplog.enddate <> mfgplog.postperiod
		or mfgplog.postperiod is null)
	and ((statuskey < 16 and @jobtype = 'S')
		or (statuskey < 24 and @jobtype = 'A'))
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rw.mfgreportkey
	and submitdata = 1
inner join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
where mfgplog.enddate <= @rdate
	and rw.mfgkey = @mfgkey
	and rm.catalogkey = @catalogkey
	and rw.status <> 'R'
and mfgplog.mfgplogkey not in (select mfgplogkey from @revtable)

--select * from @revtable

update rt
set rt.masterrpt = r2.mfgreportkey
from @reporttable as rt
inner join rmfg as r1 on r1.mfgreportkey = rt.mfgreportkey
inner join rmfg as r2 on r1.productkey = r2.productkey
	and r1.mfgkey = r2.mfgkey
	and r1.status = 'E'
	and r1.activitysetkey = r2.activitysetkey
	and r2.createweekly = 1
inner join @reporttable as rt2 on rt2.mfgreportkey = r2.mfgreportkey

insert into @revtable
select * from @reporttable where masterrpt <> mfgreportkey

delete from @reporttable where masterrpt <> mfgreportkey

--put master report on revision so that it displays in the right place
update @revtable
set masterrpt = r2.mfgreportkey
from @revtable as rt
inner join rmfg as r1 on r1.mfgreportkey = rt.mfgreportkey
inner join rmfg as r2 on r1.productkey = r2.productkey
	and r1.mfgkey = r2.mfgkey
	and r1.status = 'E'
	and r1.activitysetkey = r2.activitysetkey
	and r2.createweekly = 1
inner join @reporttable as rt2 on rt2.mfgreportkey = r2.mfgreportkey


--set explanation for those that have been submitted
update @reporttable
set statusdesc = 'Submitted'
where cansubmit = 0
update @revtable
set statusdesc = 'Submitted'
where cansubmit = 0
-- Can't submit any reports that are already in a submission job
  UPDATE @reporttable
  SET cansubmit = 0,
  statusdesc = 'In Progress'
  FROM @reporttable as rt
  WHERE CanSubmit = 1 and rt.mfgplogkey in(select distinct mfgplogkey from submission.vopenjobs)	
  UPDATE @revtable
  SET cansubmit = 0,
  statusdesc = 'In Progress'
  FROM @revtable as rt
WHERE CanSubmit = 1 and rt.mfgplogkey in(select distinct mfgplogkey from submission.vopenjobs)	

-- check for previous open reports
update @reporttable
SET cansubmit = 0,
statusdesc = convert(varchar, mp.enddate, 1) + ' Not Submitted'
from @reporttable as rt
inner join mfgplog as mp on mp.mfgreportkey = rt.mfgreportkey
	and mp.enddate = mp.postperiod
	and mp.enddate < @rdate
	and statuskey < 16
inner join rmfg on rmfg.mfgreportkey = rt.mfgreportkey
inner join dmgroup on rmfg.productkey = dmgroup.productkey
inner join dcalendar as dc on mp.enddate = dc.ddate
	and not(freqkey = 5 and endofweek = 0)
where groupkey = 7 or mp.enddate > '1/1/2004'	-- added this temporarily until we fix 2003

-- Can't submit future revisions unless the current period is submittable
--if @jobtype = 'S' begin
  UPDATE @revtable
  SET cansubmit = 2
  FROM @revtable as rvt 
  inner join @reporttable as rpt on rpt.mfgreportkey = rvt.mfgreportkey
  where rvt.cansubmit = 1 and rpt.cansubmit = 0

  UPDATE @revtable
  SET cansubmit = 2
  FROM @revtable as rvt 
  inner join @reporttable as rpt on rpt.mfgreportkey = rvt.masterrpt
  where rvt.cansubmit = 0 and rpt.cansubmit = 1

  UPDATE @revtable
  SET cansubmit = 2
  FROM @revtable as rt
  WHERE EXISTS (SELECT * FROM mfgPlog 
	INNER join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
	WHERE mfgplog.mfgreportkey = rt.masterrpt
	AND mfgplog.enddate = @rdate
	AND mfgplog.postperiod = @rdate

	AND CASE WHEN @jobtype='S' and coApproved=0 then 1
	     WHEN @jobType='A' and Coapproved=1 and TapApproved=0 then 1
		ELSE 0 END = 0)

--select * from @reporttable

--check for order of submission
-- do it twice instead of using or's.
update @reporttable
set statusdesc = 'Must Submit Total Shipments First',
cansubmit = 0
--select *
from @reporttable as rt
inner join mfgplog as mp1 on rt.mfgplogkey = mp1.mfgplogkey
inner join rmfg as r1 on r1.mfgreportkey = mp1.mfgreportkey
	and r1.master = 0 
	and r1.createweekly = 0
	and cansubmit = 1
inner join rptcomparemaster as rcm on r1.mfgreportkey = rcm.mfgreportkey2
inner join rmfg as r2 on r2.mfgreportkey <> r1.mfgreportkey
	and r2.master + r2.createweekly > 0
	and r2.mfgreportkey = rcm.mfgreportkey1
inner join mfgplog as mp2 on r2.mfgreportkey= mp2.mfgreportkey
	and mp2.enddate <= mp1.enddate
	and datepart(yy, mp2.enddate) = datepart(yy, mp1.enddate)
	and mp2.statuskey < 16

update @reporttable
set statusdesc = 'Must Submit Total Shipments First',
cansubmit = 0
--select *
from @reporttable as rt
inner join mfgplog as mp1 on rt.mfgplogkey = mp1.mfgplogkey
inner join rmfg as r1 on r1.mfgreportkey = mp1.mfgreportkey
	and r1.master = 0 
	and r1.createweekly = 0
	and cansubmit = 1
inner join rptcomparemaster as rcm on r1.mfgreportkey = rcm.mfgreportkey1
inner join rmfg as r2 on r2.mfgreportkey <> r1.mfgreportkey
	and r2.master + r2.createweekly > 0
	and r2.mfgreportkey = rcm.mfgreportkey2
inner join mfgplog as mp2 on r2.mfgreportkey= mp2.mfgreportkey
	and mp2.enddate <= mp1.enddate
	and datepart(yy, mp2.enddate) = datepart(yy, mp1.enddate)
	and mp2.statuskey < 16

--select * from @reporttable
 if (select count(*) from @revtable where masterrpt not in (select distinct mfgreportkey from @reporttable)) > 0 
 begin
   insert into @reporttable 
   select distinct mfgkey, mfgname, catalogkey, catalogname, mfgreportkey, 
   -999999+mfgreportkey, @rdate, approved, reportname, 'Data', 1, cansubmit, 
   statusdesc, masterrpt, productname
   from @revtable as rt 
   where rt.masterrpt not in (select mfgreportkey from @reporttable)
 end
--end
select EndDate = convert(varchar, @RDate, 101),rpt.mfgname,
rpt.mfgkey,
rpt.catalogname,
rpt.catalogkey,
rpt.class,
rpt.reportname, 
rpt.mfgreportkey,
rpt.approved,
rpt.mfgplogkey,
rpt.mfgreportkey, 
rpt.enddate,
rpt.datarev,
rpt.cansubmit,

rpt.statusdesc,
rpt.productname,
rvt.reportname, 
rvt.mfgreportkey,
rvt.approved,
rvt.mfgplogkey,
rvt.mfgreportkey, 
rvt.enddate,
rvt.datarev,
rvt.cansubmit,
rvt.statusdesc
FROM @reporttable as rpt 
left join @revtable as rvt on rpt.mfgreportkey = rvt.masterrpt
	and rvt.cansubmit < 2 
	and rpt.enddate = @rdate
