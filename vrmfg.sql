USE [Ahamoper]
GO

/****** Object:  View [dbo].[vrmfg]    Script Date: 2/22/2018 11:01:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

ALTER VIEW [dbo].[vrmfg]
AS
    /* Updated 12/23/14  Kds
	Reconstruction of the view needed for OpCon editing.  Took the OPEI version, added missing fields
	Also added a councilkey for use other places
	2/22/18 kds add groupkey, groupname, councilname, catalogkey and catalogname - this may have strange results if we ever add a catalog set with multiple members
*/
SELECT  rmfg.MfgReportKey ,
        rmfg.mfgkey ,
        dmfg.MfgName ,
        rmfg.productkey ,
        dproduct.ProductName ,
        rmfg.freqkey ,
        dfreq.freqname ,
        rmfg.activitysetkey ,
        sdactivity.activitysetname ,
        rmfg.geosetkey ,
        sdgeo.geosetname ,
        rmfg.usesetkey ,
        sduse.usesetname ,
        rmfg.channelsetkey ,
        sdchannel.channelsetname ,
        rmfg.measuresetkey ,
        sdmeasure.measuresetname ,
        rmfg.marketsetkey ,
        sdmarket.marketsetname ,
        rmfg.catalogsetkey ,
        sdcatalog.catalogsetname ,
        rmfg.begindate ,
        rmfg.enddate ,
        rmfg.status ,
        rmfg.serialno ,
        rptname = CASE rmfg.activitysetkey
                    WHEN 2
                    THEN ISNULL(rmfg.rptname,
                                dfreq.freqname + ' '
                                + sdcatalog.catalogsetname + ' '
                                + dproduct.productname + ' '
                                + sdactivity.activitysetname)
                    ELSE ISNULL(rmfg.rptname,
                                dfreq.freqname + ' '
                                + sdcatalog.catalogsetname + ' '
                                + dproduct.productname + ' '
                                + sdactivity.activitysetname + ' to '
                                + sdgeo.geosetname)
                  END ,
        savedrptname = rptname ,
        rmfg.sizesetkey ,
        rmfg.dinputformkey ,
        rmfg.ytd ,
        rmfg.master ,
        rmfg.createweekly ,
        rmfg.reconcile ,
        rmfg.calendar ,
        rmfg.forecasted ,
        rmfg.participationchecked ,
        rmfg.skipReportCard ,
        rmfg.entryuid ,
        rmfg.appcontext ,
        rmfg.[rowversion] ,
        dgroup.councilkey, 
		dcouncil.councilname, 
		dgroup.groupkey, 
		dgroup.groupname, 
		dcatalog.catalogkey, 
		dcatalog.catalogname
FROM    rmfg
        INNER JOIN dmfg ON dmfg.mfgkey = rmfg.mfgkey
        INNER JOIN dfreq ON dfreq.freqkey = rmfg.freqkey
        INNER JOIN sdcatalog ON rmfg.catalogsetkey = sdcatalog.catalogsetkey
        INNER JOIN dproduct ON rmfg.productkey = dproduct.productkey
        INNER JOIN sdgeo ON rmfg.geosetkey = sdgeo.geosetkey
        INNER JOIN sdactivity ON rmfg.activitysetkey = sdactivity.activitysetkey
        INNER JOIN sduse ON sduse.usesetkey = rmfg.usesetkey
        INNER JOIN sdchannel ON sdchannel.channelsetkey = rmfg.channelsetkey
        INNER JOIN sdmeasure ON sdmeasure.measuresetkey = rmfg.measuresetkey
        INNER JOIN sdmarket ON sdmarket.marketsetkey = rmfg.marketsetkey
        INNER JOIN dmgroup ON dmgroup.productkey = rmfg.productkey
        INNER JOIN dgroup ON dgroup.groupkey = dmgroup.groupkey
        INNER JOIN smcatalog AS smc ON smc.catalogsetkey = sdcatalog.catalogsetkey
        INNER JOIN dcatalog ON dcatalog.catalogkey = smc.catalogkey
		inner join dcouncil on dcouncil.councilkey = dgroup.councilkey 




GO


