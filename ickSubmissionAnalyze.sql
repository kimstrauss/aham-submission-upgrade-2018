USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickSubmissionAnalyze]    Script Date: 2/7/2018 12:59:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--afind 'at_submembers'

--ickSubmissionAnalyze 8123

ALTER PROCEDURE [Submission].[ickSubmissionAnalyze] ( @SubJobKey INT, @ntuser varchar(50) )
AS 
/* ickSubmissionAnalyze analyzes the reports to be submitted and stores
	the results for display on the web. 
	Created by Kim Strauss	
	3/18/02
	7/13/12  kds added support for ytd in SubResults
	10/1/12  kds made the last period always show up not ytd - in new submission website that makes more sense
	2/16/14  kds was getting a duplicate key error on revdata table - put in a check to not add it twice
	2/7/18   kds pass in username & change schema.  Also add security to only the owner of the subjob
*/

  --declare @SubJobKey int
  --set @SubJobKey=67925
  

    SET nocount ON 
    SET arithabort OFF
    SET ansi_warnings OFF

    DECLARE @DataPlogs TABLE
        (
          mfgplogkey INT ,
          submemberkey INT ,
          subresultskey INT ,
          mfgreportkey INT ,
          enddate SMALLDATETIME ,
          lastmonthdate SMALLDATETIME ,
          lastyeardate SMALLDATETIME
            PRIMARY KEY ( mfgreportkey, enddate, mfgplogkey, submemberkey )
        )

    DECLARE @RevPlogs TABLE
        (
          mfgplogkey INT ,
          submemberkey INT ,
          subresultskey INT ,
          mfgreportkey INT ,
          enddate SMALLDATETIME ,
          lastmonthdate SMALLDATETIME ,
          lastyeardate SMALLDATETIME
            PRIMARY KEY ( mfgplogkey, submemberkey, mfgreportkey, enddate )
        )

    DECLARE @DataPlogTotals TABLE
        (
          mfgreportkey INT ,
          enddate SMALLDATETIME ,
          munits MONEY ,
          mvalue MONEY PRIMARY KEY ( mfgreportkey, enddate )
        )

    DECLARE @Stats TABLE
        (
          SubResultskey INT PRIMARY KEY ,
          curTotal MONEY ,
          LastMonthTotal MONEY ,
          LastYearTotal MONEY ,
          ThreeMonthAvg MONEY ,
          ThreeYearAvg MONEY ,
          ThreeMonthStD FLOAT ,
          ThreeYearStd FLOAT ,
          curytd MONEY ,
          lmytd MONEY ,
          lyytd MONEY ,
          ytd TINYINT
        )


    DELETE  FROM suberrors
    WHERE   subresultskey IN ( SELECT DISTINCT
                                        subresultskey
                               FROM     subresults
                               WHERE    subjobkey = @subjobkey )

    DELETE  FROM subresults
    WHERE   subjobkey = @subjobkey

--Analyze Data Plogs
--find data plogs from list
    INSERT  INTO @dataplogs
            ( mfgplogkey ,
              submemberkey ,
              mfgreportkey ,
              enddate ,
              lastmonthdate ,
              lastyeardate
            )
            SELECT DISTINCT
                    su.mfgplogkey ,
                    submemberskey ,
                    mfgplog.mfgreportkey ,
                    mfgplog.enddate ,
                    CASE freqkey
                      WHEN 4
                      THEN DATEADD(d, -1,
                                   DATEADD(m, -1,
                                           DATEADD(d, 1, mfgplog.enddate)))
                      WHEN 3
                      THEN DATEADD(d, -1,
                                   DATEADD(m, -3,
                                           DATEADD(d, 1, mfgplog.enddate)))
                      WHEN 1 THEN DATEADD(yy, -1, mfgplog.enddate)
                      WHEN 5 THEN DATEADD(d, -7, mfgplog.enddate)
                    END ,
                    DATEADD(yy, -1, mfgplog.enddate)
            FROM    submission.submembers as su
                    INNER JOIN mfgplog ON mfgplog.mfgplogkey = su.mfgplogkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
					inner join submission.SubJobs as sj on sj.SubJobKey = su.SubJobKey and sj.NTUser = @ntuser
            WHERE   @subjobkey = su.subjobkey
                    AND mfgplog.enddate = postperiod
                    AND rmfg.calendar = 1

    INSERT  INTO @dataplogs
            ( mfgplogkey ,
              submemberkey ,
              mfgreportkey ,
              enddate ,
              lastmonthdate ,
              lastyeardate
            )
            SELECT DISTINCT
                    submembers.mfgplogkey ,
                    submemberskey ,
                    mfgplog.mfgreportkey ,
                    mfgplog.enddate ,
                    dcd.ddate ,
                    dcy.ddate
            FROM    submission.submembers
                    INNER JOIN mfgplog ON mfgplog.mfgplogkey = submembers.mfgplogkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
                    INNER JOIN dcalendar AS dc ON dc.ddate = mfgplog.enddate
                    INNER JOIN dcalendar AS dcd ON YEAR(dcd.ddate) = YEAR(dc.ddate)
                                                   AND ( ( dcd.inwkmonth = dc.inwkmonth
                                                           - 1
                                                           AND dcd.endofwkmonth = 1
                                                           AND freqkey = 4
                                                         )
                                                         OR ( dcd.inwkquarter = dc.inwkquarter
                                                              - 1
                                                              AND dcd.endofwkquarter = 1
                                                              AND freqkey = 3
                                                            )
                                                         OR ( dcd.inweek = dc.inweek
                                                              - 1
                                                              AND dcd.endofweek = 1
                                                              AND freqkey = 5
                                                            )
                                                         OR ( MONTH(dcd.ddate) = MONTH(dc.ddate)
                                                              AND DAY(dcd.ddate) = DAY(dc.ddate)
                                                              AND freqkey = 1
                                                            )
                                                       )
                    INNER JOIN dcalendar AS dcy ON YEAR(dcy.ddate) = YEAR(dc.ddate)
                                                   - 1
                                                   AND ( ( dcy.inwkmonth = dc.inwkmonth
                                                           AND dcy.endofwkmonth = 1
                                                           AND freqkey = 4
                                                         )
                                                         OR ( dcy.inwkquarter = dc.inwkquarter
                                                              AND dcy.endofwkquarter = 1
                                                              AND freqkey = 3
                                                            )
                                                         OR ( dcy.inweek = dc.inweek
                                                              AND dcy.endofweek = 1
                                                              AND freqkey = 5
                                                            )
-- 	or (month(dcy.ddate) = month(dc.ddate) and day(dcy.ddate) = day(dc.ddate)
-- 		and freqkey = 1)
                                                       )
            WHERE   @subjobkey = subjobkey
                    AND mfgplog.enddate = postperiod
                    AND rmfg.calendar = 0

    INSERT  INTO @dataplogs
            ( mfgplogkey ,
              submemberkey ,
              mfgreportkey ,
              enddate ,
              lastmonthdate ,
              lastyeardate
            )
            SELECT DISTINCT
                    submembers.mfgplogkey ,
                    submemberskey ,
                    mfgplog.mfgreportkey ,
                    mfgplog.enddate ,
                    dcd.ddate ,
                    dcy.ddate
            FROM    submission.submembers
                    INNER JOIN mfgplog ON mfgplog.mfgplogkey = submembers.mfgplogkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
                    INNER JOIN dcalendar AS dc ON dc.ddate = mfgplog.enddate
                    INNER JOIN dcalendar AS dcd ON YEAR(dcd.ddate) = YEAR(dc.ddate)
                                                   - 1
                                                   AND ( ( dc.inwkmonth = 1
                                                           AND dcd.endofwkmonth = 1
                                                           AND dcd.inwkmonth = 12
                                                           AND freqkey = 4
                                                         )
                                                         OR ( dcd.inwkquarter = 4
                                                              AND dc.inwkquarter = 1
                                                              AND dcd.endofwkquarter = 1
                                                              AND freqkey = 3
                                                            )
                                                         OR ( dcd.inweek = 52
                                                              AND dc.inweek = 1
                                                              AND dcd.endofweek = 1
                                                              AND freqkey = 5
                                                            )
                                                         OR ( MONTH(dcd.ddate) = MONTH(dc.ddate)
                                                              AND DAY(dcd.ddate) = DAY(dc.ddate)
                                                              AND freqkey = 1
                                                            )
                                                       )
                    INNER JOIN dcalendar AS dcy ON YEAR(dcy.ddate) = YEAR(dc.ddate)
                                                   - 1
                                                   AND ( ( dcy.inwkmonth = dc.inwkmonth
                                                           AND dcy.endofwkmonth = 1
                                                           AND freqkey = 4
                                                         )
                                                         OR ( dcy.inwkquarter = dc.inwkquarter
                                                              AND dcy.endofwkquarter = 1
                                                              AND freqkey = 3
                                                            )
                                                         OR ( dcy.inweek = dc.inweek
                                                              AND dcy.endofweek = 1
                                                              AND freqkey = 5
                                                            )
                                                         OR ( MONTH(dcy.ddate) = MONTH(dc.ddate)
                                                              AND DAY(dcy.ddate) = DAY(dc.ddate)
                                                              AND freqkey = 1
                                                            )
                                                       )
            WHERE   @subjobkey = subjobkey
                    AND mfgplog.enddate = postperiod
                    AND rmfg.calendar = 0

--fix previous period date if the frequency is weekly
    UPDATE  @dataplogs
    SET     lastmonthdate = dd2.ddate
    FROM    @dataplogs AS dp
            INNER JOIN dcalendar AS dd1 ON dd1.ddate = dp.enddate
            INNER JOIN dcalendar AS dd2 ON ( ( dd1.inweek = 1
                                               AND dd2.inweek = 52
                                               AND dd2.endofweek = 1
                                               AND DATEPART(yy, dd1.ddate) = DATEPART(yy,
                                                              dd2.ddate) + 1
                                             )
                                             OR ( dd1.inweek > 1
                                                  AND dd1.inweek = dd2.inweek
                                                  + 1
                                                  AND dd2.endofweek = 1
                                                  AND DATEPART(yy, dd1.ddate) = DATEPART(yy,
                                                              dd2.ddate)
                                                )
                                           )
            INNER JOIN rmfg ON rmfg.mfgreportkey = dp.mfgreportkey
                               AND freqkey = 5

--fix previous year date if the frequency is weekly
    UPDATE  @dataplogs
    SET     lastyeardate = dd2.ddate
    FROM    @dataplogs AS dp
            INNER JOIN dcalendar AS dd1 ON dd1.ddate = dp.enddate
            INNER JOIN dcalendar AS dd2 ON dd1.inweek = dd2.inweek
                                           AND dd2.endofweek = 1
                                           AND DATEPART(yy, dd1.ddate) = DATEPART(yy,
                                                              dd2.ddate) + 1
            INNER JOIN rmfg ON rmfg.mfgreportkey = dp.mfgreportkey
                               AND freqkey = 5

--select * from @dataplogs

--start results records for these plogs
    INSERT  INTO submission.SubResults
            ( subjobkey ,
              submemberkey ,
              curtotal ,
              lastpertotal ,
              lastyeartotal ,
              threeyearavg ,
              threemonthavg ,
              threeyearstd ,
              threemonthstd ,
              revamt
            )
            SELECT  @subjobkey ,
                    submemberkey ,
                    0 ,
                    0 ,
                    0 ,
                    0 ,
                    0 ,
                    0 ,
                    0 ,
                    0
            FROM    @dataplogs

    UPDATE  @dataplogs
    SET     subresultskey = sr.subresultskey
    FROM    @dataplogs AS dp
            INNER JOIN submission.subresults AS sr ON sr.submemberkey = dp.submemberkey


--Analyze Revision Plogs
-- insert into @revplogs(mfgplogkey, submemberkey, mfgreportkey, enddate, lastmonthdate, lastyeardate)
-- select distinct submembers.mfgplogkey, submemberskey, mfgreportkey, enddate, dateadd(d, -1, dateadd(m, -1, dateadd(d, 1, enddate))), dateadd(yy, -1, enddate)
-- from submembers
-- inner join mfgplog on mfgplog.mfgplogkey = submembers.mfgplogkey
-- where @subjobkey = subjobkey and (enddate <> postperiod or postperiod is null)

    INSERT  INTO @revplogs
            ( mfgplogkey ,
              submemberkey ,
              mfgreportkey ,
              enddate ,
              lastmonthdate ,
              lastyeardate
            )
            SELECT DISTINCT
                    submembers.mfgplogkey ,
                    submemberskey ,
                    mfgplog.mfgreportkey ,
                    mfgplog.enddate ,
                    CASE freqkey
                      WHEN 4
                      THEN DATEADD(d, -1,
                                   DATEADD(m, -1,
                                           DATEADD(d, 1, mfgplog.enddate)))
                      WHEN 3
                      THEN DATEADD(d, -1,
                                   DATEADD(m, -3,
                                           DATEADD(d, 1, mfgplog.enddate)))
                      WHEN 1 THEN DATEADD(yy, -1, mfgplog.enddate)
                      WHEN 5 THEN DATEADD(d, -7, mfgplog.enddate)
                    END ,
                    DATEADD(yy, -1, mfgplog.enddate)
            FROM    submission.submembers
                    INNER JOIN mfgplog ON mfgplog.mfgplogkey = submembers.mfgplogkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
            WHERE   @subjobkey = subjobkey
                    AND ( mfgplog.enddate <> postperiod
                          OR postperiod IS NULL
                        )
                    AND rmfg.calendar = 1

    INSERT  INTO @revplogs
            ( mfgplogkey ,
              submemberkey ,
              mfgreportkey ,
              enddate ,
              lastmonthdate ,
              lastyeardate
            )
            SELECT DISTINCT
                    submembers.mfgplogkey ,
                    submemberskey ,
                    mfgplog.mfgreportkey ,
                    mfgplog.enddate ,
                    dcd.ddate ,
                    dcy.ddate
            FROM    submission.submembers
                    INNER JOIN mfgplog ON mfgplog.mfgplogkey = submembers.mfgplogkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
                    INNER JOIN dcalendar AS dc ON dc.ddate = mfgplog.enddate
                    INNER JOIN dcalendar AS dcd ON YEAR(dcd.ddate) = YEAR(dc.ddate)
                                                   AND ( ( dcd.inwkmonth = dc.inwkmonth
                                                           - 1
                                                           AND dcd.endofwkmonth = 1
                                                           AND freqkey = 4
                                                         )
                                                         OR ( dcd.inwkquarter = dc.inwkquarter
                                                              - 1
                                                              AND dcd.endofwkquarter = 1
                                                              AND freqkey = 3
                                                            )
                                                         OR ( dcd.inweek = dc.inweek
                                                              - 1
                                                              AND dcd.endofweek = 1
                                                              AND freqkey = 5
                                                            )
                                                         OR ( MONTH(dcd.ddate) = MONTH(dc.ddate)
                                                              AND DAY(dcd.ddate) = DAY(dc.ddate)
                                                              AND freqkey = 1
                                                            )
                                                       )
                    INNER JOIN dcalendar AS dcy ON YEAR(dcy.ddate) = YEAR(dc.ddate)
                                                   - 1
                                                   AND ( ( dcy.inwkmonth = dc.inwkmonth
                                                           AND dcy.endofwkmonth = 1
                                                           AND freqkey = 4
                                                         )
                                                         OR ( dcy.inwkquarter = dc.inwkquarter
                                                              AND dcy.endofwkquarter = 1
                                                              AND freqkey = 3
                                                            )
                                                         OR ( dcy.inweek = dc.inweek
                                                              AND dcy.endofweek = 1
                                                              AND freqkey = 5
                                                            )
                                                         OR ( MONTH(dcy.ddate) = MONTH(dc.ddate)
                                                              AND DAY(dcy.ddate) = DAY(dc.ddate)
                                                              AND freqkey = 1
                                                            )
                                                       )
            WHERE   @subjobkey = subjobkey
                    AND ( mfgplog.enddate <> postperiod
                          OR postperiod IS NULL
                        )
                    AND rmfg.calendar = 0

--select * from @revplogs

    INSERT  INTO @revplogs
            ( mfgplogkey ,
              submemberkey ,
              mfgreportkey ,
              enddate ,
              lastmonthdate ,
              lastyeardate
            )
            SELECT DISTINCT
                    submembers.mfgplogkey ,
                    submemberskey ,
                    mfgplog.mfgreportkey ,
                    mfgplog.enddate ,
                    dcd.ddate ,
                    dcy.ddate
            FROM    submission.submembers
                    INNER JOIN mfgplog ON mfgplog.mfgplogkey = submembers.mfgplogkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
                    INNER JOIN dcalendar AS dc ON dc.ddate = mfgplog.enddate
                    INNER JOIN dcalendar AS dcd ON YEAR(dcd.ddate) = YEAR(dc.ddate)
                                                   - 1
                                                   AND ( ( dc.inwkmonth = 1
                                                           AND dcd.endofwkmonth = 1
                                                           AND dcd.inwkmonth = 12
                                                           AND freqkey = 4
                                                         )
                                                         OR ( dcd.inwkquarter = 4
                                                              AND dc.inwkquarter = 1
                                                              AND dcd.endofwkquarter = 1
                                                              AND freqkey = 3
                                                            )
                                                         OR ( dcd.inweek = 52
                                                              AND dc.inweek = 1
                                                              AND dcd.endofweek = 1
                                                              AND freqkey = 5
                                                            )
                                                         OR ( MONTH(dcd.ddate) = MONTH(dc.ddate)
                                                              AND DAY(dcd.ddate) = DAY(dc.ddate)
                                                              AND freqkey = 1
                                                            )
                                                       )
                    INNER JOIN dcalendar AS dcy ON YEAR(dcy.ddate) = YEAR(dc.ddate)
                                                   - 1
                                                   AND ( ( dcy.inwkmonth = dc.inwkmonth
                                                           AND dcy.endofwkmonth = 1
                                                           AND freqkey = 4
                                                         )
                                                         OR ( dcy.inwkquarter = dc.inwkquarter
                                                              AND dcy.endofwkquarter = 1
                                                              AND freqkey = 3
                                                            )
                                                         OR ( dcy.inweek = dc.inweek
                                                              AND dcy.endofweek = 1
                                                              AND freqkey = 5
                                                            )
                                                         OR ( MONTH(dcy.ddate) = MONTH(dc.ddate)
                                                              AND DAY(dcy.ddate) = DAY(dc.ddate)
                                                              AND freqkey = 1
                                                            )
                                                       )
            WHERE   @subjobkey = subjobkey
                    AND ( mfgplog.enddate <> postperiod
                          OR postperiod IS NULL
                        )
                    AND rmfg.calendar = 0
					and not exists (select * from @revplogs as r
						where r.mfgplogkey= submembers.mfgplogkey and
						r.submemberkey =submembers.submemberskey and
						r.mfgreportkey =mfgplog.mfgreportkey and
						r.enddate =mfgplog.enddate )


--start results records for these plogs
  INSERT  INTO submission.SubResults
            ( subjobkey ,
              submemberkey ,
              curtotal ,
              lastpertotal ,
              lastyeartotal ,
              threeyearavg ,
              threemonthavg ,
              threeyearstd ,
              threemonthstd ,
              revamt
            )
            SELECT DISTINCT
                    @subjobkey ,
                    submemberkey ,
                    0 ,
                    0 ,
                    0 ,
                    0 ,
                    0 ,
                    0 ,
                    0 ,
                    SUM(munits)
            FROM    @revplogs AS dp
                    INNER JOIN submission.submembers AS sm ON dp.submemberkey = sm.submemberskey
                    INNER JOIN at_fmanufacturer AS atf ON sm.mfgplogkey = atf.mfgplogkey
            GROUP BY submemberkey

    UPDATE  @revplogs
    SET     subresultskey = sr.subresultskey
    FROM    @revplogs AS dp
            INNER JOIN submission.subresults AS sr ON sr.submemberkey = dp.submemberkey

    INSERT  INTO @dataplogs
            SELECT  *
            FROM    @revplogs

--select * from @dataplogs

--do data analysis on these.
--calculate current total
--drop table #rpts
    SELECT DISTINCT
            mfgreportkey
    INTO    #rpts
    FROM    @dataplogs

    INSERT  INTO @DataPlogTotals
            SELECT  fm.mfgreportkey ,
                    fm.enddate ,
                    munits = SUM(munits) ,
                    mvalue = SUM(mvalue)
            FROM    fmanufacturer AS fm
                    INNER JOIN #rpts AS rpts ON fm.mfgreportkey = rpts.mfgreportkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = fm.mfgreportkey
                    INNER JOIN dcalendar AS dc ON dc.ddate = fm.enddate
                                                  AND fm.targetyear = 0
                                                  AND NOT ( freqkey = 2
                                                            AND insemiannual = 2
                                                          )
            GROUP BY fm.mfgreportkey ,
                    fm.enddate

--december floor care forecasts look at target year 1 instead
    INSERT  INTO @DataPlogTotals
            SELECT  fm.mfgreportkey ,
                    fm.enddate ,
                    munits = SUM(munits) ,
                    mvalue = SUM(mvalue)
            FROM    fmanufacturer AS fm
                    INNER JOIN #rpts AS rpts ON fm.mfgreportkey = rpts.mfgreportkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = fm.mfgreportkey
                    INNER JOIN dcalendar AS dc ON dc.ddate = fm.enddate
                                                  AND fm.targetyear = 1
                                                  AND freqkey = 2
                                                  AND insemiannual = 2
            GROUP BY fm.mfgreportkey ,
                    fm.enddate


    INSERT  INTO @stats
            SELECT  dp.subResultsKey ,
                    Curtotal = SUM(CASE WHEN dp.enddate = fm.enddate
                                        THEN munits
                                        ELSE 0
                                   END) ,
                    LastPerTotal = SUM(CASE WHEN dp.LastmonthDate = fm.enddate
                                            THEN munits
                                            ELSE 0
                                       END) ,
                    LastYearTotal = SUM(CASE WHEN dp.LastYearDate = fm.enddate
                                             THEN munits
                                             ELSE 0
                                        END) ,
                    ThreeMonthAverage = AVG(CASE WHEN fm.enddate BETWEEN DATEADD(d,
                                                              -3,
                                                              DATEADD(m, -3,
                                                              dp.enddate))
                                                              AND
                                                              DATEADD(d, -1,
                                                              dp.enddate)
                                                 THEN munits
                                                 ELSE NULL
                                            END) ,
                    ThreeYearAverage = AVG(CASE WHEN fm.enddate BETWEEN DATEADD(yy,
                                                              -3, dp.enddate)
                                                              AND
                                                              DATEADD(d, -1,
                                                              dp.enddate)
                                                THEN munits
                                                ELSE NULL
                                           END) ,
                    ThreeMonthStd = STDEVP(CASE WHEN fm.enddate BETWEEN DATEADD(d,
                                                              -3,
                                                              DATEADD(m, -3,
                                                              dp.enddate))
                                                              AND
                                                              DATEADD(d, -1,
                                                              dp.enddate)
                                                THEN munits
                                                ELSE NULL
                                           END) ,
                    ThreeYearStd = STDEVP(CASE WHEN fm.enddate BETWEEN DATEADD(yy,
                                                              -3, dp.enddate)
                                                              AND
                                                              DATEADD(d, -1,
                                                              dp.enddate)
                                               THEN munits
                                               ELSE NULL
                                          END) ,
                    Curytd = SUM(CASE WHEN dp.enddate >= fm.enddate
                                           AND DATEPART(yy, dp.enddate) = DATEPART(yy,
                                                              fm.enddate)
                                      THEN munits
                                      ELSE 0
                                 END) ,
                    Lmytd = SUM(CASE WHEN dp.LastmonthDate >= fm.enddate
                                          AND DATEPART(yy, dp.enddate) = DATEPART(yy,
                                                              fm.enddate)
                                     THEN munits
                                     ELSE 0
                                END) ,
                    LYytd = SUM(CASE WHEN dp.LastYearDate >= fm.enddate
                                          AND DATEPART(yy, dp.enddate) = DATEPART(yy,
                                                              fm.enddate)
                                     THEN munits
                                     ELSE 0
                                END) ,
                    ytd
            FROM    @dataplogs AS dp
                    INNER JOIN rmfg ON dp.mfgreportkey = rmfg.mfgreportkey
                    INNER JOIN @DataPlogTotals AS fm ON dp.mfgreportkey = fm.mfgreportkey
            WHERE   measuresetkey <> 3
            GROUP BY dp.subresultskey ,
                    ytd


    INSERT  INTO @stats
            SELECT  dp.subResultsKey ,
                    Curtotal = SUM(CASE WHEN dp.enddate = fm.enddate
                                        THEN mvalue
                                        ELSE 0
                                   END) ,
                    LastPerTotal = SUM(CASE WHEN dp.LastmonthDate = fm.enddate
                                            THEN mvalue
                                            ELSE 0
                                       END) ,
                    LastYearTotal = SUM(CASE WHEN dp.LastYearDate = fm.enddate
                                             THEN mvalue
                                             ELSE 0
                                        END) ,
                    ThreeMonthAverage = AVG(CASE WHEN fm.enddate BETWEEN DATEADD(d,
                                                              -3,
                                                              DATEADD(m, -3,
                                                              dp.enddate))
                                                              AND
                                                              DATEADD(d, -1,
                                                              dp.enddate)
                                                 THEN mvalue
                                                 ELSE NULL
                                            END) ,
                    ThreeYearAverage = AVG(CASE WHEN fm.enddate BETWEEN DATEADD(yy,
                                                              -3, dp.enddate)
                                                              AND
                                                              DATEADD(d, -1,
                                                              dp.enddate)
                                                THEN mvalue
                                                ELSE NULL
                                           END) ,
                    ThreeMonthStd = STDEVP(CASE WHEN fm.enddate BETWEEN DATEADD(d,
                                                              -3,
                                                              DATEADD(m, -3,
                                                              dp.enddate))
                                                              AND
                                                              DATEADD(d, -1,
                                                              dp.enddate)
                                                THEN mvalue
                                                ELSE NULL
                                           END) ,
                    ThreeYearStd = STDEVP(CASE WHEN fm.enddate BETWEEN DATEADD(yy,
                                                              -3, dp.enddate)
                                                              AND
                                                              DATEADD(d, -1,
                                                              dp.enddate)
                                               THEN mvalue
                                               ELSE NULL
                                          END) ,
                    Curytd = SUM(CASE WHEN dp.enddate >= fm.enddate
                                           AND DATEPART(yy, dp.enddate) = DATEPART(yy,
                                                              fm.enddate)
                                      THEN mvalue
                                      ELSE 0
                                 END) ,
                    Lmytd = SUM(CASE WHEN dp.LastmonthDate >= fm.enddate
                                          AND DATEPART(yy, dp.enddate) = DATEPART(yy,
                                                              fm.enddate)
                                     THEN mvalue
                                     ELSE 0
                                END) ,
                    LYytd = SUM(CASE WHEN dp.LastYearDate >= fm.enddate
                                          AND DATEPART(yy, dp.enddate) = DATEPART(yy,
                                                              fm.enddate)
                                     THEN mvalue
                                     ELSE 0
                                END) ,
                    ytd
            FROM    @dataplogs AS dp
                    INNER JOIN rmfg ON dp.mfgreportkey = rmfg.mfgreportkey
                    INNER JOIN @DataPlogTotals AS fm ON dp.mfgreportkey = fm.mfgreportkey
            WHERE   measuresetkey = 3
            GROUP BY dp.subresultskey ,
                    ytd

--select * from @stats

    UPDATE  Sub
    SET     CurTotal = s.curtotal , --case when ytd = 1 then s.curytd else s.curtotal end,
            ytd = s.curytd ,
	--LastPerTotal=case when s.ytd = 1 then s.lmytd else s.LastMonthTotal end,
            LastPerTotal = s.LastMonthTotal ,
            LastYearTotal = CASE WHEN s.ytd = 1 THEN s.lyytd
                                 ELSE s.LastYearTotal
                            END ,
            ThreeMonthAvg = s.ThreeMonthAvg ,
            ThreeYearAvg = s.ThreeYearAvg ,
            ThreeMonthStd = CASE WHEN s.threemonthstd = 0 THEN 0
                                 ELSE ABS(( s.curtotal - s.threemonthavg )
                                          / s.ThreeMonthStd)
                            END ,
            ThreeYearStD = CASE WHEN s.threeyearstd = 0 THEN 0
                                ELSE ABS(( s.curtotal - s.threeyearavg )
                                         / s.ThreeYearStd)
                           END
    FROM    subresults AS sub
            INNER JOIN @stats AS s ON s.subresultskey = sub.subresultskey




-- find those with no valid mfgplogkey
    DELETE  FROM @revplogs
    INSERT  INTO @revplogs
            ( mfgplogkey ,
              submemberkey
            )
            SELECT  0 ,
                    submemberskey
            FROM    submission.submembers
            WHERE   mfgplogkey NOT IN ( SELECT DISTINCT
                                                mfgplogkey
                                        FROM    mfgplog )

    INSERT  INTO subresults
            ( subjobkey ,
              submemberkey
            )
            SELECT  @subjobkey ,
                    submemberkey
            FROM    @revplogs

    UPDATE  @revplogs
    SET     subresultskey = sr.subresultskey
    FROM    @revplogs AS dp
            INNER JOIN submission.subresults AS sr ON sr.submemberkey = dp.submemberkey

    INSERT  INTO submission.suberrors
            ( subresultskey ,
              error ,
              note
            )
            SELECT  subresultskey ,
                    2 ,
                    'Unknown Plog Key - unable to analyze'
            FROM    @revplogs

    INSERT  INTO submission.suberrors
            ( subresultskey ,
              error ,
              warning ,
              note
            )
            SELECT  subresultskey ,
                    0 ,
                    1 ,
                    'Current report total is more than 4 standard deviations from the mean'
            FROM    subresults AS sr
                    INNER JOIN submembers AS sm ON sr.submemberkey = sm.submemberskey
                    INNER JOIN mfgplog ON sm.mfgplogkey = mfgplog.mfgplogkey
                    INNER JOIN rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
                                       AND rmfg.status = 'A'
            WHERE   sr.subjobkey = @subjobkey
                    AND threemonthstd > 4

    --EXEC submission.DataEntryAnalyze @subjobkey, @ntuser, 0, 0

--mark analysis as complete
    UPDATE  submission.SubJobs
    SET     results = 1
    WHERE   subjobkey = @subjobkey

-- set results to 2 if all 0's.
    IF ( SELECT SUM(curtotal)
         FROM   submission.subresults
         WHERE  subjobkey = @subjobkey
       ) = 0
        AND ( SELECT    COUNT(*)
              FROM      subresults
              WHERE     subjobkey = @subjobkey
            ) > 0 
        BEGIN
            UPDATE  submission.SubJobs
            SET     results = 2
            WHERE   subjobkey = @subjobkey
        END
