USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickSubmissionResults]    Script Date: 2/13/2018 7:54:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Submission].[ickSubmissionResults] (@subjobkey INT, @ntuser varchar(50))
AS
 /* ickSubmissionResults returns xml code that represent the results of the analysis.
	Created by Kim Strauss
	3/19/02
	7/13/12  kds Add support for ytd. 
	2/7/18   kds pass in username & change schema
*/


-- declare @subjobkey int
-- set @subjobkey = 11902
-- declare @ntuser varchar(50)
--set @ntuser = 'kstrauss'

      SET nocount ON

      --DECLARE @ntuser VARCHAR(50)
      --SET @ntuser = dbo.fnNTUSER()
      DECLARE @mfgkey INT
      DECLARE @mfgname VARCHAR(60)
      DECLARE @enddate SMALLDATETIME
      DECLARE @begindate DATETIME

      SET @mfgkey = (SELECT mfgkey
                     FROM   submission.subjobexecution
                     WHERE  @subjobkey = subjobkey)
      SET @enddate = (SELECT    Reportenddate
                      FROM      submission.subjobexecution
                      WHERE     @subjobkey = subjobkey)
      SET @begindate = (SELECT  Jobbegindate
                        FROM    submission.subjobexecution
                        WHERE   @subjobkey = subjobkey)
      SET @mfgname = (SELECT    mfgname
                      FROM      dmfg
                      WHERE     mfgkey = @mfgkey)

	


      DECLARE @subresults TABLE
              (
               subjobkey INT
              ,status VARCHAR(100)
              ,show BIT
              ,allzeros BIT
              )

      DECLARE @resultsTable TABLE
              (
               SubJobKey INT
              ,datarev VARCHAR(10)
              ,PlogKey INT
              ,ReportKey INT
              ,MfgKey INT
              ,ReportName VARCHAR(128)
              ,EndDate SMALLDATETIME
              ,CurrPeriod MONEY
              ,ytd MONEY
              ,LastPeriod MONEY
              ,LastYear MONEY
              ,ThreeYearAvg MONEY
              ,ThreeMonthAvg MONEY
              ,ThreeYearStd MONEY
              ,ThreeMonthStd MONEY
              ,DataRecord BIT
              ,revamt MONEY
              ,groupkey INT
              ,productkey INT
              ,productname VARCHAR(50)
              ,subresultskey INT
              ,master TINYINT
              ,ReportType CHAR(1)
              )


      DECLARE @revresultsTable TABLE
              (
               SubJobKey INT
              ,datarev VARCHAR(10)
              ,PlogKey INT
              ,ReportKey INT
              ,masterreportkey INT
              ,MfgKey INT
              ,ReportName VARCHAR(128)
              ,EndDate SMALLDATETIME
              ,CurrPeriod MONEY
              ,ytd MONEY
              ,LastPeriod MONEY
              ,LastYear MONEY
              ,ThreeYearAvg MONEY
              ,ThreeMonthAvg MONEY
              ,ThreeYearStd MONEY
              ,ThreeMonthStd MONEY
              ,DataRecord BIT
              ,revamt MONEY
              ,groupkey INT
              ,productkey INT
              ,subresultskey INT
              )

      INSERT    INTO @subresults
                SELECT  subjobkey
                       ,case status
						  WHEN 0 then 'Unknown'
                          WHEN 1 THEN 'Job Initiated'
                          WHEN 10 THEN 'Sent to Processing Queue'
                          WHEN 11 THEN 'Processing Active'
						  when 15 then 'Analysis Processing Completed'
						  when 50 then 'Commit Processing Queued' 
						  when 51 then 'Commit Processing Active' 
						  when 55 then 'Commit Processing Completed'
						  when 100 then 'Job Completed'
						  when 110 then 'Job Canceled' 
						  when 1000 then 'Job Terminated'
                        END
                       ,1
                       ,0
                        
                FROM    submission.subjobexecution
                WHERE   RequestingUserName = @ntuser
                        AND subjobkey = @subjobkey

      IF (SELECT    count(*)
          FROM      @subresults) = 0 
         BEGIN
               INSERT   INTO @subresults
                        SELECT  subjobkey
                               ,'Incorrect Permissions for viewing results'
                               ,0
                               ,0
                        FROM    submission.subjobExecution
                        WHERE   subjobkey = @subjobkey
         END

      IF (SELECT    count(*)
          FROM      @subresults) = 0 
         BEGIN
               INSERT   INTO @subresults
               VALUES   (@subjobkey, 'Unknown job key', 0, 0)
         END

      INSERT    INTO @resultstable
                SELECT DISTINCT
                        sr.subjobkey
                       ,case WHEN mfgplog.postperiod = mfgplog.enddate THEN 'Data'
                             ELSE 'Revision'
                        END
                       ,mfgplog.mfgplogkey
                       ,mfgplog.mfgreportkey
                       ,rmfg.mfgkey
                       ,case rmfg.activitysetkey
                          WHEN 2
                          THEN isnull(rmfg.rptname,
                                      dfreq.freqname + ' ' + catalogsetname + ' ' + dproduct.productname + ' '
                                      + activitysetname)
                          ELSE isnull(rmfg.rptname,
                                      dfreq.freqname + ' ' + catalogsetname + ' ' + dproduct.productname + ' '
                                      + activitysetname + ' to ' + geosetname)
                        END
                       ,mfgplog.enddate
                       ,curtotal
                       ,subresults.ytd
                       ,lastpertotal
                       ,lastyeartotal
                       ,threeyearavg
                       ,threemonthavg
                       ,threeyearstd
                       ,threemonthstd
                       ,case WHEN mfgplog.postperiod = mfgplog.enddate THEN 1
                             ELSE 0
                        END
                       ,revamt
                       ,groupkey
                       ,rmfg.productkey
                       ,productname
                       ,subresultskey
                       ,case WHEN dfreq.freqkey <> 5 THEN 1		-- make the split week not a master
                             WHEN endofweek = 1 THEN 1
                             ELSE 0
                        END
                       ,case WHEN rmfg.catalogsetkey = 6 THEN 'F'
                             WHEN rmfg.catalogsetkey = 4 THEN 'C'
                             ELSE 'G'
                        END
                FROM    @subresults AS sr
                INNER JOIN submission.subresults ON sr.subjobkey = subresults.subjobkey
                INNER JOIN submission.submembers ON submembers.submemberskey = subresults.submemberkey
                INNER JOIN mfgplog ON mfgplog.mfgplogkey = submembers.mfgplogkey
                INNER JOIN rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
                INNER JOIN dfreq ON dfreq.freqkey = rmfg.freqkey
                INNER JOIN dproduct ON dproduct.productkey = rmfg.productkey
                INNER JOIN sdactivity AS sda ON sda.activitysetkey = rmfg.activitysetkey
                INNER JOIN sdcatalog AS sdc ON sdc.catalogsetkey = rmfg.catalogsetkey
                INNER JOIN sdgeo ON sdgeo.geosetkey = rmfg.geosetkey
                INNER JOIN dmgroup ON dmgroup.productkey = rmfg.productkey
                INNER JOIN dcalendar AS dc ON dc.ddate = mfgplog.enddate
                WHERE   sr.show = 1
                        AND mfgplog.postperiod = mfgplog.enddate
                        AND rmfg.status <> 'E'

--select * from @resultstable

      INSERT    INTO @revresultstable
                SELECT DISTINCT
                        sr.subjobkey
                       ,case WHEN mfgplog.postperiod = mfgplog.enddate THEN 'Data'
                             ELSE 'Revision'
                        END
                       ,mfgplog.mfgplogkey
                       ,mfgplog.mfgreportkey
                       ,case WHEN rmfg.status = 'E' THEN rm.mfgreportkey
                             ELSE mfgplog.mfgreportkey
                        END
                       ,rmfg.mfgkey
                       ,case rmfg.activitysetkey
                          WHEN 2
                          THEN isnull(rmfg.rptname,
                                      dfreq.freqname + ' ' + catalogsetname + ' ' + dproduct.productname + ' '
                                      + activitysetname)
                          ELSE isnull(rmfg.rptname,
                                      dfreq.freqname + ' ' + catalogsetname + ' ' + dproduct.productname + ' '
                                      + activitysetname + ' to ' + geosetname)
                        END
                       ,mfgplog.enddate
                       ,curtotal
                       ,subresults.ytd
                       ,lastpertotal
                       ,lastyeartotal
                       ,threeyearavg
                       ,threemonthavg
                       ,threeyearstd
                       ,threemonthstd
                       ,case WHEN mfgplog.postperiod = mfgplog.enddate THEN 1
                             ELSE 0
                        END
                       ,revamt
                       ,groupkey
                       ,rmfg.productkey
                       ,subresultskey
                FROM    @subresults AS sr
                INNER JOIN submission.subresults ON sr.subjobkey = subresults.subjobkey
                INNER JOIN submission.submembers ON submembers.submemberskey = subresults.submemberkey
                INNER JOIN mfgplog ON mfgplog.mfgplogkey = submembers.mfgplogkey
                INNER JOIN rmfg AS rmfg ON rmfg.mfgreportkey = mfgplog.mfgreportkey
                INNER JOIN dfreq ON dfreq.freqkey = rmfg.freqkey
                INNER JOIN dproduct ON dproduct.productkey = rmfg.productkey
                INNER JOIN sdactivity AS sda ON sda.activitysetkey = rmfg.activitysetkey
                INNER JOIN sdcatalog AS sdc ON sdc.catalogsetkey = rmfg.catalogsetkey
                INNER JOIN sdgeo ON sdgeo.geosetkey = rmfg.geosetkey
                INNER JOIN dmgroup ON dmgroup.productkey = rmfg.productkey
                LEFT JOIN rmfg AS rm ON rm.mfgkey = rmfg.mfgkey
                                        AND rm.productkey = rmfg.productkey
                                        AND rm.activitysetkey = rmfg.activitysetkey
                                        AND rm.geosetkey = rmfg.geosetkey
                                        AND rm.mfgreportkey <> rmfg.mfgreportkey
                                        AND mfgplog.enddate BETWEEN rmfg.begindate AND rmfg.enddate
                                        AND rm.status <> 'E'
                                        AND rm.mfgreportkey IN (
                                        SELECT  mfgreportkey
                                        FROM    submission.submembers
                                        INNER JOIN mfgplog ON submembers.mfgplogkey = mfgplog.mfgplogkey)
                WHERE   sr.show = 1
                        AND (mfgplog.postperiod IS NULL
                             OR mfgplog.postperiod <> mfgplog.enddate
                             OR rmfg.status = 'E')

--select * from @revresultstable

      UPDATE    @revresultstable
      SET       reportname = reportname + ' - Split Week'
      FROM      @revresultstable AS rt
      INNER JOIN rmfg ON rmfg.mfgreportkey = rt.reportkey
      INNER JOIN dcalendar AS dc ON dc.ddate = rt.enddate
                                    AND freqkey = 5
                                    AND endofweek = 0

      UPDATE    @resultstable
      SET       reportname = reportname + ' - Split Week'
      FROM      @resultstable AS rt
      INNER JOIN rmfg ON rmfg.mfgreportkey = rt.reportkey
      INNER JOIN dcalendar AS dc ON dc.ddate = rt.enddate
                                    AND freqkey = 5
                                    AND endofweek = 0

--select * from @resultstable

      IF (SELECT    count(*)
          FROM      @revresultstable
          WHERE     masterreportkey NOT IN (SELECT DISTINCT
                                                    reportkey
                                            FROM    @resultstable)) > 0 
         BEGIN
               INSERT   INTO @resultstable
                        SELECT DISTINCT
                                rt.subjobkey
                               ,'Data'
                               ,max(rt.plogkey)
                               , 
	---999999+rt.reportkey, 
                                rt.reportkey
                               ,rt.mfgkey
                               ,reportname
                               ,max(sj.Reportenddate)
                               ,0
                               ,0
                               ,0
                               ,
--LastPeriod,
                                0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,rt.groupkey
                               ,rt.productkey
                               ,dp.productname
                               ,0
                               ,1
                               ,case WHEN rmfg.catalogsetkey = 6 THEN 'F'
                                     WHEN rmfg.catalogsetkey = 4 THEN 'C'
                                     ELSE 'G'
                                END
                        FROM    @revresultstable AS rt
                        INNER JOIN dproduct AS dp ON dp.productkey = rt.productkey
                        INNER JOIN rmfg ON rmfg.mfgreportkey = rt.reportkey
                        INNER JOIN submission.subjobExecution AS sj ON sj.subjobkey = rt.subjobkey
                        WHERE   rt.masterreportkey NOT IN (SELECT   reportkey
                                                           FROM     @resultstable)
                        GROUP BY rt.subjobkey
                               ,rt.reportkey
                               ,rt.mfgkey
                               ,reportname
                               ,rt.groupkey
                               ,rt.productkey
                               ,dp.productname
                               ,rmfg.catalogsetkey
         END


      SELECT DISTINCT
                Tag = 1
               ,Parent = NULL
               ,[SUBMISSION_RESULTS!1!Key] = NULL
               ,[Results!2!MfgKey] = NULL
               ,[Results!2!MfgName] = NULL
               ,[Results!2!EndDate] = NULL
               ,[Results!2!JobKey] = NULL
               ,[Results!2!Status] = NULL
               ,[Results!2!StartDate] = NULL
               ,[Results!2!AllZeros] = NULL
               ,[ReportClass!3!Name] = NULL
               ,[Report!4!PlogKey] = NULL
               ,[Report!4!ReportKey] = NULL
               ,[Report!4!GroupKey] = NULL
               ,[Report!4!ProductKey] = NULL
               ,[Report!4!ProductName] = NULL
               ,[Report!4!MfgKey] = NULL
               ,[Report!4!ReportName] = NULL
               ,[Report!4!EndDate] = NULL
               ,[Report!4!CurrPeriod] = NULL
               ,[Report!4!Ytd] = NULL
               ,[Report!4!LastPeriod] = NULL
               ,[Report!4!LastYear] = NULL
               ,[Report!4!ThreeYearAvg] = NULL
               ,[Report!4!ThreeMonthAvg] = NULL
               ,[Report!4!ThreeYearStd] = NULL
               ,[Report!4!ThreeMonthStd] = NULL
               ,[Report!4!DataRecord] = NULL
               ,[Report!4!Type] = NULL
               ,[Revision!5!PlogKey] = NULL
               ,[Revision!5!ReportKey] = NULL
               ,[Revision!5!GroupKey] = NULL
               ,[Revision!5!ProductKey] = NULL
               ,[Revision!5!MfgKey] = NULL
               ,[Revision!5!ReportName] = NULL
               ,[Revision!5!EndDate] = NULL
               ,[Revision!5!CurrPeriod] = NULL
               ,[Revision!5!Ytd] = NULL
               ,[Revision!5!LastPeriod] = NULL
               ,[Revision!5!LastYear] = NULL
               ,[Revision!5!ThreeYearAvg] = NULL
               ,[Revision!5!ThreeMonthAvg] = NULL
               ,[Revision!5!ThreeYearStd] = NULL
               ,[Revision!5!ThreeMonthStd] = NULL
               ,[Revision!5!AmtRevised] = NULL
               ,[Revision!5!DataRecord] = NULL
               ,[Comments!6!Error] = NULL
               ,[Comments!6!Warning] = NULL
               ,[Comments!6!Note] = NULL
               ,[Comments!6!LineDetail] = NULL
      UNION ALL
      SELECT DISTINCT
                Tag = 2
               ,Parent = 1
               ,[SUBMISSION_RESULTS!1!Key] = NULL
               ,[Results!2!MfgKey] = @mfgkey
               ,[Results!2!MfgName] = @mfgname
               ,[Results!2!EndDate] = @enddate
               ,[Results!2!JobKey] = sr.subjobkey
               ,[Results!2!Status] = sr.status
               ,[Results!2!StartDate] = @begindate
               ,[Results!2!AllZeros] = sr.allzeros
               ,[ReportClass!3!Name] = NULL
               ,[Report!4!PlogKey] = NULL
               ,[Report!4!ReportKey] = NULL
               ,[Report!4!GroupKey] = NULL
               ,[Report!4!ProductKey] = NULL
               ,[Report!4!ProductName] = NULL
               ,[Report!4!MfgKey] = NULL
               ,[Report!4!ReportName] = NULL
               ,[Report!4!EndDate] = NULL
               ,[Report!4!CurrPeriod] = NULL
               ,[Report!4!Ytd] = NULL
               ,[Report!4!LastPeriod] = NULL
               ,[Report!4!LastYear] = NULL
               ,[Report!4!ThreeYearAvg] = NULL
               ,[Report!4!ThreeMonthAvg] = NULL
               ,[Report!4!ThreeYearStd] = NULL
               ,[Report!4!ThreeMonthStd] = NULL
               ,[Report!4!DataRecord] = NULL
               ,[Report!4!Type] = NULL
               ,[Revision!5!PlogKey] = NULL
               ,[Revision!5!ReportKey] = NULL
               ,[Revision!5!GroupKey] = NULL
               ,[Revision!5!ProductKey] = NULL
               ,[Revision!5!MfgKey] = NULL
               ,[Revision!5!ReportName] = NULL
               ,[Revision!5!EndDate] = NULL
               ,[Revision!5!CurrPeriod] = NULL
               ,[Revision!5!Ytd] = NULL
               ,[Revision!5!LastPeriod] = NULL
               ,[Revision!5!LastYear] = NULL
               ,[Revision!5!ThreeYearAvg] = NULL
               ,[Revision!5!ThreeMonthAvg] = NULL
               ,[Revision!5!ThreeYearStd] = NULL
               ,[Revision!5!ThreeMonthStd] = NULL
               ,[Revision!5!AmtRevised] = NULL
               ,[Revision!5!DataRecord] = NULL
               ,[Comments!6!Error] = NULL
               ,[Comments!6!Warning] = NULL
               ,[Comments!6!Note] = NULL
               ,[Comments!6!LineDetail] = NULL
      FROM      @subresults AS sr
      UNION ALL
      SELECT DISTINCT
                Tag = 3
               ,Parent = 2
               ,[SUBMISSION_RESULTS!1!Key] = NULL
               ,[Results!2!MfgKey] = @mfgkey
               ,[Results!2!MfgName] = @mfgname
               ,[Results!2!EndDate] = @enddate
               ,[Results!2!JobKey] = sr.subjobkey
               ,[Results!2!Status] = sr.status
               ,[Results!2!StartDate] = @begindate
               ,[Results!2!AllZeros] = sr.allzeros
               ,[ReportClass!3!Name] = datarev
               ,[Report!4!PlogKey] = NULL
               ,[Report!4!ReportKey] = NULL
               ,[Report!4!GroupKey] = NULL
               ,[Report!4!ProductKey] = NULL
               ,[Report!4!ProductName] = NULL
               ,[Report!4!MfgKey] = NULL
               ,[Report!4!ReportName] = NULL
               ,[Report!4!EndDate] = NULL
               ,[Report!4!CurrPeriod] = NULL
               ,[Report!4!Ytd] = NULL
               ,[Report!4!LastPeriod] = NULL
               ,[Report!4!LastYear] = NULL
               ,[Report!4!ThreeYearAvg] = NULL
               ,[Report!4!ThreeMonthAvg] = NULL
               ,[Report!4!ThreeYearStd] = NULL
               ,[Report!4!ThreeMonthStd] = NULL
               ,[Report!4!DataRecord] = NULL
               ,[Report!4!Type] = NULL
               ,[Revision!5!PlogKey] = NULL
               ,[Revision!5!ReportKey] = NULL
               ,[Revision!5!GroupKey] = NULL
               ,[Revision!5!ProductKey] = NULL
               ,[Revision!5!MfgKey] = NULL
               ,[Revision!5!ReportName] = NULL
               ,[Revision!5!EndDate] = NULL
               ,[Revision!5!CurrPeriod] = NULL
               ,[Revision!5!Ytd] = NULL
               ,[Revision!5!LastPeriod] = NULL
               ,[Revision!5!LastYear] = NULL
               ,[Revision!5!ThreeYearAvg] = NULL
               ,[Revision!5!ThreeMonthAvg] = NULL
               ,[Revision!5!ThreeYearStd] = NULL
               ,[Revision!5!ThreeMonthStd] = NULL
               ,[Revision!5!AmtRevised] = NULL
               ,[Revision!5!DataRecord] = NULL
               ,[Comments!6!Error] = NULL
               ,[Comments!6!Warning] = NULL
               ,[Comments!6!Note] = NULL
               ,[Comments!6!LineDetail] = NULL
      FROM      @subresults AS sr
      INNER JOIN @resultstable AS rt ON sr.subjobkey = rt.subjobkey
      UNION ALL
      SELECT DISTINCT
                Tag = 4
               ,Parent = 3
               ,[SUBMISSION_RESULTS!1!Key] = NULL
               ,[Results!2!MfgKey] = @mfgkey
               ,[Results!2!MfgName] = @mfgname
               ,[Results!2!EndDate] = @enddate
               ,[Results!2!JobKey] = sr.subjobkey
               ,[Results!2!Status] = sr.status
               ,[Results!2!StartDate] = @begindate
               ,[Results!2!AllZeros] = sr.allzeros
               ,[ReportClass!3!Name] = datarev
               ,[Report!4!PlogKey] = plogkey
               ,[Report!4!ReportKey] = reportkey
               ,[Report!4!GroupKey] = groupkey
               ,[Report!4!ProductKey] = productkey
               ,[Report!4!ProductName] = productname
               ,[Report!4!MfgKey] = mfgkey
               ,[Report!4!ReportName] = reportname
               ,[Report!4!EndDate] = enddate
               ,[Report!4!CurrPeriod] = isnull(CurrPeriod, 0)
               ,[Report!4!Ytd] = isnull(ytd, 0)
               ,[Report!4!LastPeriod] = isnull(LastPeriod, 0)
               ,[Report!4!LastYear] = isnull(lastyear, 0)
               ,[Report!4!ThreeYearAvg] = 0
               , --isnull(threeyearavg, 0),
                [Report!4!ThreeMonthAvg] = isnull(threemonthavg, 0)
               ,[Report!4!ThreeYearStd] = 0
               ,  --isnull(abs(threeyearstd), 0),
                [Report!4!ThreeMonthStd] = isnull(abs(threemonthstd), 0)
               ,[Report!4!DataRecord] = datarecord
               ,[Report!4!Type] = reporttype
               ,[Revision!5!PlogKey] = NULL
               ,[Revision!5!ReportKey] = NULL
               ,[Revision!5!GroupKey] = NULL
               ,[Revision!5!ProductKey] = NULL
               ,[Revision!5!MfgKey] = NULL
               ,[Revision!5!ReportName] = NULL
               ,[Revision!5!EndDate] = NULL
               ,[Revision!5!CurrPeriod] = NULL
               ,[Revision!5!Ytd] = NULL
               ,[Revision!5!LastPeriod] = NULL
               ,[Revision!5!LastYear] = NULL
               ,[Revision!5!ThreeYearAvg] = NULL
               ,[Revision!5!ThreeMonthAvg] = NULL
               ,[Revision!5!ThreeYearStd] = NULL
               ,[Revision!5!ThreeMonthStd] = NULL
               ,[Revision!5!AmtRevised] = NULL
               ,[Revision!5!DataRecord] = NULL
               ,[Comments!6!Error] = NULL
               ,[Comments!6!Warning] = NULL
               ,[Comments!6!Note] = NULL
               ,[Comments!6!LineDetail] = NULL
      FROM      @subresults AS sr
      INNER JOIN @resultstable AS rt ON rt.subjobkey = sr.subjobkey
      UNION ALL
      SELECT DISTINCT
                Tag = 5
               ,Parent = 4
               ,[SUBMISSION_RESULTS!1!Key] = NULL
               ,[Results!2!MfgKey] = @mfgkey
               ,[Results!2!MfgName] = @mfgname
               ,[Results!2!EndDate] = @enddate
               ,[Results!2!JobKey] = sr.subjobkey
               ,[Results!2!Status] = sr.status
               ,[Results!2!StartDate] = @begindate
               ,[Results!2!AllZeros] = sr.allzeros
               ,[ReportClass!3!Name] = rt.datarev
               ,[Report!4!PlogKey] = rt.plogkey
               ,[Report!4!ReportKey] = rt.reportkey
               ,[Report!4!GroupKey] = rt.groupkey
               ,[Report!4!ProductKey] = rt.productkey
               ,[Report!4!ProductName] = rt.productname
               ,[Report!4!MfgKey] = rt.mfgkey
               ,[Report!4!ReportName] = rt.reportname
               ,[Report!4!EndDate] = rt.enddate
               ,[Report!4!CurrPeriod] = isnull(rt.CurrPeriod, 0)
               ,[Report!4!Ytd] = isnull(rt.ytd, 0)
               ,[Report!4!LastPeriod] = isnull(rt.LastPeriod, 0)
               ,[Report!4!LastYear] = isnull(rt.lastyear, 0)
               ,[Report!4!ThreeYearAvg] = 0
               , --isnull(rt.threeyearavg, 0),
                [Report!4!ThreeMonthAvg] = isnull(rt.threemonthavg, 0)
               ,[Report!4!ThreeYearStd] = 0
               , --isnull(abs(rt.threeyearstd), 0),
                [Report!4!ThreeMonthStd] = isnull(abs(rt.threemonthstd), 0)
               ,[Report!4!DataRecord] = rt.datarecord
               ,[Report!4!Type] = reporttype
               ,[Revision!5!PlogKey] = rrt.plogkey
               ,[Revision!5!ReportKey] = rrt.reportkey
               ,[Revision!5!GroupKey] = rrt.groupkey
               ,[Revision!5!ProductKey] = rrt.productkey
               ,[Revision!5!MfgKey] = rrt.mfgkey
               ,[Revision!5!ReportName] = rrt.reportname
               ,[Revision!5!EndDate] = rrt.enddate
               ,[Revision!5!CurrPeriod] = isnull(rrt.CurrPeriod, 0)
               ,[Revision!5!Ytd] = isnull(rrt.ytd, 0)
               ,[Revision!5!LastPeriod] = isnull(rrt.LastPeriod, 0)
               ,[Revision!5!LastYear] = isnull(rrt.lastyear, 0)
               ,[Revision!5!ThreeYearAvg] = 0
               , --isnull(rrt.threeyearavg,0),
                [Revision!5!ThreeMonthAvg] = isnull(rrt.threemonthavg, 0)
               ,[Revision!5!ThreeYearStd] = 0
               , --isnull(abs(rrt.threeyearstd),0),
                [Revision!5!ThreeMonthStd] = isnull(abs(rrt.threemonthstd), 0)
               ,[Revision!5!AmtRevised] = rrt.revamt
               ,[Revision!5!DataRecord] = rrt.datarecord
               ,[Comments!6!Error] = NULL
               ,[Comments!6!Warning] = NULL
               ,[Comments!6!Note] = NULL
               ,[Comments!6!LineDetail] = NULL
      FROM      @subresults AS sr
      INNER JOIN @resultstable AS rt ON rt.subjobkey = sr.subjobkey
      INNER JOIN @revresultstable AS rrt ON rrt.masterreportkey = rt.reportkey
                                            AND master = 1
      UNION ALL
      SELECT DISTINCT
                Tag = 6
               ,Parent = 4
               ,[SUBMISSION_RESULTS!1!Key] = NULL
               ,[Results!2!MfgKey] = @mfgkey
               ,[Results!2!MfgName] = @mfgname
               ,[Results!2!EndDate] = @enddate
               ,[Results!2!JobKey] = sr.subjobkey
               ,[Results!2!Status] = sr.status
               ,[Results!2!StartDate] = @begindate
               ,[Results!2!AllZeros] = sr.allzeros
               ,[ReportClass!3!Name] = rt.datarev
               ,[Report!4!PlogKey] = rt.plogkey
               ,[Report!4!ReportKey] = rt.reportkey
               ,[Report!4!GroupKey] = rt.groupkey
               ,[Report!4!ProductKey] = rt.productkey
               ,[Report!4!ProductName] = rt.productname
               ,[Report!4!MfgKey] = rt.mfgkey
               ,[Report!4!ReportName] = rt.reportname
               ,[Report!4!EndDate] = rt.enddate
               ,[Report!4!CurrPeriod] = isnull(rt.CurrPeriod, 0)
               ,[Report!4!Ytd] = isnull(ytd, 0)
               ,[Report!4!LastPeriod] = isnull(rt.LastPeriod, 0)
               ,[Report!4!LastYear] = isnull(rt.lastyear, 0)
               ,[Report!4!ThreeYearAvg] = 0
               , --isnull(rt.threeyearavg, 0),
                [Report!4!ThreeMonthAvg] = isnull(rt.threemonthavg, 0)
               ,[Report!4!ThreeYearStd] = 0
               , --isnull(abs(rt.threeyearstd), 0),
                [Report!4!ThreeMonthStd] = isnull(abs(rt.threemonthstd), 0)
               ,[Report!4!DataRecord] = rt.datarecord
               ,[Report!4!Type] = reporttype
               ,[Revision!5!PlogKey] = NULL
               ,[Revision!5!ReportKey] = NULL
               ,[Revision!5!GroupKey] = NULL
               ,[Revision!5!ProductKey] = NULL
               ,[Revision!5!MfgKey] = NULL
               ,[Revision!5!ReportName] = NULL
               ,[Revision!5!EndDate] = NULL
               ,[Revision!5!CurrPeriod] = NULL
               ,[Revision!5!Ytd] = NULL
               ,[Revision!5!LastPeriod] = NULL
               ,[Revision!5!LastYear] = NULL
               ,[Revision!5!ThreeYearAvg] = NULL
               ,[Revision!5!ThreeMonthAvg] = NULL
               ,[Revision!5!ThreeYearStd] = NULL
               ,[Revision!5!ThreeMonthStd] = NULL
               ,[Revision!5!AmtRevised] = NULL
               ,[Revision!5!DataRecord] = NULL
               ,[Comments!6!Error] = error
               ,[Comments!6!Warning] = warning
               ,[Comments!6!Note] = note
               ,[Comments!6!LineDetail] = se.mfgkey
      FROM      @subresults AS sr
      INNER JOIN @resultstable AS rt ON rt.subjobkey = sr.subjobkey
--inner join subresults on sr.subjobkey = subresults.subjobkey
      INNER JOIN submission.suberrors as se ON se.subresultskey = rt.subresultskey
      LEFT OUTER JOIN fmanufacturer AS fm ON se.mfgkey = fm.mfgfactkey
      UNION ALL
      SELECT DISTINCT
                Tag = 6
               ,Parent = 5
               ,[SUBMISSION_RESULTS!1!Key] = NULL
               ,[Results!2!MfgKey] = @mfgkey
               ,[Results!2!MfgName] = @mfgname
               ,[Results!2!EndDate] = @enddate
               ,[Results!2!JobKey] = sr.subjobkey
               ,[Results!2!Status] = sr.status
               ,[Results!2!StartDate] = @begindate
               ,[Results!2!AllZeros] = sr.allzeros
               ,[ReportClass!3!Name] = rt.datarev
               ,[Report!4!PlogKey] = rt.plogkey
               ,[Report!4!ReportKey] = rt.reportkey
               ,[Report!4!GroupKey] = rt.groupkey
               ,[Report!4!ProductKey] = rt.productkey
               ,[Report!4!ProductName] = rt.productname
               ,[Report!4!MfgKey] = rt.mfgkey
               ,[Report!4!ReportName] = rt.reportname
               ,[Report!4!EndDate] = rt.enddate
               ,[Report!4!CurrPeriod] = isnull(rt.CurrPeriod, 0)
               ,[Report!4!Ytd] = isnull(rt.ytd, 0)
               ,[Report!4!LastPeriod] = isnull(rt.LastPeriod, 0)
               ,[Report!4!LastYear] = isnull(rt.lastyear, 0)
               ,[Report!4!ThreeYearAvg] = 0
               , --isnull(rt.threeyearavg, 0),
                [Report!4!ThreeMonthAvg] = isnull(rt.threemonthavg, 0)
               ,[Report!4!ThreeYearStd] = 0
               , --isnull(abs(rt.threeyearstd), 0),
                [Report!4!ThreeMonthStd] = isnull(abs(rt.threemonthstd), 0)
               ,[Report!4!DataRecord] = rt.datarecord
               ,[Report!4!Type] = reporttype
               ,[Revision!5!PlogKey] = rrt.plogkey
               ,[Revision!5!ReportKey] = rrt.reportkey
               ,[Revision!5!GroupKey] = rrt.groupkey
               ,[Revision!5!ProductKey] = rrt.productkey
               ,[Revision!5!MfgKey] = rrt.mfgkey
               ,[Revision!5!ReportName] = rrt.reportname
               ,[Revision!5!EndDate] = rrt.enddate
               ,[Revision!5!CurrPeriod] = isnull(rrt.CurrPeriod, 0)
               ,[Revision!5!Ytd] = isnull(rrt.ytd, 0)
               ,[Revision!5!LastPeriod] = isnull(rrt.LastPeriod, 0)
               ,[Revision!5!LastYear] = isnull(rrt.lastyear, 0)
               ,[Revision!5!ThreeYearAvg] = 0
               , --isnull(rrt.threeyearavg,0),
                [Revision!5!ThreeMonthAvg] = isnull(rrt.threemonthavg, 0)
               ,[Revision!5!ThreeYearStd] = 0
               , --isnull(abs(rrt.threeyearstd),0),
                [Revision!5!ThreeMonthStd] = isnull(abs(rrt.threemonthstd), 0)
               ,[Revision!5!AmtRevised] = rrt.revamt
               ,[Revision!5!DataRecord] = rrt.datarecord
               ,[Comments!6!Error] = error
               ,[Comments!6!Warning] = warning
               ,[Comments!6!Note] = note
               ,[Comments!6!LineDetail] = se.mfgkey
      FROM      @subresults AS sr
      INNER JOIN @resultstable AS rt ON rt.subjobkey = sr.subjobkey
--inner join subresults on sr.subjobkey = subresults.subjobkey
      INNER JOIN @revresultstable AS rrt ON rrt.masterreportkey = rt.reportkey
                                            AND master = 1
      INNER JOIN submission.suberrors as se ON se.subresultskey = rrt.subresultskey
      LEFT OUTER JOIN fmanufacturer AS fm ON se.mfgkey = fm.mfgfactkey
      ORDER BY  [SUBMISSION_RESULTS!1!Key]
               ,[Results!2!JobKey]
               ,[ReportClass!3!Name]
               ,[Report!4!ProductName]
               ,[Report!4!ReportName]
               ,[Report!4!EndDate]
               ,[Revision!5!EndDate]
               ,[Comments!6!Note]
      FOR       XML EXPLICIT

--return

