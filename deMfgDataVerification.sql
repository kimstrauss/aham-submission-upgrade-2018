USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[deMfgDataVerification]    Script Date: 2/22/2018 12:08:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from rmfg 
--inner join sdsize on rmfg.sizesetkey = sdsize.sizesetkey
--where mfgkey = 121 and measuresetkey = 4
--and dim1key <> 0
--and sizesetkey in (443, 432, 428, 488)

--select * from sdsize where sizesetkey in (443, 432, 428, 488)

--deMfgDataVerification 121, 2012, 4, 0

ALTER PROCEDURE [Submission].[deMfgDataVerification]
    (
      @mfgkey INT,
      @cyear INT,
      @councilkey INT,
	  @ntuser varchar(50),
      @productgroup INT = 0 
    )
AS 
/*  deMfgDataVerification creates a year to date report for all 
    accepted data for the specified products for the specified company.  
    It also indicates whether the value data is complete for the year
    or not.  this is for use in the data verification report.
	Created by Kim Strauss
		   12/6/05
		   12/28/12 updated for canada
		   1/18/16 make sure the pedestals and refrigerator drawers show up on the right council and aren't double
		   2/7/18 kds Pass in username and add check on user's permission to see the data
*/

 --declare @mfgkey int
 --declare @cyear int
 --declare @productgroup int  --0 for all
 --declare @councilkey int
 --declare @ntuser varchar(50) = 'kstrauss'
 --set @mfgkey =10
 --set @cyear = 2017
 --set @productgroup = 0
 --set @councilkey = 1
 --drop table #t, #unitdata, #unitrevs, #valdata, #valrevs, #rstdev, #davg, 
 --	#unitdev, #totals, #valdev, #d, #dv

    SET NOCOUNT ON;

    DECLARE @mfgname CHAR(128);
    SET @mfgname = ( SELECT
                        MfgName
                     FROM
                        DMfg
                     WHERE
                        MfgKey = @mfgkey );

    DECLARE @rmfglistval TABLE
        (
          mfgreportkey INT,
          productkey INT,
          marketsetkey INT
        );

    DECLARE @rmfglistwkl TABLE
        (
          mfgreportkey INT,
          productkey INT,
          marketsetkey INT
        );

	if (select count(*) from ContactRoles as cr where cr.ntuser = @ntuser and cr.mfgkey in (@mfgkey, 0) and seecodata = 1) = 0 begin
		print('This user does not have permission to see company data for this company')
		return
	end

--find the list of report keys to include for values
--look specifically for value reports
    INSERT  INTO @rmfglistval
            SELECT
                MfgReportKey,RM.ProductKey,MarketSetKey
            FROM
                vRMfg as rm
                INNER JOIN SDSize ON Rm.SizeSetKey = SDSize.SizeSetKey
            WHERE
                MfgKey = @mfgkey
                AND ( rm.GroupKey = @productgroup
                      OR @productgroup = 0
                    )
                AND rm.CouncilKey = @councilkey
                AND @cyear BETWEEN YEAR(RM.BeginDate) AND YEAR(RM.EndDate)
                AND MeasureSetKey = 4  -- values
                AND FreqKey > 1
                AND ActivitySetKey = 2
                AND NOT ( RM.ProductKey IN ( 99,100,102,103 )
                          AND @cyear = 2005
                        )
                AND NOT ( @councilkey = 4
                          AND Dim1Key > 0
                        ); --in canada just want full products, no sizes

--include combination reports - like weekly reporters
    INSERT  INTO @rmfglistval
            SELECT
                MfgReportKey,RM.ProductKey,MarketSetKey
            FROM
                vRMfg as rm
            WHERE
                MfgKey = @mfgkey
                AND ( rm.GroupKey = @productgroup
                      OR @productgroup = 0
                    )
                AND rm.CouncilKey = @councilkey
                AND @cyear BETWEEN YEAR(RM.BeginDate) AND YEAR(RM.EndDate)
                AND rm.master = 1
                AND RM.ProductKey NOT IN ( SELECT
                                                productkey
                                             FROM
                                                @rmfglistval )
                AND FreqKey > 1
                AND ActivitySetKey = 2
                AND NOT ( RM.ProductKey IN ( 99,100,102,103 )
                          AND @cyear = 2005
                        );

--find weekly reports
    INSERT  INTO @rmfglistwkl
            SELECT
                MfgReportKey,RM.ProductKey,MarketSetKey
            FROM
                vRMfg as rm
            WHERE
                MfgKey = @mfgkey
                AND ( rm.GroupKey = @productgroup
                      OR @productgroup = 0
                    )
                AND rm.CouncilKey = @councilkey
                AND @cyear BETWEEN YEAR(RM.BeginDate) AND YEAR(RM.EndDate)
                AND FreqKey = 5
                AND rm.master = 1
                AND ActivitySetKey = 2
                AND NOT ( RM.ProductKey IN ( 99,100,102,103 )
                          AND @cyear = 2005
                        ); --dont include ventilation hoods and blowers

--then look for monthly products
    INSERT  INTO @rmfglistwkl
            SELECT
                mfgreportkey,productkey,marketsetkey
            FROM
                @rmfglistval AS r
            WHERE
                NOT EXISTS ( SELECT
                                *
                             FROM
                                @rmfglistwkl AS rw
                             WHERE
                                rw.productkey = r.productkey
                                AND rw.marketsetkey = rw.marketsetkey );

    INSERT  INTO @rmfglistwkl
            SELECT
                MfgReportKey,ProductKey,MarketSetKey
            FROM
                RMfg
            WHERE
                MfgKey = @mfgkey
                AND RMfg.ProductKey IN ( 126,119 )
                AND @councilkey = 1
                AND FreqKey = 3
                AND @cyear BETWEEN YEAR(RMfg.BeginDate) AND YEAR(RMfg.EndDate)
                AND NOT EXISTS ( SELECT
                                    *
                                 FROM
                                    @rmfglistwkl AS rw
                                 WHERE
                                    rw.productkey = RMfg.ProductKey
                                    AND rw.marketsetkey = rw.marketsetkey );


--figure out what the last date is that everything has been accepted for
    DECLARE @numNotAccepted TINYINT;
    SET @numNotAccepted = ( SELECT
                                COUNT(*)
                            FROM
                                MfgPlog
                                INNER JOIN @rmfglistval AS r ON r.mfgreportkey = MfgPlog.MfgReportKey
                                                                AND YEAR(MfgPlog.EndDate) = @cyear
                                                                AND MfgPlog.EndDate = MfgPlog.PostPeriod
                                                                AND StatusKey < 24 ); 

    DECLARE @minDateNotAccepted SMALLDATETIME;
    SET @minDateNotAccepted = ISNULL(( SELECT
                                        MIN(MfgPlog.EndDate)
                                       FROM
                                        MfgPlog
                                        INNER JOIN @rmfglistval AS r ON r.mfgreportkey = MfgPlog.MfgReportKey
                                                                        AND YEAR(MfgPlog.EndDate) = @cyear
                                                                        AND MfgPlog.EndDate = MfgPlog.PostPeriod
                                                                        AND MfgPlog.EndDate IS NOT NULL
                                                                        AND StatusKey < 24 ),'12/31/2050');


    DECLARE @maxenddate SMALLDATETIME;
    SET @maxenddate = ( SELECT
                            MAX(EndDate)
                        FROM
                            MfgPlog
                            INNER JOIN @rmfglistval AS r ON r.mfgreportkey = MfgPlog.MfgReportKey
                                                            AND YEAR(MfgPlog.EndDate) = @cyear
                                                            AND MfgPlog.EndDate = MfgPlog.PostPeriod
                                                            AND MfgPlog.EndDate IS NOT NULL
                                                            AND MfgPlog.EndDate < @minDateNotAccepted
                            INNER JOIN DCalendar ON DDate = MfgPlog.EndDate
                                                    AND EndOfMonth = 1
                                                    AND @councilkey > 1 );

    IF @maxenddate IS NULL
        BEGIN
            SET @maxenddate = ( SELECT
                                    MAX(EndDate)
                                FROM
                                    MfgPlog
                                    INNER JOIN @rmfglistval AS r ON r.mfgreportkey = MfgPlog.MfgReportKey
                                                                    AND YEAR(MfgPlog.EndDate) = @cyear
                                                                    AND MfgPlog.EndDate = MfgPlog.PostPeriod
                                                                    AND MfgPlog.EndDate IS NOT NULL
                                                                    AND MfgPlog.EndDate < @minDateNotAccepted
                                    INNER JOIN DCalendar ON DDate = MfgPlog.EndDate
                                                            AND EndOfWkMonth = 1
                                                            AND @councilkey = 1 );

        END;

--find the current data for the unit reports
    SELECT
        fm.EndDate,fm.mfgReportKey,productkey,marketsetkey,munits = SUM(MUnits)
    INTO
        #d
    FROM
        at_fManufacturer AS fm
        INNER JOIN MfgPlog AS mp ON mp.MfgPlogKey = fm.mfgplogKey
                                    AND mp.StatusKey >= 24
        INNER JOIN @rmfglistwkl AS rm ON rm.mfgreportkey = fm.mfgReportKey
    WHERE
        YEAR(fm.EndDate) = @cyear
        AND fm.EndDate <= @maxenddate
    GROUP BY
        fm.EndDate,productkey,marketsetkey,fm.mfgReportKey;


--find the revisions for the unit reports
    SELECT
        mp1.EndDate,mp.PostPeriod,productkey,marketsetkey,munits = SUM(ISNULL(fm.MUnits,0))--, mvalue = sum(mvalue) 
    INTO
        #unitrevs
    FROM
        #d AS mp1
        LEFT JOIN MfgPlog AS mp ON mp.MfgReportKey = mp1.mfgReportKey
                                   AND mp.EndDate = mp1.EndDate
                                   AND mp.PostPeriod > mp.EndDate
                                   AND mp.StatusKey >= 24
        LEFT JOIN at_fManufacturer AS fm ON fm.mfgplogKey = mp.MfgPlogKey
    GROUP BY
        mp1.EndDate,productkey,marketsetkey,mp.PostPeriod;

--find the current data for the value reports
    SELECT
        fm.EndDate,fm.mfgReportKey,productkey,marketsetkey,mvalue = SUM(MValue)
    INTO
        #dv
    FROM
        at_fManufacturer AS fm
        INNER JOIN MfgPlog AS mp ON mp.MfgPlogKey = fm.mfgplogKey
                                    AND mp.StatusKey >= 24
        INNER JOIN @rmfglistval AS rm ON rm.mfgreportkey = fm.mfgReportKey
    WHERE
        YEAR(fm.EndDate) = @cyear
        AND fm.EndDate <= @maxenddate
    GROUP BY
        fm.EndDate,productkey,marketsetkey,fm.mfgReportKey;

--find the revisions for the value reports
    SELECT
        mp1.EndDate,mp.PostPeriod,productkey,marketsetkey,mvalue = SUM(ISNULL(fm.MValue,0))--, mvalue = sum(mvalue) 
    INTO
        #valrevs
    FROM
        #dv AS mp1
        LEFT JOIN MfgPlog AS mp ON mp.MfgReportKey = mp1.mfgReportKey
                                   AND mp.EndDate = mp1.EndDate
                                   AND mp.PostPeriod > mp.EndDate
                                   AND mp.StatusKey >= 24
        LEFT JOIN at_fManufacturer AS fm ON fm.mfgplogKey = mp.MfgPlogKey
    GROUP BY
        mp1.EndDate,productkey,marketsetkey,mp.PostPeriod;

--create a list of the product/market combinations
    DECLARE @prodlist TABLE
        (
          productkey INT,
          marketsetkey INT
        );
    INSERT  INTO @prodlist
            SELECT DISTINCT
                productkey,marketsetkey
            FROM
                #d;

--find the standard deviations and calculate the totals.....
    SELECT
        ProductName,MarketSetName,sd = ( SELECT
                                            STDEVP(munits)
                                         FROM
                                            #unitrevs AS u
                                         WHERE
                                            u.productkey = t.productkey
                                            AND u.marketsetkey = t.marketsetkey ) * 100 / ( SELECT
                                                                                                CASE WHEN AVG(munits) = 0 THEN 1
                                                                                                     ELSE ISNULL(AVG(munits),1)
                                                                                                END
                                                                                            FROM
                                                                                                #d AS d
                                                                                            WHERE
                                                                                                d.productkey = t.productkey
                                                                                                AND d.marketsetkey = t.marketsetkey ),
        sv = ( SELECT
                STDEVP(mvalue)
               FROM
                #valrevs AS u
               WHERE
                u.productkey = t.productkey
                AND u.marketsetkey = t.marketsetkey ) * 100 / ( SELECT
                                                                    CASE WHEN AVG(mvalue) = 0 THEN 1
                                                                         ELSE ISNULL(AVG(mvalue),1)
                                                                    END
                                                                FROM
                                                                    #dv AS d
                                                                WHERE
                                                                    d.productkey = t.productkey
                                                                    AND d.marketsetkey = t.marketsetkey ),unitavg = ( SELECT
                                                                                                                        AVG(munits)
                                                                                                                      FROM
                                                                                                                        #d AS d
                                                                                                                      WHERE
                                                                                                                        d.productkey = t.productkey
                                                                                                                        AND d.marketsetkey = t.marketsetkey ),
        valavg = ( SELECT
                    AVG(mvalue)
                   FROM
                    #dv AS d
                   WHERE
                    d.productkey = t.productkey
                    AND d.marketsetkey = t.marketsetkey ),munits = ( SELECT
                                                                        SUM(munits)
                                                                     FROM
                                                                        #d AS d
                                                                     WHERE
                                                                        t.productkey = d.productkey
                                                                        AND d.marketsetkey = t.marketsetkey ),
        mvalue = ( SELECT
                    SUM(mvalue)
                   FROM
                    #dv AS dv
                   WHERE
                    dv.productkey = t.productkey
                    AND dv.marketsetkey = t.marketsetkey )
    INTO
        #rstdev
    FROM
        @prodlist AS t
        INNER JOIN DProduct AS dp ON t.productkey = dp.ProductKey
        INNER JOIN SDMarket AS sm ON sm.MarketSetKey = t.marketsetkey;



select
		ReadyToVerify = CASE WHEN @numNotAccepted = 0
                     AND MONTH(@maxenddate) = 12
                     AND DATEPART(dd,@maxenddate) = 31 THEN 1
                ELSE 0
                END, 
		LastEndDate = isnull(@maxenddate, '1/1/1900'), 
		ManufacturerName = @mfgname, 
		ProductName = CASE WHEN @councilkey = 2 THEN ProductName + ' - ' + MarketSetName
                         ELSE ProductName
                      END, 
		Units = munits, 
		Value = isnull(mvalue, 0), 
		UnitStDev = CASE WHEN unitavg > 0 THEN ROUND(CAST(ISNULL(sd,0) AS MONEY),1)
                         ELSE 0
                    END,
		ValueStDev = CASE WHEN valavg > 0 THEN ROUND(CAST(ISNULL(sv,0) AS MONEY),1)
                         ELSE 0
                    END
		from #rstdev
		order by manufacturername, productname





