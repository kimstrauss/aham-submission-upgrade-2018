USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickSubmissionSetStatus]    Script Date: 2/8/2018 12:11:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER Procedure  [Submission].[ickSubmissionSetStatus](
--	@reportDate date, 
	@subjobkey int, 
	@ntuser varchar(50), 
	@plogtable submission.PlogTableType readonly

--	@doc varchar(8000)
	)

as

/* ickSubmissionSetStatus changes each of the plogs in the @doc list to the new
	status as specified by @newStatus.  It then returns a list of the documents
	submitted.
	Created by Kim Strauss
	4/16/02
*/

--drop table #t
/*declare @doc varchar(8000)
set @doc = '<HAI_PROCESSING ReportDate="5/31/04" JobKey="4633"> 
	<PLog Key="1011604"/>
	<PLog Key="1011605"/>
	<PLog Key="1011600"/>
	<PLog Key="1011601"/>
	<PLog Key="1011602"/>
	<PLog Key="1011603"/>
	<PLog Key="1011606"/>
	<PLog Key="1007452"/>
</HAI_PROCESSING>'
*/
set nocount on
declare	@NewStatus tinyint 
--declare @idoc int
declare @errorcount int
--declare @ntuser varchar(50)
declare @curdate datetime
declare @SubmissionKey int
declare @mfgname varchar(50)
declare @postdate smalldatetime
declare @mfgkey int

declare @ploglist table(
mfgplogkey integer,
postdate smalldatetime,
subjobkey int)

declare @WarningList table(
mfgplogkey integer)

declare @reporttable table(
mfgplogkey int,
mfgreportkey int, 
reportname varchar(250),
enddate smalldatetime,
datarev varchar(20),
catalogsetkey int,
catalogsetname varchar(30))

declare @flashlist table(
mfgplogkey integer primary key, 
flmfgreportkey integer)


declare @errorlist table(
mfgplogkey integer, 
errorcode integer,
Message varchar(250))
 
--Get data out of XML and into temp tables
set @errorcount = 0
--set @ntuser = dbo.fnNTUSER()
set @curdate = getdate()

--EXEC sp_xml_preparedocument @idoc OUTPUT, @doc

--insert into @ploglist(mfgplogkey, postdate, subjobkey)
--SELECT    mfgplogkey, postdate, subjobkey
--FROM       OPENXML (@idoc, '/HAI_PROCESSING/PLog',2)
--            WITH (mfgplogkey int '@Key',
--		  postdate smalldatetime '../@ReportDate',
--		  SubJobKey int '../@JobKey')

--EXEC sp_xml_removedocument @idoc
set @postdate = (select enddate from submission.subjobs
	where subjobkey = @submissionkey)

insert into @ploglist(mfgplogkey, postdate, subjobkey)
select plogkey, @postdate, @subjobkey 
from @plogtable

--select * from @ploglist

set @submissionkey = (select top 1 subjobkey from @ploglist)
set @mfgkey = (select mfgkey from submission.subjobs
	where subjobkey = @submissionkey)
set @mfgname = (select mfgname from dmfg 
	inner join submission.subjobs on subjobs.mfgkey = dmfg.mfgkey
	where subjobkey = @submissionkey)
--select @postdate

select pl.mfgplogkey, enddate = min(mp.enddate)
into #t
from @ploglist as pl
inner join mfgplog as mp2 on pl.mfgplogkey = mp2.mfgplogkey
inner join mfgplog as mp on mp.mfgreportkey = mp2.mfgreportkey
	and mp.enddate >= mp2.enddate
	and mp.statuskey < 24
	and mp.enddate = mp.postperiod
inner join rmfg on rmfg.mfgreportkey = mp.mfgreportkey
inner join dcalendar on mp.enddate = ddate and ((endofweek = 1 and (rmfg.calendar = 0 or rmfg.freqkey = 5))
	or (endofmonth = 1 and rmfg.calendar =1 and rmfg.freqkey < 5))
group by pl.mfgplogkey

union all

select pl.mfgplogkey, min(mp1.enddate)
from @ploglist as pl
inner join mfgplog as mp2 on pl.mfgplogkey = mp2.mfgplogkey
inner join rmfg as r2 on r2.mfgreportkey = mp2.mfgreportkey
inner join rmfg as r1 on r1.mfgreportkey <> r2.mfgreportkey
		and r1.freqkey = r2.freqkey
		and r1.catalogsetkey = r2.catalogsetkey
		and r1.marketsetkey = r2.marketsetkey
		and r1.mfgkey = r2.mfgkey
inner join mfgplog as mp1 on mp1.mfgreportkey = r1.mfgreportkey
	and mp1.enddate >= mp2.enddate
	and mp1.statuskey < 24
	and mp1.enddate = mp1.postperiod
inner join dcalendar on mp1.enddate = ddate and ((endofweek = 1 and (r1.calendar = 0 or r1.freqkey = 5))
	or (endofmonth = 1 and r1.calendar =1 and r1.freqkey < 5))
where not exists(select * from mfgplog as mp where mp.mfgreportkey = mp2.mfgreportkey
				and mp.statuskey < 24
				and mp.enddate > mp2.enddate
				and mp.enddate <> mp.postperiod)
group by pl.mfgplogkey

update @ploglist
set postdate = enddate
from @ploglist as pl
inner join #t as t on t.mfgplogkey = pl.mfgplogkey

--select * from @ploglist

select @newstatus = case when Jobtype='S' then 16 when jobtype='A' then 24 else 0 end from submission.subjobs where subjobkey=@submissionkey

if @newstatus=24 and isnull((Select count(*) from duser where ntuser=@ntuser and opeiops=1),0) < 1 begin
SELECT  tag=1,parent=0,
	[SubConfirm!1!Error]='User does not have permission to Accept reports',
	[SubConfirm!1!UserName]=@ntuser,
	[SubConfirm!1!SubmissionKey]=@submissionkey,
	[SubConfirm!1!TimeSubmitted]=getdate()
return
end



-- need to check permissions if want to change status to something smaller.
insert into @warninglist
select pl.mfgplogkey
from @ploglist as pl
inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
where mfgplog.statuskey > (@newstatus + 8)




insert into @errorlist
select wl.mfgplogkey, 1, 'User does not have sufficient permission to change this status'
from @Warninglist as wl
where mfgplogkey not in(select distinct wl.mfgplogkey from @warninglist as wl
inner join mfgplog on mfgplog.mfgplogkey = wl.mfgplogkey
inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vmodelpermissions as vmp on vmp.mfgkey = rmfg.mfgkey
	and vmp.productkey = rmfg.productkey
	and ntuser = @ntuser
	and seclevel >= 10)

insert into @errorlist
select pl.mfgplogkey, 1, 'User does not have permission to change this status'
from @ploglist as pl
where mfgplogkey not in(select distinct pl.mfgplogkey from @ploglist as pl
inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vmodelpermissions as vmp on vmp.mfgkey = rmfg.mfgkey
	and vmp.productkey = rmfg.productkey
	and ntuser = @ntuser
	and submitdata = 1)
and mfgplogkey not in(select distinct mfgplogkey from @errorlist)



--Set status.  If the new status is submit or higher and there is no 
--official submission date, set it.  Otherwise, don't. 

Begin tran
if @newstatus >= 16 and @newstatus < 24 begin
  update mfgplog
  set statuskey = @newstatus, 
  statusdate = @curdate,
  OffSubmitDate = isnull(offsubmitdate, @curdate),
  postperiod = isnull(postperiod, postdate),
  submitkey = @submissionkey
  from @ploglist as pl
  inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
  where pl.mfgplogkey not in(select distinct mfgplogkey from @errorlist)	

end
else if @newstatus > 23 begin	-- accepting
  update mfgplog
  set statuskey = @newstatus, 
  statusdate = @curdate,
  acceptkey = @submissionkey
  from @ploglist as pl
  inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
  where pl.mfgplogkey not in(select distinct mfgplogkey from @errorlist)
end
else begin	-- just changing
  update mfgplog
  set statuskey = @newstatus, 
  statusdate = @curdate
  from @ploglist as pl
  inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey
  where pl.mfgplogkey not in(select distinct mfgplogkey from @errorlist)
end
commit


/*
insert into @reporttable
select pt.mfgplogkey, rmfg.mfgreportkey, 
case rmfg.activitysetkey when 2 then isnull(rmfg.rptname, dfreq.freqname + ' ' + catalogsetname + ' ' + productname + ' ' + activitysetname)
else isnull(rmfg.rptname, dfreq.freqname + ' ' + catalogsetname + ' ' + productname + ' ' + activitysetname + ' to ' + geosetname) end,
mfgplog.enddate, 
case when mfgplog.enddate = mfgplog.postperiod then 'Data'
	else 'Revision' end,
sdcatalog.catalogsetkey, catalogsetname
from @ploglist as pt
inner join mfgplog on mfgplog.mfgplogkey = pt.mfgplogkey
inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
inner join dfreq on dfreq.freqkey = rmfg.freqkey
inner join sdactivity on sdactivity.activitysetkey = rmfg.activitysetkey
inner join sdcatalog on sdcatalog.catalogsetkey = rmfg.catalogsetkey
inner join sdgeo on sdgeo.geosetkey = rmfg.geosetkey
inner join dproduct as dpt on dpt.productkey = rmfg.productkey

/* Fill and submit any flash reports that are associated with these reports
   if this is a submit */
--if @newstatus = 16 begin

--exec FlashMakeModels @mfgkey, @postdate		-- make sure models and markets exist

/* find list of flash reports to fill and submit */
/*  insert into @flashlist
  select distinct mfgplog.mfgplogkey,  vfa.flmfgreportkey
  from @reporttable as rt
  inner join vFlashActXRef as vfa on vfa.actmfgreportkey = rt.mfgreportkey
  inner join mfgplog on mfgplog.mfgreportkey = vfa.flmfgreportkey
	and mfgplog.enddate = rt.enddate
	and mfgplog.statuskey < 16
  where not exists(select * from vFlashActXRef as vfa2
		inner join mfgplog as mp2 on vfa2.actmfgreportkey = mp2.mfgreportkey
				and mp2.enddate = rt.enddate
		where vfa2.flmfgreportkey = vfa.flmfgreportkey
		and vfa2.actmfgreportkey <> vfa.actmfgreportkey
		and mp2.statuskey < 16)	*/
		/* true when there are multiple actuals that go into a flash and 
		   one of them is not yet submitted */

/*force fill these reports to be equal to the actuals */
/* change the ones that already exist */
/*  update fmanufacturer
  set munits = vamt.munits
  from @flashlist as fl
  inner join mfgplog on fl.mfgplogkey = mfgplog.mfgplogkey
  inner join vFlashFillAmounts as vamt on vamt.flmfgreportkey = mfgplog.mfgreportkey
		and vamt.enddate = mfgplog.enddate
  inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
  inner join dmodel on rmfg.productkey = dmodel.productkey	--find flash model
		and rmfg.mfgkey = dmodel.mfgkey
		and sizemodel = 1
  inner join fmanufacturer as fm on fm.mfgreportkey = rmfg.mfgreportkey
		and fm.enddate = mfgplog.enddate
		and fm.modelkey = dmodel.modelkey */

/* add the ones that don't */
/*  insert into fmanufacturer(mfgreportkey, enddate, marketkey, activitykey,
  modelkey, serialnumber, geokey, channelkey, usekey, catalogkey, munits, mvalue)
  select rmfg.mfgreportkey, mfgplog.enddate,0, 1, dmodel.modelkey, '', 6722,
  0, 0, 2, vamt.munits, 0
  from @flashlist as fl
  inner join mfgplog on fl.mfgplogkey = mfgplog.mfgplogkey
  inner join vFlashFillAmounts as vamt on vamt.flmfgreportkey = mfgplog.mfgreportkey
		and vamt.enddate = mfgplog.enddate
  inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
  inner join dmodel on rmfg.productkey = dmodel.productkey	--find flash model
		and rmfg.mfgkey = dmodel.mfgkey
		and sizemodel = 1
  where not exists(select * from fmanufacturer as fm 
		where fm.mfgreportkey = rmfg.mfgreportkey
		and fm.enddate = mfgplog.enddate
		and fm.modelkey = dmodel.modelkey)
*/

/* submit the reports just filled */
/*  update mfgplog
  set statuskey = @newstatus, 
  statusdate = @curdate,
  OffSubmitDate = isnull(offsubmitdate, @curdate),
  postperiod = isnull(postperiod, @postdate),
  submitkey = @submissionkey
  from @flashlist as fl
  inner join mfgplog on mfgplog.mfgplogkey = fl.mfgplogkey
  where fl.mfgplogkey not in(select distinct mfgplogkey from @errorlist)	
*/
/* add flash reports to output report */
/*  insert into @reporttable
  select ft.mfgplogkey, rmfg.mfgreportkey, 
  case rmfg.activitysetkey when 2 then isnull(rmfg.rptname, dfreq.freqname + ' ' + catalogsetname + ' ' + productname + ' ' + activitysetname)
  else isnull(rmfg.rptname, dfreq.freqname + ' ' + catalogsetname + ' ' + productname + ' ' + activitysetname + ' to ' + geosetname) end,
  mfgplog.enddate,   'Data',
  sdcatalog.catalogsetkey, catalogsetname
  from @Flashlist as ft
  inner join mfgplog on mfgplog.mfgplogkey = ft.mfgplogkey
  inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
  inner join dfreq on dfreq.freqkey = rmfg.freqkey
  inner join sdactivity on sdactivity.activitysetkey = rmfg.activitysetkey
  inner join sdcatalog on sdcatalog.catalogsetkey = rmfg.catalogsetkey
  inner join sdgeo on sdgeo.geosetkey = rmfg.geosetkey
  inner join dproduct as dpt on dpt.productkey = rmfg.productkey
*/
--end

--set @errorcount = (select count(*) from @errorlist)
--if @errorcount = 0 begin
  --insert into @errorlist values(0, 0, 'All reports have been submitted')
--end

SELECT distinct 1       as Tag, 
         NULL     	as Parent,
	null as			[SubConfirm!1!Key],
	null as			[Job!2!UserName],
	null as 		[Job!2!SubmissionKey],
	null as			[Job!2!TimeSubmitted],
	null as			[Job!2!MfgKey],
	null as			[Job!2!MfgName],
	null as 		[Job!2!PostDate],
	null	as		[Catalog!3!Key],
	null	as		[Catalog!3!Name],
	null	as		[Report!4!PlogKey],
	null	as		[Report!4!ReportKey],
	null	as		[Report!4!ReportName],
	null	as		[Report!4!EndDate],
	null	as		[Report!4!DataRevision],
	null	as		[Error!5!ErrorCode],
	null	as		[Error!5!Message]

UNION ALL
SELECT distinct 2, 1,
	null,
	rtrim(@ntuser), 
	@submissionKey,
	@curdate, @mfgkey,
	@mfgname, @postdate,
	null, null,
	null, null, null, null, null,
	null, null

UNION ALL
SELECT distinct 3, 2,
	null,
	rtrim(@ntuser), 
	@submissionKey,
	@curdate, @mfgkey,
	@mfgname, @postdate,
	catalogsetkey, catalogsetname,
	null, null, null, null, null,
	null, null
from @reporttable

UNION ALL
SELECT distinct 4, 3,
	null,
	rtrim(@ntuser),
	@submissionKey,
	@curdate, @mfgkey, 
	@mfgname, @postdate,
	catalogsetkey, catalogsetname,
	mfgplogkey, 
	mfgreportkey, 
	reportname,
	enddate,
	datarev,
	null, null
from @reporttable


UNION ALL
SELECT distinct 5, 4,
	null,
	rtrim(@ntuser),
	@submissionKey,
	@curdate, @mfgkey,
	@mfgname, @postdate,
	catalogsetkey, catalogsetname,
	rt.mfgplogkey, 
	mfgreportkey, 
	reportname,
	enddate,
	datarev,
	errorcode,
	message
from @reporttable as rt
inner join @errorlist as et on et.mfgplogkey = rt.mfgplogkey

order by [SubConfirm!1!Key], [Job!2!SubmissionKey],[Catalog!3!Name],
	[Report!4!DataRevision],[Report!4!EndDate],[Report!4!ReportName],[Error!5!ErrorCode]

--for XML explicit





















*/