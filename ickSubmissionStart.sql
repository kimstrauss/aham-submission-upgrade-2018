USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickSubmissionStart]    Script Date: 2/12/2018 12:52:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO












ALTER Procedure  [Submission].[ickSubmissionStart] (
	@mfgkey int,
	@reportdate datetime, 
	@ntuser varchar(50),
	@jobType char(1)='S',
	@plogTable submission.PlogTableType readonly 
)
as

/* IckSubmissionStart creates a submission job, and saves the members of the job.
	it returns a -1 if there is already an open submission, and the submission
	job key otherwise. 
	created by Kim Strauss
	3/18/02
	2/9/18 kds Change schema, and use the identity for subjobkey
*/


declare @subJobkey int



-- make sure there's not an open job, then make one
if (select count(*) from submission.SubJobExecution where RequestingUserName = @ntuser and Status < 100 ) > 0 begin
 return -1
end

insert into submission.SubJobExecution(RequestingUserName, JobBeginDate, Status, MfgKey, ReportEndDate, JobType)
values (@ntuser, getdate(), 1, @mfgkey, @reportdate, @jobType)

set @subjobkey = (select cast(scope_identity() as int))

insert into submission.submembers(SubJobKey,mfgplogkey)
select @subJobkey, plogkey from @plogtable

return @subjobkey















