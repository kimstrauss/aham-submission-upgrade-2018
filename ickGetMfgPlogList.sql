USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickGetMfgPlogList]    Script Date: 2/22/2018 2:11:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [Submission].[ickGetMfgPlogList]
	@rdate [smalldatetime],
	@mfgkey [int],
	@catalogkey [int],
	@DateStatus [char](1), 
	@ntuser varchar(50)
--WITH EXECUTE AS CALLER
AS
/* ickGetMfgPlogList creates a list of all of the reports for the given date,
	mfgkey and catalogkey, 
	created by Kim Strauss
		8/29/02
	2/7/18 kds Pass in username
*/

-- declare
-- 	@rdate smalldatetime,
-- 	@mfgkey int,
-- 	@catalogkey int,
--	@datestatus char, 
--	@ntuser varchar(50)
---- 
-- set @rdate='12/31/17'
-- set @mfgkey=10
-- set @catalogkey=1
-- set @datestatus = 'S'
-- set @ntuser = 'kstrauss'

    set nocount on
    if isnull(( Select  count(*)
                from    duser
                where   ntuser = @ntuser
                        and opeiops = 1
              ), 0) < 1 
        begin
            --SELECT  tag = 1,
            --        parent = 0,
            --        [OpenReports!1!Error] = 'User does not have permission to modify plog details',
            --        [OpenReports!1!UserName] = @ntuser,
            --        [OpenReports!1!TimeSubmitted] = getdate()
            --for     xml explicit
            return 
        end
    declare @reporttable table
        (
          mfgkey int,
          mfgname varchar(75),
          catalogkey int,
          catalogname varchar(75),
          mfgreportkey int,
          mfgplogkey int,
          enddate smalldatetime,
          postperiod smalldatetime,
          reportname varchar(175),
          SubmitDate smalldatetime,
          published tinyint,
          statusdesc varchar(30),
          [message] varchar(1000),
          canchange tinyint,
          changelater tinyint,
          currentstatus varchar(256)
        )
    declare @revtable table
        (
          mfgkey int,
          mfgname varchar(75),
          catalogkey int,
          catalogname varchar(75),
          mfgreportkey int,
          mfgplogkey int,
          enddate smalldatetime,
          postperiod smalldatetime,
          reportname varchar(175),
          SubmitDate smalldatetime,
          published tinyint,
          statusdesc varchar(30),
          [message] varchar(280),
          canchange tinyint,
          changelater tinyint,
          currentstatus varchar(256)
        )

    insert  into @reporttable
            select  rm.mfgkey,
                    rm.mfgname,
                    rm.catalogkey,
                    catalogname,
                    rm.mfgreportkey,
                    mfgplogkey,
                    mfgplog.enddate,
                    postperiod,
					rm.rptname,
                    offsubmitdate,
                    0,
                    'Eligible',
                    '',
                    1,
                    0,
                    dcs.description
            from    mfgplog
                    inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
                    inner join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
            where   ( mfgplog.postperiod = @rdate
                      and mfgplog.enddate = @rdate
                    )
                    and rm.mfgkey = @mfgkey
                    and rm.catalogkey = @catalogkey
                    and ( @datestatus = 'S'
                          or offsubmitdate is not null
                          or dcs.coapproved = 1
                        )


    insert  into @revtable
            select  rm.mfgkey,
                    mfgname,
                    rm.catalogkey,
                    rm.catalogname,
                    rm.mfgreportkey,
                    mfgplogkey,
                    mfgplog.enddate,
                    postperiod,
					rm.rptname,
                    offsubmitdate,
                    0,
                    'Eligible',
                    '',
                    1,
                    0,
                    dcs.description
            from    mfgplog
                    inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
                    inner join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
            where   ( mfgplog.postperiod = @rdate
                      and mfgplog.enddate < @rdate
                    )
                    and mfgplog.enddate < @rdate
                    and rm.mfgkey = @mfgkey
                    and rm.catalogkey = @catalogkey
                    and ( @datestatus = 'S'
                          or offsubmitdate is not null
                          or dcs.coapproved = 1
                        )
--check if data published


    insert  into @revtable
            select  rm.mfgkey,
                    rm.mfgname,
                    rm.catalogkey,
                    rm.catalogname,
                    rm.mfgreportkey,
                    mfgplogkey,
                    mfgplog.enddate,
                    postperiod,
					rm.rptname,
                    offsubmitdate,
                    0,
                    'Eligible',
                    '',
                    1,
                    0,
                    dcs.description
            from    mfgplog
                    inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
                    inner join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
                    inner join dcalendar as dcal on dcal.ddate = mfgplog.enddate
                    inner join dcalendar as dcal2 on dcal2.inweek = dcal.inweek
                                                     and dcal.year = dcal2.year
                                                     and dcal.endofmonth = 1
                                                     and dcal2.endofweek = 1
                                                     and rm.freqkey = 5
                                                     and dcal.ddate <> dcal2.ddate
            where   ( dcal2.ddate = @rdate
                      and mfgplog.enddate = mfgplog.postperiod
                    )
                    and rm.mfgkey = @mfgkey
                    and rm.catalogkey = @catalogkey
                    and ( @datestatus = 'S'
                          or offsubmitdate is not null
                          or dcs.coapproved = 1
                        )

--select * from @revtable

    declare @pub table
        (
          mfgplogkey int primary key
        )
    insert  into @pub
            select distinct
                    mfgplogkey
            from    @reporttable as rt
                    inner join rmfginindustry as mii on mii.mfgreportkey = rt.mfgreportkey
                    inner join FIndRptSeg as firs on firs.rindustrykey = mii.rindustrykey
                                                     and firs.reportenddate = rt.enddate
                    inner join dindstatus as dis on dis.indstatuskey = firs.indstatuskey
                                                    and publishedanywhere = 1
    update  @reporttable
    set     published = 1
    from    @reporttable as rt
    where   exists ( select *
                     from   @pub as pub
                     where  pub.mfgplogkey = rt.mfgplogkey )
    delete  from @pub
    insert  into @pub
            select distinct
                    mfgplogkey
            from    @revtable as rt
                    inner join rmfginindustry as mii on mii.mfgreportkey = rt.mfgreportkey
                    inner join FIndRptSeg as firs on firs.rindustrykey = mii.rindustrykey
                                                     and firs.reportenddate = rt.postperiod
                    inner join dindstatus as dis on dis.indstatuskey = firs.indstatuskey
                                                    and publishedanywhere = 1
    update  @revtable
    set     published = 1
    from    @revtable as rt
    where   exists ( select *
                     from   @pub as pub
                     where  pub.mfgplogkey = rt.mfgplogkey )
--add wanings for the status change version
    if @datestatus = 'S' 
        begin
            update  @reporttable
            set     message = 'Unsubmitting this report will cause later periods to also be unsubmitted',
                    changelater = 1
            from    @reporttable as rt
            where   exists ( select *
                             from   mfgplog
                                    inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
                                                            and coapproved = 1
                             where  rt.mfgreportkey = mfgplog.mfgreportkey
                                    and mfgplog.postperiod > rt.postperiod )
            update  @reporttable
            set     message = 'Unable to change status after report has been published',
                    canchange = 0
            where   published = 1
            update  @revtable
            set     message = 'Unsubmitting this report will cause later periods to also be unsubmitted',
                    changelater = 1
            from    @revtable as rt
            where   exists ( select *
                             from   mfgplog
                                    inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
                                                            and coapproved = 1
                             where  rt.mfgreportkey = mfgplog.mfgreportkey
                                    and mfgplog.postperiod > rt.postperiod )
            update  @revtable
            set     message = 'Unable to change status after report has been published',
                    canchange = 0
            where   published = 1
        end
-- Can't submit any reports that are already in a submission job
    UPDATE  @revtable
    SET     canchange = 0,
            message = 'Currently being changed by another process'
    FROM    @revtable as rt
    WHERE   Canchange = 1
            and rt.mfgplogkey in ( select distinct
                                            mfgplogkey
                                   from     submission.submembers )	
    UPDATE  @reporttable
    SET     canchange = 0,
            message = 'Currently being changed by another process'
    FROM    @reporttable as rt
    WHERE   Canchange = 1
            and rt.mfgplogkey in ( select distinct
                                            mfgplogkey
                                   from     submission.submembers )	
-- check for previous open reports

    if ( select count(*)
         from   @revtable
         where  mfgreportkey not in ( select distinct
                                                mfgreportkey
                                      from      @reporttable )
       ) > 0 
        begin
            insert  into @reporttable
                    select distinct
                            mfgkey,
                            mfgname,
                            catalogkey,
                            catalogname,
                            mfgreportkey,
                            -999999 + mfgreportkey,
                            @rdate,
                            @rdate,
                            reportname,
                            submitdate,
                            published,
                            statusdesc,
                            message,
                            canchange,
                            changelater,
                            ''
                    from    @revtable as rt
                    where   rt.mfgreportkey not in ( select mfgreportkey
                                                     from   @reporttable )
        end

select EndDate = convert(varchar, @rDate, 101),
            ManufacturerName = rpt.mfgname,
            ManufacturerKey = rpt.mfgkey,
            CatalogName = rpt.catalogname,
            CatalogKey = rpt.catalogkey,
            ReportName = rpt.reportname,
            ReportKey = rpt.mfgreportkey,
            ReportMfgPlogKey = rpt.mfgplogkey,
            ReportEndDate = rpt.enddate,
            ReportSubmitDate = rpt.submitdate,
            ReportCanChange = rpt.canchange,
            ReportChangeLater = rpt.changelater,
            ReportCurrentStatus = rpt.currentstatus,
            ReportMessage = rpt.message,
            RevisionName = rvt.reportname,
            RevisionKey = rvt.mfgreportkey,
            RevisionMfgPlogKey = rvt.mfgplogkey,
            RevisionEndDate = rvt.enddate,
            RevisionSubmitDate = rvt.submitdate,
            RevisionCanChange = rvt.canchange,
            RevisionCurrentStatus = rvt.currentstatus,
            RevisionMessage = rvt.message
    FROM    @reporttable as rpt
            left join @revtable as rvt on rpt.mfgreportkey = rvt.mfgreportkey












