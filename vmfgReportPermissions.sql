USE [Ahamoper]
GO

/****** Object:  View [Submission].[vMfgReportPermissions]    Script Date: 2/9/2018 5:03:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [Submission].[vMfgReportPermissions]
AS
SELECT DISTINCT cr.ContactKey, cr.AssnKey, cr.NTUser, RMfg.MfgKey, mfgname,  RMfg.ProductKey, productname, 
MfgReportKey, cr.DealerKey, cr.SeeCoData, cr.EditCoData, cr.SeeIndData, cr.SeeModels, 
cr.EditModels, cr.SubmitData, cr.Responsible, cr.EditCodes, cr.SecLevel, cr.SeePublic, cr.SeeAll, dgroup.CouncilKey, councilname
FROM dbo.ContactRoles AS cr
     INNER JOIN dbo.RMfg ON(RMfg.ProductKey=cr.ProductKey AND cr.MfgKey=RMfg.MfgKey)
	 OR(cr.MfgKey=RMfg.MfgKey AND cr.ProductKey=0)
	 OR(cr.MfgKey=0 AND cr.ProductKey=RMfg.ProductKey)
	 OR(cr.MfgKey=0 AND cr.ProductKey=0)
     INNER JOIN dbo.DMGroup ON RMfg.ProductKey=DMGroup.ProductKey
     INNER JOIN dbo.DGroup ON DGroup.GroupKey=DMGroup.GroupKey
	 inner join dmfg on rmfg.mfgkey = dmfg.mfgkey 
	 inner join dproduct on dproduct.productkey = rmfg.productkey
	 inner join Dcouncil on dcouncil.councilkey = dgroup.CouncilKey
	 inner join YearDates on begdate between rmfg.begindate and rmfg.enddate and companydatapresent = 1

where seecodata + editcodata + seeinddata + seemodels + submitdata + seeall > 0
and rmfg.mfgkey not in (50, 52, 120)

GO


