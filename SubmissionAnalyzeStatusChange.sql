USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[SubmissionAnalyzeStatusChange]    Script Date: 3/6/2018 11:22:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






ALTER Procedure  [Submission].[SubmissionAnalyzeStatusChange](
	@mfgkey int, 
	@asofDate datetime, 
	@ntuser varchar(50), 
	@plogtable [Submission].[PlogTableType] readonly

)
as

/* SubmissionAnalyseStatusChange finds all of the reports that would change if you
   decide to make the proposed change
	created by Kim Strauss
	9/3/02
	12/10/12  changed length to max
	2/7/18  kds  Changed parameters to table input and schema
*/

--declare @doc varchar(8000)
--set @doc = '<HAI_PROCESSING MfgKey="50" AsOfDate="10/31/2002">
--  <PLog Key="77145" StatusKey="2" ChangeLater="0" /> 
--  <PLog Key="78471" StatusKey="16" ChangeLater="0" /> 
-- </HAI_PROCESSING>
--'
--declare 	@mfgkey int, 
--	@asofDate datetime, 
--	@ntuser varchar(50), 
--	@plogtable [Submission].[PlogTableType]


declare @idoc int
declare @errorcount int
--declare @ntuser varchar(50)
declare @ploglist table(
mfgplogkey int primary key,
newstatus int,
oldstatus int, 
changelater int,
unsubmit int,
rdate smalldatetime)
declare @newstat int

--Get data out of XML and into temp tables
set @errorcount = 0
--set @ntuser = dbo.fnNTUSER()

insert into @ploglist(mfgplogkey,newstatus,ChangeLater,rdate)
select plogkey, isnull([status], 16), changeflag, @asofDate
from @plogtable 

--EXEC sp_xml_preparedocument @idoc OUTPUT, @doc

--Insert into @ploglist(mfgplogkey,newstatus,ChangeLater,rdate)
--select mfgplogkey, status, changeLater, rdate
--FROM       OPENXML (@idoc, '/HAI_PROCESSING/PLog',2)
--            WITH (mfgplogkey int '@Key',
--		  status int '@StatusKey',
--		  changelater tinyint '@ChangeLater',
--		  rdate smalldatetime '../@AsOfDate')

--EXEC sp_xml_removedocument @idoc

update @ploglist
set oldstatus = mfgplog.statuskey,
unsubmit = 0
from @ploglist as pl
inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey

update @ploglist
set unsubmit = 1
where oldstatus > 15 and newstatus < 16

-- deal with ones that don't include an unsubmit

if (select count(*) from @ploglist where unsubmit = 1) > 0 begin

declare @plogs table (
origplog int,
revplog int,
newstatus int,
primary key(origplog, revplog))

-- find plogs for later periods that have been submitted - must unsubmit them as well.
if (select count(*) from @ploglist where changelater = 1 and unsubmit = 1) > 0 begin

insert into @ploglist 
select distinct mp1.mfgplogkey, pl.newstatus, mp1.statuskey, 0, pl.unsubmit, pl.rdate
from mfgplog as mp
inner join @ploglist as pl on mp.mfgplogkey = pl.mfgplogkey 
		and pl.changelater = 1
		and pl.unsubmit = 1
inner join mfgplog as mp1 on mp1.enddate > mp.enddate
		and mp1.mfgreportkey = mp.mfgreportkey
		and mp1.postperiod = mp1.enddate
inner join dcostatus as dc on mp1.statuskey = dc.dcostatuskey
		and coapproved = 1
where not exists(select mfgplogkey from @ploglist as pl2 where pl2.mfgplogkey = mp1.mfgplogkey)

end

end
/*
-- include any revisions submitted with the data
insert into @ploglist
select mp1.mfgplogkey, pl.newstatus, mp1.statuskey, 0, pl.unsubmit, pl.rdate
from mfgplog as mp
inner join @ploglist as pl on mp.mfgplogkey = pl.mfgplogkey
inner join mfgplog as mp1 on mp1.postperiod = mp.postperiod
		and mp1.enddate <> mp.enddate
		and mp1.mfgreportkey = mp.mfgreportkey
inner join dcostatus as dc on mp1.statuskey = dc.dcostatuskey

*/

update @ploglist
set oldstatus = mfgplog.statuskey,
unsubmit = 0
from @ploglist as pl
inner join mfgplog on mfgplog.mfgplogkey = pl.mfgplogkey

update @ploglist
set unsubmit = 1
where oldstatus > 15 and newstatus < 16

declare @reporttable table(
rdate smalldatetime,
mfgkey int, 
mfgname varchar(75),
catalogkey int, 
catalogname varchar(75),
mfgreportkey int, 
mfgplogkey int, 
enddate smalldatetime,
postperiod smalldatetime,
reportname varchar(175),
SubmitDate smalldatetime,
published tinyint,
oldstatuskey int,
newstatuskey int, 
oldstatdesc varchar(30),
newstatdesc varchar(30),
statusdesc varchar(30),
message varchar(280),
canchange tinyint,
unsubmit tinyint, 
changelater tinyint)

declare @revtable table(
rdate smalldatetime,
mfgkey int, 
mfgname varchar(75),
catalogkey int, 
catalogname varchar(75),
mfgreportkey int, 
mfgplogkey int, 
enddate smalldatetime,
postperiod smalldatetime,
reportname varchar(175),
SubmitDate smalldatetime,
published tinyint,
oldstatuskey int,
newstatuskey int, 
oldstatdesc varchar(30),
newstatdesc varchar(30),
statusdesc varchar(30),
message varchar(280),
canchange tinyint,
unsubmit tinyint, 
changelater tinyint)

insert into @reporttable
select distinct pl.rdate, rm.mfgkey, rm.mfgname, rm.catalogkey, rm.catalogname, rm.mfgreportkey, mfgplog.mfgplogkey, mfgplog.enddate, postperiod,
rm.rptname, 
offsubmitdate, 0,pl.oldstatus, pl.newstatus,dc1.[description], dc2.[description],
'Eligible','', 1, pl.unsubmit, 0
from mfgplog
inner join @ploglist as pl on pl.mfgplogkey = mfgplog.mfgplogkey
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
inner join dcostatus as dc1 on dc1.dcostatuskey = pl.oldstatus
inner join dcostatus as dc2 on dc2.dcostatuskey = pl.newstatus
where mfgplog.enddate = mfgplog.postperiod


insert into @revtable
select distinct pl.rdate, rm.mfgkey, rm.mfgname, rm.catalogkey, rm.catalogname, rm.mfgreportkey, mfgplog.mfgplogkey, mfgplog.enddate, postperiod,
rm.rptname,
offsubmitdate, 0,pl.oldstatus, pl.newstatus,dc1.[description], dc2.[description],
'Eligible', '', 1, pl.unsubmit, 0

from mfgplog
inner join @ploglist as pl on pl.mfgplogkey = mfgplog.mfgplogkey
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join dcostatus as dcs on dcs.dcostatuskey = mfgplog.statuskey
inner join dcostatus as dc1 on dc1.dcostatuskey = pl.oldstatus
inner join dcostatus as dc2 on dc2.dcostatuskey = pl.newstatus
where mfgplog.enddate <> mfgplog.postperiod


select mfgname, mfgkey, catalogname, catalogkey, mfgreportkey, mfgplogkey, 
reportenddate = rt.enddate, Unsubmit,  OldStatusKey,  OldStatDesc, NewStatusKey, NewStatDesc, DataRevision = 'D'  
from @reporttable as rt

select mfgname, mfgkey, catalogname, catalogkey, mfgreportkey, mfgplogkey, 
reportenddate = rt.enddate, Unsubmit,  OldStatusKey,  OldStatDesc, NewStatusKey, NewStatDesc, DataRevision = 'R'  
from @revtable as rt












