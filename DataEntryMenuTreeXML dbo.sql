USE [Ahamoper]
GO
/****** Object:  StoredProcedure [dbo].[DataEntryMenuTreeXML]    Script Date: 2/8/2018 1:14:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--DataEntryMenuTreeXML 124, 0, 'D'
 
create PROCEDURE [dbo].[DataEntryMenuTreeXML]
      (
       @mfgKey INT
      ,@Scope INT = 0
      ,@ListType CHAR(1) = 'D'
			
      )
AS ---DataEntryMenuTreeXML -- valid dates for a mfg
--	Created by Kim Strauss
--	8/27/04	
--  4/10/13  Changed it to only show those with valid dinputformkeys so that it doesn't show reports not valid for data entry
-- 
/* Declare @scope int, @mfgkey int, @listtype Char(1)
 Set @scope = 2006
 set @mfgkey = 2
 set @listtype = 'D'
 drop table #r,#st, #str, #rmaster
*/
-- 
      SET nocount ON

      DECLARE @begindate SMALLDATETIME
             ,@enddate SMALLDATETIME
      DECLARE @ValidYears TABLE (year INT PRIMARY KEY)


      INSERT    INTO @validyears
                SELECT DISTINCT
                        year(f.enddate)
                FROM    mfgplog AS f
                INNER JOIN rmfg ON rmfg.mfgreportkey = f.mfgreportkey
                WHERE   f.enddate > '1/1/2003'
                        AND rmfg.mfgkey = @mfgkey
      INSERT    INTO @validyears
                SELECT  0

      IF @Scope NOT IN (SELECT  Year
                        FROM    @validyears) 
         BEGIN
               SET @scope = 0
         END
      IF @scope = 0 
         BEGIN
               SELECT   @begindate = dateadd(m, -15, getdate())
                       ,@enddate = dateadd(dd, 1, getdate())
         END
      ELSE 
         BEGIN
               SELECT   @enddate = '12/31/' + str(@scope, 4)
               SELECT   @begindate = '1/1/' + str(@scope, 4)	
         END


      DECLARE @rptSec TABLE
              (
               mfgreportkey INT
              ,begindate SMALLDATETIME
              ,enddate SMALLDATETIME
              ,PRIMARY KEY (mfgReportKey, Begindate)
              )

      INSERT    INTO @rptsec
                SELECT  *
                FROM    dbo.fnSecurityInputReportKeys(@ListType)


      SELECT    dg.councilkey
               ,dg.groupkey
               ,cat.catalogkey
               ,f.freqkey
               ,mp.enddate
               ,rmfg.calendar
               ,CouncilName
               ,Freqname
               ,catalogname
               ,dmfg.mfgkey
               ,mfgname
               ,statuskey = 0
      INTO      #r
      FROM      Dgroup AS dg
      INNER JOIN dmgroup AS dm ON dm.groupkey = dg.groupkey
      INNER JOIN rmfg ON dm.productkey = rmfg.productkey
                         AND rmfg.mfgkey = @mfgkey
      INNER JOIN dmfg ON rmfg.mfgkey = dmfg.mfgkey
      INNER JOIN mfgplog AS mp ON mp.mfgreportkey = rmfg.mfgreportkey
                                  AND (mp.enddate BETWEEN @begindate AND @enddate
                                       OR (mp.enddate BETWEEN @begindate AND dateadd(dd, 28, @enddate)
                                           AND rmfg.catalogsetkey = 6
                                           AND @scope = 0)
                                       OR (mp.enddate BETWEEN @begindate AND dateadd(dd, 35, @enddate)
                                           AND createweekly = 1))
      INNER JOIN Dcouncil AS dc ON dc.councilkey = dg.councilkey
      INNER JOIN Dfreq AS f ON f.freqkey = rmfg.freqkey
      INNER JOIN smcatalog AS smc ON smc.catalogsetkey = rmfg.catalogsetkey
      INNER JOIN dcatalog AS cat ON cat.catalogkey = smc.catalogkey
      INNER JOIN dcalendar AS dcal ON dcal.ddate = mp.enddate
      WHERE     (EXISTS ( SELECT    *
                          FROM      @RptSec AS orf
                          WHERE     orf.mfgreportkey = rmfg.mfgreportkey
                                    AND mp.enddate BETWEEN orf.begindate AND orf.enddate ))
				and (dinputformkey > 0 or @listtype <> 'D')  -- propose adding this to make sure it's a valid editable report
                AND (f.freqkey <> 5
                     OR dcal.endofweek = 1)	-- make sure it only shows valid end dates for weeklies
                AND (f.freqkey <> 4
                     OR (dcal.endofmonth = 1
                         AND rmfg.calendar = 1)
                     OR (dcal.endofwkmonth = 1
                         AND rmfg.calendar = 0))
      GROUP BY  dg.councilkey
               ,dg.groupkey
               ,cat.catalogkey
               ,f.freqkey
               ,mp.enddate
               ,rmfg.calendar
               ,CouncilName
               ,Freqname
               ,catalogname
               ,mfgname
               ,dmfg.mfgkey
      UNION ALL
      SELECT    dg.councilkey
               ,dg.groupkey
               ,cat.catalogkey
               ,f.freqkey
               ,dcal2.ddate
               ,rmfg.calendar
               ,CouncilName
               ,Freqname
               ,catalogname
               ,dmfg.mfgkey
               ,mfgname
               ,statuskey = 0
      FROM      Dgroup AS dg
      INNER JOIN dmgroup AS dm ON dm.groupkey = dg.groupkey
      INNER JOIN rmfg ON dm.productkey = rmfg.productkey
                         AND rmfg.mfgkey = @mfgkey
      INNER JOIN dmfg ON rmfg.mfgkey = dmfg.mfgkey
      INNER JOIN mfgplog AS mp ON mp.mfgreportkey = rmfg.mfgreportkey
                                  AND (mp.enddate BETWEEN @begindate AND @enddate
                                       OR (mp.enddate BETWEEN @begindate AND dateadd(dd, 28, @enddate)
                                           AND rmfg.catalogsetkey = 6
                                           AND @scope = 0)
                                       OR (mp.enddate BETWEEN @begindate AND dateadd(dd, 35, @enddate)
                                           AND createweekly = 1))
      INNER JOIN Dcouncil AS dc ON dc.councilkey = dg.councilkey
      INNER JOIN Dfreq AS f ON f.freqkey = rmfg.freqkey
      INNER JOIN smcatalog AS smc ON smc.catalogsetkey = rmfg.catalogsetkey
      INNER JOIN dcatalog AS cat ON cat.catalogkey = smc.catalogkey
      INNER JOIN dcalendar AS dcal ON dcal.ddate = mp.enddate
      INNER JOIN dcalendar AS dcal2 ON dcal2.inweek = dcal.inweek
                                       AND dcal2.endofweek = 1
                                       AND dcal2.year = dcal.year
                                       AND dcal2.ddate <> dcal.ddate
      WHERE     (EXISTS ( SELECT    *
                          FROM      @RptSec AS orf
                          WHERE     orf.mfgreportkey = rmfg.mfgreportkey
                                    AND mp.enddate BETWEEN orf.begindate AND orf.enddate ))
				and (dinputformkey > 0 or @listtype <> 'D')  -- propose adding this to make sure it's a valid editable report
                AND f.freqkey = 5
                AND dcal.endofmonth = 1
                AND dcal.endofweek = 1	-- make sure it only shows valid end dates for weeklies
GROUP BY        dg.councilkey
               ,dg.groupkey
               ,cat.catalogkey
               ,f.freqkey
               ,dcal2.ddate
               ,rmfg.calendar
               ,CouncilName
               ,Freqname
               ,catalogname
               ,mfgname
               ,dmfg.mfgkey


      SELECT    r.councilkey
               ,r.groupkey
               ,r.catalogkey
               ,r.freqkey
               ,r.enddate
               ,r.calendar
               ,r.mfgkey
               ,minstatus = min(mfgplog.statuskey)
               ,maxstatus = max(mfgplog.statuskey)
      INTO      #st
      FROM      #r AS r
      INNER JOIN rmfg ON r.calendar = rmfg.calendar
                         AND r.freqkey = rmfg.freqkey
      INNER JOIN mfgplog ON rmfg.mfgreportkey = mfgplog.mfgreportkey
      INNER JOIN smcatalog AS smc ON smc.catalogsetkey = rmfg.catalogsetkey
      INNER JOIN dmgroup ON dmgroup.productkey = rmfg.productkey
      INNER JOIN dgroup ON dgroup.groupkey = dmgroup.groupkey
                           AND r.councilkey = dgroup.councilkey
      WHERE     r.enddate = mfgplog.enddate
                AND r.catalogkey = smc.catalogkey
                AND mfgplog.enddate = mfgplog.postperiod
                AND r.mfgkey = rmfg.mfgkey
      GROUP BY  r.councilkey
               ,r.groupkey
               ,r.catalogkey
               ,r.freqkey
               ,r.enddate
               ,r.calendar
               ,r.mfgkey


--select * from #r where enddate = '8/26/06' and freqkey = 4
--select * from #st where enddate = '8/26/06' and freqkey = 4

      UPDATE    #r
      SET       statuskey = 1	-- no data entered
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND maxstatus = 1


      UPDATE    #r
      SET       statuskey = 2	--'Some Data Entered'
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND minstatus = 1
                              AND maxstatus > 1

      UPDATE    #r
      SET       statuskey = 3	--'All Data Entered'
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND minstatus BETWEEN 2 AND 15

      UPDATE    #r
      SET       statuskey = 4	--'All Data Submitted'
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND minstatus BETWEEN 16 AND 23

      UPDATE    #r
      SET       statuskey = 5	--'All Data Accepted (Closed)'
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND minstatus > 23
	
      CREATE CLUSTERED INDEX koni ON #r
      (
      catalogkey
      )

      SELECT    r.councilkey
               ,r.groupkey
               ,r.catalogkey
               ,r.freqkey
               ,r.enddate
               ,r.calendar
               ,r.mfgkey
               ,minstatus = min(mfgplog.statuskey)
               ,maxstatus = max(mfgplog.statuskey)
      INTO      #str
      FROM      #r AS r
      INNER JOIN rmfg ON r.calendar = rmfg.calendar
                         AND r.freqkey = rmfg.freqkey
      INNER JOIN mfgplog ON rmfg.mfgreportkey = mfgplog.mfgreportkey
      INNER JOIN smcatalog AS smc ON smc.catalogsetkey = rmfg.catalogsetkey
      WHERE     r.enddate = mfgplog.enddate
                AND r.catalogkey = smc.catalogkey
                AND (mfgplog.enddate <> mfgplog.postperiod
                     OR mfgplog.postperiod IS NULL)
                AND r.mfgkey = rmfg.mfgkey
                AND r.statuskey IN (4, 5)
      GROUP BY  r.councilkey
               ,r.groupkey
               ,r.catalogkey
               ,r.freqkey
               ,r.enddate
               ,r.calendar
               ,r.mfgkey

      UPDATE    #r
      SET       statuskey = 11	--'Revisions Pending'
      FROM      #r AS r
      INNER JOIN #str AS st ON st.councilkey = r.councilkey
                               AND st.groupkey = r.groupkey
                               AND st.catalogkey = r.catalogkey
                               AND r.freqkey = st.freqkey
                               AND st.enddate = r.enddate
                               AND r.calendar = st.calendar
                               AND r.mfgkey = st.mfgkey
                               AND minstatus < 16

      UPDATE    #r
      SET       statuskey = 12	--'Revisions Submitted'
      FROM      #r AS r
      INNER JOIN #str AS st ON st.councilkey = r.councilkey
                               AND st.groupkey = r.groupkey
                               AND st.catalogkey = r.catalogkey
                               AND r.freqkey = st.freqkey
                               AND st.enddate = r.enddate
                               AND r.calendar = st.calendar
                               AND r.mfgkey = st.mfgkey
                               AND minstatus BETWEEN 16 AND 23

      SELECT DISTINCT
                councilkey
               ,councilname
               ,mfgkey
               ,mfgname
      INTO      #rmaster
      FROM      #r

      DELETE    FROM #r
      WHERE     statuskey IN (4, 5)
                AND @scope = 0
                AND @listtype = 'D' 

      DELETE    FROM #r
      WHERE     statuskey IN (5, 12, 11)
                AND @scope = 0
                AND @listtype = 'S' 

      DELETE    FROM #r
      WHERE     enddate < '12/30/03'
                AND @scope = 0


      SELECT DISTINCT
                tag = 1
               ,parent = NULL
               ,[Report!1!Title] = 'AHAM Input Report Viewer Menu'
               ,[Report!1!RunTime] = getdate()
               ,[Report!1!MfgKey] = mfgkey
               ,[Report!1!MfgName] = mfgname
               ,[Council!2!Key] = NULL
               ,[Council!2!Name] = NULL
               ,[Catalog!3!Key] = NULL
               ,[Catalog!3!Name] = NULL
               ,[Frequency!4!Key] = NULL
               ,[Frequency!4!Calendar] = NULL
               ,[Frequency!4!Name] = NULL
               ,[Frequency!4!Sort] = NULL
               ,[Date!5!Value] = NULL
               ,[Date!5!Status] = NULL
               ,[Date!5!Sort] = NULL
               ,[FilterYears!6!Value] = NULL
               ,[FilterYears!6!Text] = NULL
      FROM      #rmaster
      UNION
      SELECT DISTINCT
                tag = 2
               ,parent = 1
               ,[Report!1!Title] = 'AHAM Input Report Viewer Menu'
               ,[Report!1!RunTime] = NULL
               ,[Report!1!MfgKey] = NULL
               ,[Report!1!MfgName] = NULL
               ,[Council!2!Key] = #rmaster.Councilkey
               ,[Council!2!Name] = CouncilName
               ,[Catalog!3!Key] = NULL
               ,[Catalog!3!Name] = NULL
               ,[Frequency!4!Key] = NULL
               ,[Frequency!4!Calendar] = NULL
               ,[Frequency!4!Name] = NULL
               ,[Frequency!4!Sort] = NULL
               ,[Date!5!Value] = NULL
               ,[Date!5!Status] = NULL
               ,[Date!5!Sort] = NULL
               ,[FilterYears!6!Value] = NULL
               ,[FilterYears!6!Text] = NULL
      FROM      #rmaster
      GROUP BY  #rmaster.CouncilKey
               ,Councilname
      UNION
      SELECT DISTINCT
                tag = 3
               ,parent = 2
               ,[Report!1!Title] = 'AHAM Input Report Viewer Menu'
               ,[Report!1!RunTime] = NULL
               ,[Report!1!MfgKey] = NULL
               ,[Report!1!MfgName] = NULL
               ,[Council!2!Key] = #r.Councilkey
               ,[Council!2!Name] = Councilname
               ,[Catalog!3!Key] = #R.catalogkey
               ,[Catalog!3!Name] = CatalogName
               ,[Frequency!4!Key] = NULL
               ,[Frequency!4!Calendar] = NULL
               ,[Frequency!4!Name] = NULL
               ,[Frequency!4!Sort] = NULL
               ,[Date!5!Value] = NULL
               ,[Date!5!Status] = NULL
               ,[Date!5!Sort] = NULL
               ,[FilterYears!6!Value] = NULL
               ,[FilterYears!6!Text] = NULL
      FROM      #r
      GROUP BY  #r.CouncilKey
               ,#r.catalogkey
               ,catalogname
               ,Councilname
      UNION
      SELECT DISTINCT
                tag = 4
               ,parent = 3
               ,[Report!1!Title] = 'AHAM Input Report Viewer Menu'
               ,[Report!1!RunTime] = NULL
               ,[Report!1!MfgKey] = NULL
               ,[Report!1!MfgName] = NULL
               ,[Council!2!Key] = #r.Councilkey
               ,[Council!2!Name] = Councilname
               ,[Catalog!3!Key] = #R.catalogkey
               ,[Catalog!3!Name] = Catalogname
               ,[Frequency!4!Key] = #R.Freqkey
               ,[Frequency!4!Calendar] = #R.Calendar
               ,[Frequency!4!Name] = case WHEN #r.calendar = 1 THEN 'Calendar '
                                          ELSE ''
                                     END + FreqName
               ,[Frequency!4!Sort] = 0 - #r.freqkey
               ,[Date!5!Value] = NULL
               ,[Date!5!Status] = NULL
               ,[Date!5!Sort] = NULL
               ,[FilterYears!6!Value] = NULL
               ,[FilterYears!6!Text] = NULL
      FROM      #r
      GROUP BY  #r.CouncilKey
               ,councilname
               ,#r.catalogkey
               ,Catalogname
               ,#r.freqkey
               ,case WHEN #r.calendar = 1 THEN 'Calendar '
                     ELSE ''
                END + FreqName
               ,#r.calendar
      UNION
      SELECT DISTINCT
                tag = 5
               ,parent = 4
               ,[Report!1!Title] = 'AHAM Input Report Viewer Menu'
               ,[Report!1!RunTime] = NULL
               ,[Report!1!MfgKey] = NULL
               ,[Report!1!MfgName] = NULL
               ,[Council!2!Key] = #r.Councilkey
               ,[Council!2!Name] = Councilname
               ,[Catalog!3!Key] = #R.catalogkey
               ,[Catalog!3!Name] = Catalogname
               ,[Frequency!4!Key] = #R.Freqkey
               ,[Frequency!4!Calendar] = #R.Calendar
               ,[Frequency!4!Name] = case WHEN #r.calendar = 1 THEN 'Calendar '
                                          ELSE ''
                                     END + FreqName
               ,[Frequency!4!Sort] = 0 - #r.freqkey
               ,[Date!5!Value] = EndDate
               ,[Date!5!Status] = statuskey
               ,[Date!5!Sort] = datediff(dd, enddate, '1/1/1990')
               ,[FilterYears!6!Value] = NULL
               ,[FilterYears!6!Text] = NULL
      FROM      #r
      WHERE     (@scope <> 0
                 OR #r.enddate IN (SELECT TOP 4
                                            x.EndDate
                                   FROM     #r AS x
                                   WHERE    x.groupkey = #r.groupkey
                                            AND x.catalogkey = #r.catalogkey
                                            AND x.freqkey = #r.freqkey
                                            AND x.Calendar = #R.calendar
                                   ORDER BY x.Enddate DESC))
      UNION
      SELECT DISTINCT
                tab = 6
               ,parent = 1
               ,[Report!1!Title] = 'AHAM Input Report Viewer Menu'
               ,[Report!1!RunTime] = NULL
               ,[Report!1!MfgKey] = NULL
               ,[Report!1!MfgName] = NULL
               ,[Council!2!Key] = NULL
               ,[Council!2!Name] = NULL
               ,[Catalog!3!Key] = NULL
               ,[Catalog!3!Name] = NULL
               ,[Frequency!4!Key] = NULL
               ,[Frequency!4!Calendar] = NULL
               ,[Frequency!4!Name] = NULL
               ,[Frequency!4!Sort] = NULL
               ,[Date!5!Value] = NULL
               ,[Date!5!Status] = NULL
               ,[Date!5!Sort] = NULL
               ,[FilterYears!6!Value] = y.year
               ,[FilterYears!6!Text] = case WHEN y.year = 0 THEN 'Current'
                                            ELSE str(y.year, 4)
                                       END
      FROM      @ValidYears AS y
      WHERE     y.year <> @scope
      ORDER BY  [Council!2!Name]
               ,[Catalog!3!Key]
               ,[Frequency!4!Sort]
               ,[Frequency!4!Calendar]
               ,[Date!5!Sort]
               ,[FilterYears!6!Text]
      FOR       XML EXPLICIT
 





































