USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickSubmissionExistCurrent]    Script Date: 2/22/2018 2:46:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure  [Submission].[ickSubmissionExistCurrent] (@ntuser varchar(50))
as
/* ickSubmissionExistCurrent looks for an open submission and returns information on
	it's key and completion status or an indicator of none open.  
	For quick checks of open submission from web site. 
	Created by Kim Strauss
	3/18/02
	2/7/18 kds pass in username, change schema
*/

set nocount on
--declare @ntuser varchar(50)
--set @ntuser = dbo.fnNTUSER()
declare @job table(
subjobkey int,
OpenJob bit,
results bit)

if (select count(*) from submission.SubJobExecution where RequestingUsername = @ntuser and [status] <100) = 0 begin
  insert into @job values(0, 0, 0)
endelse begin
  insert into @job 
  select subjobkey, 1, status from submission.SubJobExecution
  where RequestingUsername = @ntuser and [status] < 100
end

select openjob, subjobkey, completed = case when results > 0 then 1 else 0 end
from @job

--SELECT distinct Tag = 1, 
--        Parent = NULL,
--	[SUBMISSION!1!OpenJobs] = openjob,
--	[SUBMISSION!1!JobKey] = subJobkey,
--	[SUBMISSION!1!Completed] = case when results > 0 then 1
--				else 0 end
--from @job	
--for XML explicit




