USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickGetSubmissionStatus]    Script Date: 2/22/2018 2:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






ALTER Procedure  [Submission].[ickGetSubmissionStatus](
	@mfgkey int,
	@rdate datetime, 
	@ntuser varchar(50),
	@filter char(1) = 'A')
as
/* returns all of the information needed for the submission status screen 
	created by Kim Strauss	
	2/25/02
	2/22/18 kds Remove xml output, change rmfg to vrmfg to avoid lookups
*/

--set nocount on
--declare @ntuser varchar(50), @mfgkey int, @filter char(1)
--set @ntuser = dbo.fnNTUSER()
--declare @OPEIOps smallint
--select @opeiops = case when (Select Count(*) from duser
--	where ntuser=@ntuser and opeiops=1) > 0 then 1 else 0 end
--	declare @rdate smalldatetime 
--	set @rdate = '12/31/17' 
--	set @mfgkey = 14
--	set @filter = 'A' 

declare @catalogtable table(
mfgkey int,
catalogkey int,
catalogname varchar(50),
catalogid varchar(24),
inprocess int, 
notstarted int, 
submitted int,
revsubmitted int, 
approved int, 
errors int,
questioned int,
tobereviewed int,
laststatusdate datetime,
totaltobedone int,
totaldone int,
finalstatus varchar(50))

declare @statustable table(
assnName varchar(50),
mfgkey int,
mfgname varchar(50),
laststatusdate datetime,
finalstatus varchar(50))
declare @inprocess table(
catalogkey int primary key,
num int)
declare @submitted table(
catalogkey int primary key,
num int)
declare @revsubmitted table(
catalogkey int primary key,
num int)
declare @notstarted table(
catalogkey int primary key,
num int)
declare @approved table(
catalogkey int primary key,
num int)
declare @errors table(
catalogkey int primary key,
num int)
declare @questioned table(
catalogkey int primary key,
num int)
declare @tobereviewed table(
catalogkey int primary key,
num int)

insert into  @inprocess select rm.catalogkey, count(distinct mfgplogkey)
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
where postperiod = @rdate and rm.mfgkey = @mfgkey and coapproved = 0 and anyactivity=1
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))
group by rm.catalogkey

insert into @notstarted select rm.catalogkey, count(distinct mfgplogkey)
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
where postperiod = @rdate and rm.mfgkey = @mfgkey and anyactivity = 0
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))
group by rm.catalogkey

insert into @submitted select rm.catalogkey, count(distinct mfgplogkey)
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
where postperiod = @rdate and rm.mfgkey = @mfgkey and coapproved = 1
	and postperiod = mfgplog.enddate
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))
group by rm.catalogkey

insert into @revsubmitted select rm.catalogkey, count(distinct mfgplogkey)
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
where postperiod = @rdate and rm.mfgkey = @mfgkey and coapproved = 1
	and not postperiod = mfgplog.enddate
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))
group by rm.catalogkey

insert into @approved select rm.catalogkey, count(distinct mfgplogkey)
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))
inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
where postperiod = @rdate and rm.mfgkey = @mfgkey and tapapproved = 1 and 
	errors = 0 and challenged = 0 
group by rm.catalogkey

insert into @errors select rm.catalogkey, count(distinct mfgplogkey)
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
where postperiod = @rdate and rm.mfgkey = @mfgkey and errors = 1 and coapproved = 1
	and publishedanywhere = 0
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))
group by rm.catalogkey

insert into @questioned select rm.catalogkey, count(distinct mfgplogkey)
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
where postperiod = @rdate and rm.mfgkey = @mfgkey and challenged = 1 and 
	coapproved = 1 and publishedanywhere = 0
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))
group by rm.catalogkey

insert into @tobereviewed select rm.catalogkey, count(distinct mfgplogkey)
from mfgplog
inner join vrmfg as rm on rm.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rm.mfgreportkey
	and submitdata = 1
inner join dcostatus on mfgplog.statuskey = dcostatus.dcostatuskey
where postperiod = @rdate and rm.mfgkey = @mfgkey and errors = 0 and coapproved = 1
	and tapapproved = 0 and challenged = 0
	and rm.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rm.freqkey = 4)
	or (@filter = 'W' and rm.freqkey = 5) 
	or (@filter = 'Q' and rm.freqkey in (3) and rm.catalogsetkey <> 6)
	or (@filter = 'F' and rm.catalogsetkey = 6)
	or (@filter = 'Y'and rm.freqkey = 1))
group by rm.catalogkey

insert into @catalogtable
select @mfgkey, dc.catalogkey, catalogname, catalogabbrev, ip.num, 
	ns.num, sub.num, rev.num, app.num, err.num, q.num, tbr.num, 
	max(statusdate),(ip.num+ns.num+sub.num+rev.num), 
	(app.num + err.num + q.num+ tbr.num),
	case when ns.num > 0 and (sub.num = 0 or sub.num is null) 
			and (ip.num = 0 or ip.num is null) then 'Not Started'
	     when (ns.num = 0 or ns.num is null) and (ip.num = 0 or ip.num is null)
			 and sub.num > 0 and (err.num = 0 or err.num is null)
			 and (q.num = 0 or q.num is null) and (tbr.num = 0 or tbr.num is null)
			 then 'Complete'
	     else 'Incomplete' end
from mfgplog
inner join rmfg on rmfg.mfgreportkey = mfgplog.mfgreportkey
inner join submission.vMfgReportPermissions as vrp on vrp.ntuser = @ntuser
	and vrp.mfgreportkey = rmfg.mfgreportkey
	and submitdata = 1
inner join smcatalog as smc on smc.catalogsetkey = rmfg.catalogsetkey
inner join dcatalog as dc on dc.catalogkey = smc.catalogkey
left outer join  @inprocess as ip on dc.catalogkey = ip.catalogkey
left outer join  @notstarted as ns on dc.catalogkey = ns.catalogkey
left outer join  @Submitted as sub on dc.catalogkey = sub.catalogkey
left outer join  @revsubmitted as rev on dc.catalogkey = rev.catalogkey
left outer join  @approved as app on dc.catalogkey = app.catalogkey
left outer join  @errors as err on dc.catalogkey = err.catalogkey
left outer join  @questioned as q on dc.catalogkey = q.catalogkey
left outer join  @tobereviewed as tbr on dc.catalogkey = tbr.catalogkey
where postperiod = @rdate and rmfg.mfgkey = @mfgkey
	and rmfg.status <> 'R'
	and (@filter = 'A' or (@Filter = 'M' and rmfg.freqkey = 4)
	or (@filter = 'W' and rmfg.freqkey = 5) 
	or (@filter = 'Q' and rmfg.freqkey in (3) and rmfg.catalogsetkey <> 6)
	or (@filter = 'F' and rmfg.catalogsetkey = 6)
	or (@filter = 'Y'and rmfg.freqkey = 1))
group by rmfg.mfgkey, dc.catalogkey, dc.catalogname, dc.catalogabbrev,
ip.num, ns.num, sub.num, rev.num, app.num, err.num, q.num, tbr.num

insert into @statustable 
select rtrim(assnabbrev), vmp.mfgkey, vmp.mfgname, max(laststatusdate), max(finalstatus)
from submission.vModelPermissions as vmp
inner join dassn on vmp.assnkey = dassn.assnkey
inner join @catalogtable as ct on ct.mfgkey = vmp.mfgkey
where @ntuser = vmp.ntuser and vmp.mfgkey = @mfgkey
		and SubmitData = 1
group by assnabbrev, vmp.mfgkey, vmp.mfgname

	select 
	assnname,
	--@opeiOps	,
	st.mfgkey, 
	mfgname,
	enddate = @rdate,
	st.laststatusdate,
	st.finalstatus,
	catalogkey,
	catalogname,
	catalogid,
	InProcess = isnull(inprocess, 0),
	NotStarted = isnull(notstarted, 0),
	Submitted = isnull(submitted, 0),
	RevSubmitted = isnull(revsubmitted, 0),
	Approved = isnull(approved, 0),
	Errors = isnull(errors, 0),
	Questioned = isnull(questioned, 0),
	ToBeReviewed = isnull(tobereviewed, 0),
	ct.laststatusdate,
	TotalToBeDone = isnull(totaltobedone, 0),
	TotalDone = isnull(totaldone, 0),
	CT.finalstatus
from @statustable as st
inner join @catalogtable as ct on ct.mfgkey = st.mfgkey








