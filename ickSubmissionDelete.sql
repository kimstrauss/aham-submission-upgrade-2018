USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[ickSubmissionDelete]    Script Date: 2/7/2018 11:50:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure  [Submission].[ickSubmissionDelete](
	@SubJobKey int, 
	@ntuser varchar(50))
as
/* Removes all references to the current submission job
	Created by Kim STrauss
	3/18/02
	2/7/18 kds Change schema - change the deletion so that rather than deleting records, we just set the status to 0 (no longer active)
*/
set nocount on

update Submission.SubJobs set status = 0 where @SubJobKey = SubJobKey


--delete from submission.suberrors 
--where subresultskey in (select distinct subresultskey from submission.subresults
--			where subjobkey = @subjobkey)

--delete from submission.SubResults
--where subjobkey = @subjobkey

--delete from submission.SubMembers 
--where SubJobkey = @subjobkey

--delete from submission.SubJobs
--where Subjobkey = @subjobkey




