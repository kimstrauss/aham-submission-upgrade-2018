USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[DataEntryMenuTreeXML]    Script Date: 2/22/2018 9:51:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--submission.DataEntryMenuTreeXML 124, 0, 'D'
 
ALTER PROCEDURE [Submission].[DataEntryMenuTreeXML]
      (
       @mfgKey INT
	  ,@ntuser varchar(50)
      ,@Scope INT = 0
      ,@ListType CHAR(1) = 'D'
			
      )
AS 
---DataEntryMenuTreeXML -- valid dates for a mfg
--	Created by Kim Strauss
--	8/27/04	
--  4/10/13  Changed it to only show those with valid dinputformkeys so that it doesn't show reports not valid for data entry
--  2/7/18 kds pass in username, change schema, change output and use vrmfg instead of most lookups
------ 
 --Declare @scope int, @mfgkey int, @listtype Char(1), @ntuser varchar(50)
 --Set @scope = 2017
 --set @mfgkey = 2
 --set @listtype = 'D'
 --set @ntuser = 'kstrauss'
 --drop table #r,#st, #str, #rmaster

-- 
      SET nocount ON

      DECLARE @begindate SMALLDATETIME
             ,@enddate SMALLDATETIME
      DECLARE @ValidYears TABLE (year INT PRIMARY KEY)


      INSERT    INTO @validyears
                SELECT DISTINCT
                        year(f.enddate)
                FROM    mfgplog AS f
                INNER JOIN rmfg ON rmfg.mfgreportkey = f.mfgreportkey
                WHERE   f.enddate > '1/1/2003'
                        AND rmfg.mfgkey = @mfgkey
      INSERT    INTO @validyears
                SELECT  0

      IF @Scope NOT IN (SELECT  Year
                        FROM    @validyears) 
         BEGIN
               SET @scope = 0
         END
      IF @scope = 0 
         BEGIN
               SELECT   @begindate = dateadd(m, -15, getdate())
                       ,@enddate = dateadd(dd, 1, getdate())
         END
      ELSE 
         BEGIN
               SELECT   @enddate = '12/31/' + str(@scope, 4)
               SELECT   @begindate = '1/1/' + str(@scope, 4)	
         END


      DECLARE @rptSec TABLE
              (
               mfgreportkey INT
              ,begindate SMALLDATETIME
              ,enddate SMALLDATETIME
              ,PRIMARY KEY (mfgReportKey, Begindate)
              )

      INSERT    INTO @rptsec
                SELECT  *
                FROM    fnSecurityInputReportKeysPassInUser(@ntuser, @ListType)


      SELECT    rm.councilkey
               ,rm.groupkey
               ,rm.catalogkey
               ,rm.freqkey
               ,mp.enddate
               ,rm.calendar
               ,rm.CouncilName
               ,rm.Freqname
               ,rm.catalogname
               ,rm.mfgkey
               ,rm.mfgname
               ,statuskey = 0
      INTO      #r
      FROM      Dgroup AS dg
      INNER JOIN vrmfg as rm ON dg.groupkey = rm.groupkey
                         AND rm.mfgkey = @mfgkey
      INNER JOIN mfgplog AS mp ON mp.mfgreportkey = rm.mfgreportkey
                                  AND (mp.enddate BETWEEN @begindate AND @enddate
                                       OR (mp.enddate BETWEEN @begindate AND dateadd(dd, 28, @enddate)
                                           AND rm.catalogsetkey = 6
                                           AND @scope = 0)
                                       OR (mp.enddate BETWEEN @begindate AND dateadd(dd, 35, @enddate)
                                           AND createweekly = 1))
      INNER JOIN dcalendar AS dcal ON dcal.ddate = mp.enddate
      WHERE     (EXISTS ( SELECT    *
                          FROM      @RptSec AS orf
                          WHERE     orf.mfgreportkey = rm.mfgreportkey
                                    AND mp.enddate BETWEEN orf.begindate AND orf.enddate ))
				and (dinputformkey > 0 or @listtype <> 'D')  -- propose adding this to make sure it's a valid editable report
                AND (rm.freqkey <> 5
                     OR dcal.endofweek = 1)	-- make sure it only shows valid end dates for weeklies
                AND (rm.freqkey <> 4
                     OR (dcal.endofmonth = 1
                         AND rm.calendar = 1)
                     OR (dcal.endofwkmonth = 1
                         AND rm.calendar = 0))
      GROUP BY  rm.councilkey
               ,rm.groupkey
               ,rm.catalogkey
               ,rm.freqkey
               ,mp.enddate
               ,rm.calendar
               ,rm.CouncilName
               ,rm.Freqname
               ,rm.catalogname
               ,rm.mfgname
               ,rm.mfgkey
      UNION ALL
      SELECT    rm.councilkey
               ,rm.groupkey
               ,rm.catalogkey
               ,rm.freqkey
               ,dcal2.ddate
               ,rm.calendar
               ,rm.CouncilName
               ,rm.Freqname
               ,rm.catalogname
               ,rm.mfgkey
               ,rm.mfgname
               ,statuskey = 0
      FROM      Dgroup AS dg
      INNER JOIN vrmfg as rm ON dg.groupkey = rm.groupkey
                         AND rm.mfgkey = @mfgkey
      INNER JOIN mfgplog AS mp ON mp.mfgreportkey = rm.mfgreportkey
                                  AND (mp.enddate BETWEEN @begindate AND @enddate
                                       OR (mp.enddate BETWEEN @begindate AND dateadd(dd, 28, @enddate)
                                           AND rm.catalogsetkey = 6
                                           AND @scope = 0)
                                       OR (mp.enddate BETWEEN @begindate AND dateadd(dd, 35, @enddate)
                                           AND createweekly = 1))
      INNER JOIN dcalendar AS dcal ON dcal.ddate = mp.enddate
      INNER JOIN dcalendar AS dcal2 ON dcal2.inweek = dcal.inweek
                                       AND dcal2.endofweek = 1
                                       AND dcal2.year = dcal.year
                                       AND dcal2.ddate <> dcal.ddate
      WHERE     (EXISTS ( SELECT    *
                          FROM      @RptSec AS orf
                          WHERE     orf.mfgreportkey = rm.mfgreportkey
                                    AND mp.enddate BETWEEN orf.begindate AND orf.enddate ))
				and (dinputformkey > 0 or @listtype <> 'D')  -- propose adding this to make sure it's a valid editable report
                AND rm.freqkey = 5
                AND dcal.endofmonth = 1
                AND dcal.endofweek = 1	-- make sure it only shows valid end dates for weeklies
GROUP BY        rm.councilkey
               ,rm.groupkey
               ,rm.catalogkey
               ,rm.freqkey
               ,dcal2.ddate
               ,rm.calendar
               ,rm.CouncilName
               ,rm.Freqname
               ,rm.catalogname
               ,rm.mfgname
               ,rm.mfgkey


      SELECT    r.councilkey
               ,r.groupkey
               ,r.catalogkey
               ,r.freqkey
               ,r.enddate
               ,r.calendar
               ,rm.mfgkey
               ,minstatus = min(mfgplog.statuskey)
               ,maxstatus = max(mfgplog.statuskey)
      INTO      #st
      FROM      #r AS r
      INNER JOIN vrmfg as rm ON r.calendar = rm.calendar
                         AND r.freqkey = rm.freqkey
      INNER JOIN mfgplog ON rm.mfgreportkey = mfgplog.mfgreportkey
      WHERE     r.enddate = mfgplog.enddate
                AND r.catalogkey = rm.catalogkey
                AND mfgplog.enddate = mfgplog.postperiod
                AND r.mfgkey = rm.mfgkey
      GROUP BY  r.councilkey
               ,r.groupkey
               ,r.catalogkey
               ,r.freqkey
               ,r.enddate
               ,r.calendar
               ,rm.mfgkey



      UPDATE    #r
      SET       statuskey = 1	-- no data entered
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND maxstatus = 1


      UPDATE    #r
      SET       statuskey = 2	--'Some Data Entered'
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND minstatus = 1
                              AND maxstatus > 1

      UPDATE    #r
      SET       statuskey = 3	--'All Data Entered'
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND minstatus BETWEEN 2 AND 15

      UPDATE    #r
      SET       statuskey = 4	--'All Data Submitted'
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND minstatus BETWEEN 16 AND 23

      UPDATE    #r
      SET       statuskey = 5	--'All Data Accepted (Closed)'
      FROM      #r AS r
      INNER JOIN #st AS st ON st.councilkey = r.councilkey
                              AND st.groupkey = r.groupkey
                              AND st.catalogkey = r.catalogkey
                              AND r.freqkey = st.freqkey
                              AND st.enddate = r.enddate
                              AND r.calendar = st.calendar
                              AND r.mfgkey = st.mfgkey
                              AND minstatus > 23
	
      CREATE CLUSTERED INDEX koni ON #r
      (
      catalogkey
      )

      SELECT    r.councilkey
               ,r.groupkey
               ,r.catalogkey
               ,r.freqkey
               ,r.enddate
               ,r.calendar
               ,r.mfgkey
               ,minstatus = min(mfgplog.statuskey)
               ,maxstatus = max(mfgplog.statuskey)
      INTO      #str
      FROM      #r AS r
      INNER JOIN vrmfg as rm ON r.calendar = rm.calendar
                         AND r.freqkey = rm.freqkey
      INNER JOIN mfgplog ON rm.mfgreportkey = mfgplog.mfgreportkey
      WHERE     r.enddate = mfgplog.enddate
                AND r.catalogkey = rm.catalogkey
                AND (mfgplog.enddate <> mfgplog.postperiod
                     OR mfgplog.postperiod IS NULL)
                AND r.mfgkey = rm.mfgkey
                AND r.statuskey IN (4, 5)
      GROUP BY  r.councilkey
               ,r.groupkey
               ,r.catalogkey
               ,r.freqkey
               ,r.enddate
               ,r.calendar
               ,r.mfgkey

      UPDATE    #r
      SET       statuskey = 11	--'Revisions Pending'
      FROM      #r AS r
      INNER JOIN #str AS st ON st.councilkey = r.councilkey
                               AND st.groupkey = r.groupkey
                               AND st.catalogkey = r.catalogkey
                               AND r.freqkey = st.freqkey
                               AND st.enddate = r.enddate
                               AND r.calendar = st.calendar
                               AND r.mfgkey = st.mfgkey
                               AND minstatus < 16

      UPDATE    #r
      SET       statuskey = 12	--'Revisions Submitted'
      FROM      #r AS r
      INNER JOIN #str AS st ON st.councilkey = r.councilkey
                               AND st.groupkey = r.groupkey
                               AND st.catalogkey = r.catalogkey
                               AND r.freqkey = st.freqkey
                               AND st.enddate = r.enddate
                               AND r.calendar = st.calendar
                               AND r.mfgkey = st.mfgkey
                               AND minstatus BETWEEN 16 AND 23


      DELETE    FROM #r
      WHERE     statuskey IN (4, 5)
                AND @scope = 0
                AND @listtype = 'D' 

      DELETE    FROM #r
      WHERE     statuskey IN (5, 12, 11)
                AND @scope = 0
                AND @listtype = 'S' 

      DELETE    FROM #r
      WHERE     enddate < '12/30/03'
                AND @scope = 0

select 
	reporttitle = 'AHAM Input Report Viewer Menu', 
	RunTime = getdate(), 
	Mfgkey = r.mfgkey, 
	mfgname = r.mfgname, 
	councilKey = r.councilkey, 
	councilname = r.councilname, 
	catalogkey = r.catalogkey, 
	catalogname = r.catalogname, 
	freqkey = r.freqkey, 
	Calendar = r.calendar, 
	freqname = case WHEN r.calendar = 1 THEN 'Calendar '
                                          ELSE ''
                                     END + FreqName,
	freqSort = 0-r.freqkey, 
	EndDate = r.EndDate, 
	statuskey = r.Statuskey,
	DateSort = datediff(dd, enddate, '1/1/1990')
	from #r as r
	WHERE     (@scope <> 0
                 OR r.enddate IN (SELECT TOP 4
                                            x.EndDate
                                   FROM     #r AS x
                                   WHERE    x.groupkey = r.groupkey
                                            AND x.catalogkey = r.catalogkey
                                            AND x.freqkey = r.freqkey
                                            AND x.Calendar = R.calendar
                                   ORDER BY x.Enddate DESC))

	Select 
	FilterYearsValue = y.year,
    FilterYearsText  = case WHEN y.year = 0 THEN 'Current'
                                            ELSE str(y.year, 4)
                                       END 
     FROM      @ValidYears AS y
      WHERE     y.year <> @scope



































