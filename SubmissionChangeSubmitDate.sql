USE [Ahamoper]
GO
/****** Object:  StoredProcedure [Submission].[SubmissionChangeSubmitDate]    Script Date: 2/7/2018 1:36:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







ALTER Procedure  [Submission].[SubmissionChangeSubmitDate](
	@ntuser varchar(50),
	@PlogTable [Submission].[PlogTableType] readonly
	--@doc text
)
as
/* SubmissionChangeSubmitDate sets the official submission date of the requested
   reports to the requested date
	created by Kim Strauss
	8/30/02
*/

declare @idoc int
declare @errorcount int
--declare @ntuser varchar(50)
declare @ploglist table(
mfgplogkey int primary key,
newdate datetime)

--Get data out of XML and into temp tables
set @errorcount = 0
--set @ntuser = dbo.fnNTUSER()

--EXEC sp_xml_preparedocument @idoc OUTPUT, @doc

--Insert into @ploglist(mfgplogkey,newdate)
--select mfgplogkey, newdate
--FROM       OPENXML (@idoc, '/HAI_PROCESSING/PLog',2)
--            WITH (mfgplogkey int '@Key',
--		  newdate datetime '@SubmitDate')

--EXEC sp_xml_removedocument @idoc

insert into @ploglist(mfgplogkey, newdate)
select plogkey, isnull(submissionDate, getdate()) from @plogtable 

if isnull((Select count(*) from duser where ntuser=@ntuser and opeiops=1),0) < 1 begin
SELECT  tag=1,parent=0,
	[OpenReports!1!Error]='User does not have permission to modify plog details',
	[OpenReports!1!UserName]=@ntuser,
	[OpenReports!1!TimeSubmitted]=getdate()
	for xml explicit
return -1
end


update mfgplog
set OffSubmitDate = newdate
from mfgplog
inner join @ploglist as pl on pl.mfgplogkey = mfgplog.mfgplogkey

return 0





