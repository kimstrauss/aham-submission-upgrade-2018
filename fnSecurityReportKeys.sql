USE [Ahamoper]
GO
/****** Object:  UserDefinedFunction [Submission].[fnSecurityInputReportKeys]    Script Date: 2/7/2018 11:33:31 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO





ALTER                      function [Submission].[fnSecurityInputReportKeys] (@ntuser varchar(50), @DESub char(1)= 'D')
returns @Reports table (Mfgreportkey int primary key,
	begindate smalldatetime,enddate smalldatetime)
as

begin

-- modified 2/7/18 kds pass in username 

-- declare @Reports table (outputreportkey int primary key,
--  	begindate smalldatetime,enddate smalldatetime)
-- declare @PreviewOrFinal char(1)
-- set @PrevieworFinal='f'
-- drop table #t,#or


--declare @ntuser varchar(50)
--select @ntuser=dbo.fnNtuser()

declare @or table (mfgreportkey int,begindate smalldatetime, enddate smalldatetime)



IF isnull((select max(opeiOPS) from Duser where ntuser=@ntuser),0)>0 begin 
	insert into @or
	SELECT  dor.mfgreportkey,dor.begindate,dor.enddate
	from rmfg as dor 
	where (select max(OPEIOPS) from Duser where ntuser=@ntuser)>0
	end 

else begin
	insert into @or
	select distinct rmfg.mfgreportkey,rmfg.begindate,rmfg.enddate
	--into #t
	from rmfg 
	inner join smcatalog as smc on smc.catalogsetkey = rmfg.catalogsetkey
	inner join contactRoles as cr on cr.ntuser=@ntuser
		and (cr.productkey=rmfg.productkey or cr.productkey=0)
		and (cr.catalogkey=smc.catalogkey or cr.catalogkey=0)
		and (cr.mfgkey = rmfg.mfgkey or cr.mfgkey = 0)
	and seecodata = 1 and @DESub = 'D'
	union all
	select distinct rmfg.mfgreportkey,rmfg.begindate,rmfg.enddate
	from rmfg 
	inner join smcatalog as smc on smc.catalogsetkey = rmfg.catalogsetkey
	inner join contactRoles as cr on cr.ntuser=@ntuser
		and (cr.productkey=rmfg.productkey or cr.productkey=0)
		and (cr.catalogkey=smc.catalogkey or cr.catalogkey=0)
		and (cr.mfgkey = rmfg.mfgkey or cr.mfgkey = 0)
	and submitdata = 1 and @DESub = 'S'
end 

insert into @reports
select mfgreportkey,min(begindate),max(enddate)
from @or
group by mfgreportkey

return
end


